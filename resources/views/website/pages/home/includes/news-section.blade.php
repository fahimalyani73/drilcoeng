<section id="news" class="news">
			<div class="container">
				<div class="row text-center">
					<h2 class="section-title">Work of Excellence</h2>
					<h3 class="section-sub-title">Recent Projects</h3>
				</div>
				<!--/ Title row end -->

				<div class="row">
					@if(isset($recent_projects) && !empty($recent_projects))
					@foreach($recent_projects as $key => $value)
					<div class="col-md-4 col-xs-12">
						<div class="latest-post">
							<div class="latest-post-media">
								<a href="{{ route('news.detail') }}" class="latest-post-img">
									<img class="img-responsive" src="{{ asset('uploads/projects'.'/'.$value->image_name) }}" alt="img">
								</a>
							</div>
							<div class="post-body">
								<h4 class="post-title">
									<a href="{{ route('news.detail') }}">
										{{ $value->title }}
									</a>
								</h4>
								<div class="latest-post-meta">
									<span class="post-item-date">
										<i class="fa fa-clock-o"></i> 
										{{ date('F d, Y', strtotime($value->created_at)) }}
										
									</span>
								</div>
							</div>
						</div><!-- Latest post end -->
					</div>
					@endforeach
					@endif

					<div class="col-md-4 col-xs-12">
						<div class="latest-post">
							<div class="latest-post-media">
								<a href="{{ route('news.detail') }}" class="latest-post-img">
									<img class="img-responsive" src="{{ asset('website_assets/') }}/images/news/news2.jpg" alt="img">
								</a>
							</div>
							<div class="post-body">
								<h4 class="post-title">
									<a href="{{ route('news.detail') }}">Thandler Airport Water Reclamation Facility Expansion Project Named</a>
								</h4>
								<div class="latest-post-meta">
									<span class="post-item-date">
										<i class="fa fa-clock-o"></i> June 17, 2017
									</span>
								</div>
							</div>
						</div><!-- Latest post end -->
					</div><!-- 2nd post col end -->

					<div class="col-md-4 col-xs-12">
						<div class="latest-post">
							<div class="latest-post-media">
								<a href="{{ route('news.detail') }}" class="latest-post-img">
									<img class="img-responsive" src="{{ asset('website_assets/') }}/images/news/news3.jpg" alt="img">
								</a>
							</div>
							<div class="post-body">
								<h4 class="post-title">
									<a href="{{ route('news.detail') }}">Silicon Bench and Cornike Begin Construction Solar Facilities</a>
								</h4>
								<div class="latest-post-meta">
									<span class="post-item-date">
										<i class="fa fa-clock-o"></i> Aug 13, 2017
									</span>
								</div>
							</div>
						</div><!-- Latest post end -->
					</div><!-- 3rd post col end -->
				</div>
				<!--/ Content row end -->

				<div class="general-btn text-center">
					<a class="btn btn-primary" href="news-right-sidebar.html">See All Posts</a>
				</div>

			</div>
			<!--/ Container end -->
		</section>
		<!--/ News end -->