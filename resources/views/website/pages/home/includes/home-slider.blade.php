<div id="box-slide" class="owl-carousel owl-theme page-slider">
		@if(isset($home_slider) && !empty($home_slider))
		@foreach($home_slider as $key => $value)
			<div class="item @if($key == 0) active @endif" style="background-image:url(./uploads/home_slider/{{ $value->image_name }})">
				<div class="container">
					<div class="box-slider-content" style="color: {{$value->slider_text_color}};">
						<div class="box-slider-text" style="color: {{$value->slider_text_color}};">
							<h2 class="box-slide-title" style="color: {{$value->slider_text_color}};"> {{ $value->heading_1 }} </h2>
							<h3 class="box-slide-sub-title" style="color: {{$value->slider_text_color}};"> {{ $value->heading_2 }} </h3>
							<p class="box-slide-description" style="color: {{$value->slider_text_color}};"> {{ $value->heading_3 }} </p>
							<p>
								<a href="{{ $value->button_url }}" class="slider btn btn-primary" style="color: {{$value->slider_text_color}}; background-color: {{$value->btn_bg_color}};"> {{ $value->button_text }} </a>
							</p>
						</div>
					</div>
				</div>

			</div><!-- Item 1 end -->
			@endforeach
			@endif
			

		</div><!-- Box owl carousel end-->
