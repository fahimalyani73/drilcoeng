
<section id="ts-service-area" class="ts-service-area">
			<div class="container">
				<div class="row text-center">
					<h2 class="section-title">
						@php 
							$what_we_do_title = isset($what_we_do) && !empty($what_we_do) ? $what_we_do->title : 'We Are Specialists In'
						@endphp
						{{ $what_we_do_title }}
					</h2>
					<h3 class="section-sub-title">
						@php 
							$what_we_do_heading = isset($what_we_do) && !empty($what_we_do) ? $what_we_do->heading : 'What We Do'
						@endphp
						{{ $what_we_do_heading }}
						
					</h3>
				</div>
				<!--/ Title row end -->

				<div class="row">
					<div class="col-md-4">
						@if(isset($what_we_do_detail) && !empty($what_we_do_detail))
						@foreach($what_we_do_detail as $key => $value)
						@if($key%2 == 0)
						<div class="ts-service-box">
							{{-- <div class="ts-service-box-img pull-left">
								<img src="{{ asset('uploads/what_we_do'.'/'.$value->icon) }}" alt="" class="image-icon"/ />
							</div> --}}
							<div class="ts-service-box-info">
								<h3 class="service-box-title"><a href="#"> {{ $value->title }} </a></h3>
								<p>
									{{ $value->description }}
								</p>
							</div>
						</div>
						@endif
						@endforeach
						@endif
					</div>


					<div class="col-md-4 text-center">
						@php 
							$what_we_do_service_image = isset($what_we_do) && !empty($what_we_do) ? 'uploads/what_we_do'.'/'.$what_we_do->image_name : 'website_assets/images/services/service-center.jpg'
						@endphp

						<img class="service-center-img img-responsive" src="{{ asset($what_we_do_service_image) }}" alt="" width="360px" height="540px" />
					</div><!-- Col end -->
					<div class="col-md-4">
					@if(isset($what_we_do_detail) && !empty($what_we_do_detail))
						@foreach($what_we_do_detail as $key => $value)
						@if($key%2 != 0)
					
						<div class="ts-service-box">
						{{-- 	<div class="ts-service-box-img pull-left">
								<img src="{{ asset('uploads/what_we_do'.'/'.$value->icon) }}" alt="" class="image-icon" />
							</div> --}}
							<div class="ts-service-box-info">
								<h3 class="service-box-title"><a href="#"> {{ $value->title }} </a></h3>
								<p>
									{{ $value->description }}
								</p>
							</div>
						</div><!-- Service 4 end -->
					@endif
					@endforeach
					@endif
				</div>

				</div><!-- Content row end -->

			</div>
			<!--/ Container end -->
		</section><!-- Service end -->