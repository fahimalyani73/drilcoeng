<section class="call-to-action no-padding">
	<div class="container">
		<div class="action-style-box">
			<div class="row">
				<div class="col-md-10">
					<div class="call-to-action-text">
						<h3 class="action-title">We understand your needs on construction</h3>
					</div>
				</div><!-- Col end -->
				<div class="col-md-2">
					<div class="call-to-action-btn">
						<a class="btn btn-primary" href="{{ route('contact.us') }}">Request Quote</a>
					</div>
				</div><!-- col end -->
			</div><!-- row end -->
		</div><!-- Action style box -->
	</div><!-- Container end -->
</section><!-- Action end -->