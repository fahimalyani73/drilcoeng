<section id="project-area" class="project-area solid-bg">
			<div class="container">
				<div class="row text-center">
					<h2 class="section-title">Work of Excellence</h2>
					<h3 class="section-sub-title">Recent Projects</h3>
				</div>
				<!--/ Title row end -->

				<div class="row">
					<div class="isotope-nav" data-isotope-nav="isotope">
						<ul>
							<li>
								<a href="#" class="active" data-filter="*">Show All</a>
							</li>
							@if(isset($projects) && !empty($projects))
							@foreach($projects as $key => $value)
								<li>
									<a href="#" data-filter=".project_{{ $key }}">
										{{ $value->type }}
									</a>
								</li>
							@endforeach
							@endif
{{-- 							<li><a href="#" data-filter=".education">Education</a></li>
							<li><a href="#" data-filter=".government">Government</a></li>
							<li><a href="#" data-filter=".infrastructure">Infrastructure</a></li>
							<li><a href="#" data-filter=".residential">Residential</a></li>
							<li><a href="#" data-filter=".healthcare">Healthcare</a></li> --}}
						</ul>
					</div><!-- Isotope filter end -->


					<div id="isotope" class="isotope">
						@if(isset($projects) && !empty($projects))
							@foreach($projects as $key => $value)
								<div class="col-md-4 col-sm-6 col-xs-12 project_{{ $key }} isotope-item">
									<div class="isotope-img-container">
										<a class="gallery-popup" href="images/projects/project1.jpg">
											<img class="img-responsive" src="{{ asset('uploads/projects/'.'/'.$value->image_name) }}" alt="">
											<span class="gallery-icon"><i class="fa fa-plus"></i></span>
										</a>
										<div class="project-item-info">
											<div class="project-item-info-content">
												<h3 class="project-item-title">
													<a href="{{ route('project.detail',[$value->title,base64_encode($value->id)]) }}">
														{{ $value->title }}
													</a>
												</h3>
												<p class="project-cat"> {{ $value->type }} </p>
											</div>
										</div>
									</div>
								</div>
							@endforeach
						@endif
{{-- 
						<div class="col-md-4 col-sm-6 col-xs-12 healthcare isotope-item">
							<div class="isotope-img-container">
								<a class="gallery-popup" href="images/projects/project2.jpg">
									<img class="img-responsive" src="{{ asset('website_assets/') }}/images/projects/project2.jpg" alt="">
									<span class="gallery-icon"><i class="fa fa-plus"></i></span>
								</a>
								<div class="project-item-info">
									<div class="project-item-info-content">
										<h3 class="project-item-title">
											<a href="{{ route('project.detail') }}">Ghum Touch Hospital</a>
										</h3>
										<p class="project-cat">Healthcare</p>
									</div>
								</div>
							</div>
						</div><!-- Isotope item 2 end -->

						<div class="col-md-4 col-sm-6 col-xs-12 government isotope-item">
							<div class="isotope-img-container">
								<a class="gallery-popup" href="images/projects/project3.jpg">
									<img class="img-responsive" src="{{ asset('website_assets/') }}/images/projects/project3.jpg" alt="">
									<span class="gallery-icon"><i class="fa fa-plus"></i></span>
								</a>
								<div class="project-item-info">
									<div class="project-item-info-content">
										<h3 class="project-item-title">
											<a href="{{ route('project.detail') }}">TNT East Facility</a>
										</h3>
										<p class="project-cat">Government</p>
									</div>
								</div>
							</div>
						</div><!-- Isotope item 3 end -->

						<div class="col-md-4 col-sm-6 col-xs-12 education isotope-item">
							<div class="isotope-img-container">
								<a class="gallery-popup" href="images/projects/project4.jpg">
									<img class="img-responsive" src="{{ asset('website_assets/') }}/images/projects/project4.jpg" alt="">
									<span class="gallery-icon"><i class="fa fa-plus"></i></span>
								</a>
								<div class="project-item-info">
									<div class="project-item-info-content">
										<h3 class="project-item-title">
											<a href="{{ route('project.detail') }}">Narriot Headquarters</a>
										</h3>
										<p class="project-cat">Infrastructure</p>
									</div>
								</div>
							</div>
						</div><!-- Isotope item 4 end -->

						<div class="col-md-4 col-sm-6 col-xs-12 infrastructure isotope-item">
							<div class="isotope-img-container">
								<a class="gallery-popup" href="images/projects/project5.jpg">
									<img class="img-responsive" src="{{ asset('website_assets/') }}/images/projects/project5.jpg" alt="">
									<span class="gallery-icon"><i class="fa fa-plus"></i></span>
								</a>
								<div class="project-item-info">
									<div class="project-item-info-content">
										<h3 class="project-item-title">
											<a href="{{ route('project.detail') }}">Kalas Metrorail</a>
										</h3>
										<p class="project-cat">Infrastructure</p>
									</div>
								</div>
							</div>
						</div><!-- Isotope item 5 end -->

						<div class="col-md-4 col-sm-6 col-xs-12 residential isotope-item">
							<div class="isotope-img-container">
								<a class="gallery-popup" href="images/projects/project6.jpg">
									<img class="img-responsive" src="{{ asset('website_assets/') }}/images/projects/project6.jpg" alt="">
									<span class="gallery-icon"><i class="fa fa-plus"></i></span>
								</a>
								<div class="project-item-info">
									<div class="project-item-info-content">
										<h3 class="project-item-title">
											<a href="{{ route('project.detail') }}">Ancraft Avenue House</a>
										</h3>
										<p class="project-cat">Residential</p>
									</div>
								</div>
							</div>
						</div><!-- Isotope item 6 end --> --}}
					</div><!-- Isotop end -->

					<div class="general-btn text-center">
						<a class="btn btn-primary" href="{{ route('projects') }}">View All Projects</a>
					</div>

				</div><!-- Content row end -->
			</div>
			<!--/ Container end -->
		</section><!-- Project area end -->