<section class="content">
			<div class="container">
				<div class="row">
					{{-- <div class="col-md-6">
						<h3 class="column-title">Testimonials</h3>

						<div id="testimonial-slide" class="owl-carousel owl-theme testimonial-slide">
							@if(isset($testimonial) && !empty($testimonial))
							@foreach($testimonial as $key => $value)
							<div class="item">
								<div class="quote-item">
									<span class="quote-text">
										{{ $value->description }}
									</span>

									<div class="quote-item-footer">
										<img class="testimonial-thumb" src="{{ asset('uploads/testimonial'.'/'.$value->image_name) }}" alt="testimonial">
										<div class="quote-item-info">
											<h3 class="quote-author"> {{ $value->name }} </h3>
											<span class="quote-subtext"> {{ $value->designation }} </span>
										</div>
									</div>
								</div><!-- Quote item end -->
							</div>
							@endforeach
							@endif
							<!--/ Item 1 end -->

							<div class="item">
								<div class="quote-item">
									<span class="quote-text">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor inci done idunt ut
										labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitoa tion ullamco laboris
										nisi aliquip consequat.
									</span>

									<div class="quote-item-footer">
										<img class="testimonial-thumb" src="{{ asset('website_assets/') }}/images/clients/testimonial2.png" alt="testimonial">
										<div class="quote-item-info">
											<h3 class="quote-author">Weldon Cash</h3>
											<span class="quote-subtext">CFO, First Choice</span>
										</div>
									</div>
								</div><!-- Quote item end -->
							</div>
							<!--/ Item 2 end -->

							<div class="item">
								<div class="quote-item">
									<span class="quote-text">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor inci done idunt ut
										labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitoa tion ullamco laboris
										nisi ut commodo consequat.
									</span>

									<div class="quote-item-footer">
										<img class="testimonial-thumb" src="{{ asset('website_assets/') }}/images/clients/testimonial3.png" alt="testimonial">
										<div class="quote-item-info">
											<h3 class="quote-author">Minter Puchan</h3>
											<span class="quote-subtext">Director, AKT</span>
										</div>
									</div>
								</div><!-- Quote item end -->
							</div>
							<!--/ Item 3 end -->

						</div>
						<!--/ Testimonial carousel end-->
					</div> --}}

					<div class="col-md-12">

						<h3 class="column-title">Happy Clients</h3>

						<div class="row all-clients">
							<div class="col-sm-2">
								<figure class="clients-logo">
									<a href="#"><img class="img-responsive" src="{{ asset('website_assets/') }}/images/clients/client1.png" alt="" /></a>
								</figure>
							</div><!-- Client 1 end -->

							<div class="col-sm-2">
								<figure class="clients-logo">
									<a href="#"><img class="img-responsive" src="{{ asset('website_assets/') }}/images/clients/client2.png" alt="" /></a>
								</figure>
							</div><!-- Client 2 end -->

							<div class="col-sm-2">
								<figure class="clients-logo">
									<a href="#"><img class="img-responsive" src="{{ asset('website_assets/') }}/images/clients/client3.png" alt="" /></a>
								</figure>
							</div><!-- Client 3 end -->

							<div class="col-sm-2">
								<figure class="clients-logo">
									<a href="#"><img class="img-responsive" src="{{ asset('website_assets/') }}/images/clients/client4.png" alt="" /></a>
								</figure>
							</div><!-- Client 4 end -->

							<div class="col-sm-2">
								<figure class="clients-logo">
									<a href="#"><img class="img-responsive" src="{{ asset('website_assets/') }}/images/clients/client5.png" alt="" /></a>
								</figure>
							</div><!-- Client 5 end -->

							<div class="col-sm-2">
								<figure class="clients-logo">
									<a href="#"><img class="img-responsive" src="{{ asset('website_assets/') }}/images/clients/client6.png" alt="" /></a>
								</figure>
							</div><!-- Client 6 end -->

						</div><!-- Clients row end -->

					</div><!-- Col end -->

				</div>
				<!--/ Content row end -->
			</div>
			<!--/ Container end -->
		</section><!-- Content end -->