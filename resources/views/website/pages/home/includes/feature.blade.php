<section id="ts-features" class="ts-features">
			<div class="container">
				<div class="row">
					@if(isset($service) && !empty($service))
					@foreach($service as $key => $value)
					<div class="col-md-4">
						<div class="ts-service-box">
							<div class="ts-service-image-wrapper">
								<img class="img-responsive" src="{{ asset('uploads/services'.'/'.$value->image_name ) }}" alt="">
							</div>
							<div class="ts-service-box-img pull-left">
								<img src="{{ asset('uploads/services'.'/'.$value->icon ) }}" alt="" />
							</div>
							<div class="ts-service-info">
								<h3 class="service-box-title">
									<a href="{{ route('service.detail',$value->id) }}">
										{{ $value->heading }}
									</a>
								</h3>
								<p>
									{{ $value->description }}
								</p>
								<p><a class="learn-more" href="{{ route('service.detail',$value->id) }}"><i class="fa fa-caret-right"></i> Learn More</a></p>
							</div>
						</div><!-- Service1 end -->
					</div><!-- Col 1 end -->
					@endforeach
					@endif

					{{-- <div class="col-md-4">
						<div class="ts-service-box">
							<div class="ts-service-image-wrapper">
								<img class="img-responsive" src="{{ asset('website_assets/') }}/images/services/service2.jpg" alt="">
							</div>
							<div class="ts-service-box-img pull-left">
								<img src="{{ asset('website_assets/') }}/images/icon-image/service-icon2.png" alt="" />
							</div>
							<div class="ts-service-info">
								<h3 class="service-box-title"><a href="{{ route('service.detail') }}">Virtual Construction</a></h3>
								<p>You have ideas, goals, and dreams. We have a culturally diverse, forward thinking team looking for
									talent like. Lorem ipsum dolor sit amet integer suscipit.</p>
								<p><a class="learn-more" href="{{ route('service.detail') }}"><i class="fa fa-caret-right"></i> Learn More</a></p>
							</div>
						</div><!-- Service2 end -->
					</div><!-- Col 2 end -->

					<div class="col-md-4">
						<div class="ts-service-box">
							<div class="ts-service-image-wrapper">
								<img class="img-responsive" src="{{ asset('website_assets/') }}/images/services/service3.jpg" alt="">
							</div>
							<div class="ts-service-box-img pull-left">
								<img src="{{ asset('website_assets/') }}/images/icon-image/service-icon3.png" alt="" />
							</div>
							<div class="ts-service-info">
								<h3 class="service-box-title"><a href="{{ route('service.detail') }}">Build To Last</a></h3>
								<p>You have ideas, goals, and dreams. We have a culturally diverse, forward thinking team looking for
									talent like. Lorem ipsum dolor sit amet integer suscipit.</p>
								<p><a class="learn-more" href="{{ route('service.detail') }}"><i class="fa fa-caret-right"></i> Learn More</a></p>
							</div>
						</div><!-- Service3 end -->
					</div><!-- Col 3 end --> --}}
				</div><!-- Content row end -->
			</div><!-- Container end -->
		</section><!-- Feature are end -->