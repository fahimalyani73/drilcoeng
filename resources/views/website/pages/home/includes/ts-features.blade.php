<section id="ts-features" class="ts-features">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 text-center"><h2>Services </h2></div>
					<br><br> 
					@if(isset($service) && !empty($service))
					@foreach($service as $key => $value)
					<div class="col-md-4">
						<div class="ts-service-box">
							<div class="ts-service-image-wrapper">
								<img class="img-responsive" src="{{ asset('uploads/services'.'/'.$value->image_name ) }}" alt="">
							</div>
							{{-- <div class="ts-service-box-img pull-left">
								<img src="{{ asset('uploads/services'.'/'.$value->icon ) }}" alt="" class="image-icon" />
							</div> --}}
							<div class="ts-service-info">
								<h3 class="service-box-title">
									<a href="{{ route('service.detail',[$value->heading,base64_encode($value->id)]) }}">
										{{ $value->heading }}
									</a>
								</h3>
								<p>
									{{ $value->description }}
								</p>
								<p><a class="learn-more" href="{{ route('service.detail',[$value->heading,base64_encode($value->id)]) }}"><i class="fa fa-caret-right"></i> Learn More</a></p>
							</div>
						</div><!-- Service1 end -->
					</div><!-- Col 1 end -->
					@endforeach
					@endif

					{{-- w --}}
				</div><!-- Content row end -->
			</div><!-- Container end -->
		</section>