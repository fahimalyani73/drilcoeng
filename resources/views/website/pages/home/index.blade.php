@extends('website.master-layout')

@push('css')
	<style type="text/css">
		.page-slider .item{
			min-height: 550px;
		}
	</style>
@endpush

@section('content')
	
	@include('website.pages.home.includes.home-slider')
	{{-- @include('website.pages.home.includes.request-quot') --}}
	{{-- @include('website.pages.home.includes.services') --}}
	
	@include('website.pages.home.includes.ts-features')
	@include('website.pages.home.includes.projects') 
	@include('website.pages.home.includes.project-areas')
	@include('website.pages.home.includes.testimonials')
	@include('website.pages.home.includes.subscribe-section')

	@include('website.pages.home.includes.news-section')

@endsection

@push('js')

@endpush