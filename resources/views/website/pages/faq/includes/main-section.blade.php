<section id="main-container" class="main-container">
      <div class="container">

         <div class="row">
            <div class="col-md-8">
              @if(isset($faq) && !empty($faq))
              @foreach($faq as $key => $value)
               <h3 class="border-title border-left mar-t0"> {{ $value->question_category }} </h3>

               <div class="panel-group panel-classic" id="accordion_{{ $key }}">
                
                @foreach($value['faq_detail'] as $index => $values)
                  <div class="panel panel-default">
                      <div class="panel-heading">
                         <h4 class="panel-title"> 
                           <a data-toggle="collapse" data-parent="#accordion_{{ $key }}" href="#collapse_{{ $index }}">
                            {{ $values['question'] }}
                           </a> 
                         </h4>
                      </div>
                      <div id="collapse_{{ $index }}" class="panel-collapse collapse in">
                           <div class="panel-body">
                           <p>
                             {{ $values['answer'] }}
                           </p>
                        </div>
                      </div>
                  </div><!--/ Panel 1 end-->
                  @endforeach
                  

                  <div class="panel panel-default">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                           <a data-toggle="collapse" class="collapsed" data-parent="#accordionA, #accordionB" href="#collapseTwo">What are the first aid requirements for sites?</a>
                        </h4>
                      </div>
                      <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                          <p>Anemi nim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea com modo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupid henderit in voluptate velit esse cillu oris nisi ut aliquip ex ea com matat.</p>
                        </div>
                      </div>
                  </div><!--/ Panel 2 end-->

                  <div class="panel panel-default">
                     <div class="panel-heading">
                         <h4 class="panel-title">
                         <a data-toggle="collapse" class="collapsed" data-parent="#accordionA, #accordionB" href="#collapseThree">What is an appointed person?</a> 
                        </h4>
                     </div>
                     <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                     </div>
                  </div><!--/ Panel 3 end-->

               </div><!-- Accordion end -->
               @endforeach
                  @endif

               <div class="gap-40"></div>

               <h3 class="border-title border-left">Safety</h3>

               <div class="panel-group panel-classic" id="accordionB">
                  <div class="panel panel-default">
                      <div class="panel-heading">
                         <h4 class="panel-title"> 
                           <a data-toggle="collapse" data-parent="#accordionA, #accordionB" href="#collapseA">
                           Mauris rhoncus pretium porttitor cras scelerisque commodo odio ?</a> 
                         </h4>
                      </div>
                      <div id="collapseA" class="panel-collapse collapse in">
                           <div class="panel-body">
                           <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                      </div>
                  </div><!--/ Panel 1 end-->

                  <div class="panel panel-default">
                      <div class="panel-heading">
                         <h4 class="panel-title">
                           <a data-toggle="collapse" class="collapsed" data-parent="#accordionA, #accordionB" href="#collapseB">Lutpat consequat estibulum ante ipsum primis in faucibu ?    </a>
                        </h4>
                      </div>
                      <div id="collapseB" class="panel-collapse collapse">
                        <div class="panel-body">
                          <p>Anemi nim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea com modo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupid henderit in voluptate velit esse cillu oris nisi ut aliquip ex ea com matat.</p>
                        </div>
                      </div>
                  </div><!--/ Panel 2 end-->

                  <div class="panel panel-default">
                     <div class="panel-heading">
                         <h4 class="panel-title">
                         <a data-toggle="collapse" class="collapsed" data-parent="#accordionA, #accordionB" href="#collapseC">Donec volutpat diam nec quam sagittis uenot egestas libero ? </a> 
                        </h4>
                     </div>
                     <div id="collapseC" class="panel-collapse collapse">
                        <div class="panel-body">
                          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                     </div>
                  </div><!--/ Panel 3 end-->

               </div><!-- Accordion end -->

            </div><!-- Col end -->

            <div class="col-md-4">

               <div class="sidebar sidebar-right">
                  <div class="widget recent-posts">
                     <h3 class="widget-title">Recent Posts</h3>
                     <ul class="unstyled clearfix">
                      @if(isset($recent_projects) && !empty($recent_projects))
                      @foreach($recent_projects as $key => $value)
                        <li>
                          <div class="posts-thumb pull-left"> 
                              <a href="#"><img alt="img" src="{{ asset('uploads/projects/'.'/'.$value->image_name) }}"></a>
                          </div>
                          <div class="post-info">
                              <h4 class="entry-title">
                                 <a href="#">
                                   {{ $value->title }}
                                 </a>
                              </h4>
                          </div>
                          <div class="clearfix"></div>
                        </li><!-- 1st post end-->
                        @endforeach
                        @endif

                        <li>
                          <div class="posts-thumb pull-left"> 
                              <a href="#"><img alt="img" src="{{ asset('website_assets/') }}/images/news/news2.jpg"></a>
                          </div>
                          <div class="post-info">
                              <h4 class="entry-title">
                                 <a href="#">Thandler Airport Water Reclamation Facility Expansion Project Named</a>
                              </h4>
                          </div>
                          <div class="clearfix"></div>
                        </li><!-- 2nd post end-->

                        <li>
                          <div class="posts-thumb pull-left"> 
                              <a href="#"><img alt="img" src="{{ asset('website_assets/') }}/images/news/news3.jpg"></a>
                          </div>
                          <div class="post-info">
                              <h4 class="entry-title">
                                 <a href="#">Silicon Bench And Cornike Begin Construction Solar Facilities</a>
                              </h4>
                          </div>
                          <div class="clearfix"></div>
                        </li><!-- 3rd post end-->

                     </ul>
                     
                  </div><!-- Recent post end -->
               </div><!-- Sidebar end -->
            
            </div><!-- Col end -->
            
         </div><!-- Content row end -->

      </div><!-- Container end -->
   </section>