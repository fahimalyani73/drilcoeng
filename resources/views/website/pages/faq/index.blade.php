@extends('website.master-layout')

@push('css')

@endpush

@section('content')
	
	@include('website.pages.faq.includes.banner-section')
	@include('website.pages.faq.includes.main-section')

@endsection

@push('js')

@endpush