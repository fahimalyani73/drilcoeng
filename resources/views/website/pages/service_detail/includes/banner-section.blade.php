

@php
   $bg_banner_image = isset($services) && !empty($services->banner_image_name) ? '../uploads/services/'.$services->banner_image_name : '../website_assets/images/banner/banner2.jpg';
@endphp
<style type="text/css">
   @if(isset($services->banner_text_color) && !empty($services->banner_text_color))
   .banner-text-color{
      color: {{$services->banner_text_color}};
   }
   @endif
   @if(isset($services->banner_text_color) && !empty($services->banner_text_color))
   .breadcrumb>li+li:before{
      color: {{$services->banner_text_color}};
   }
   @endif
</style>
<div id="banner-area" class="banner-area" style="background-image:url({{ $bg_banner_image }})">
      <div class="banner-text">
         <div class="container">
            <div class="row">
               <div class="col-xs-12">
                  <div class="banner-heading">
                     <h1 class="banner-title banner-text-color">Services</h1>
                     <ol class="breadcrumb banner-text-color">
                        <li>Home</li>
                        <li>Services</li>
                        <li><a href="#">
                           {{ $services->heading }}
                        </a></li>
                     </ol>
                  </div>
               </div><!-- Col end -->
            </div><!-- Row end -->
         </div><!-- Container end -->
      </div><!-- Banner text end -->
   </div>