<section id="main-container" class="main-container">
      <div class="container">
         <div class="row">

            <div class="col-lg-3 col-md-3 col-sm-12">
               <div class="sidebar sidebar-left">
                  <div class="widget">
                     <h3 class="widget-title">Services</h3>
                     <ul class="nav nav-tabs nav-stacked service-menu">
                      @if(isset($service) && !empty($service))
                      @foreach($service as $key => $value)
                        <li class="@if($key == 0) active @endif">
                          <a href="{{ route('service.detail',[str_replace(' ', '-', $value->heading),base64_encode($value->id)]) }}">
                            {{ $value->heading }}
                          </a>
                        </li>
                        @endforeach
                        @endif
            
                     </ul>
                  </div>
                  <div class="widget">
                     <div class="quote-item quote-border">
                        {{-- <div class="quote-text-border">
                          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                        </div>

                        <div class="quote-item-footer">
                           <img class="testimonial-thumb" src="{{ asset('website_assets/') }}/images/clients/testimonial1.png" alt="testimonial">
                           <div class="quote-item-info">
                              <h3 class="quote-author">Weldon Cash</h3>
                              <span class="quote-subtext">CEO, First Choice Group</span>
                           </div>
                        </div> --}}
                    </div><!-- Quote item end -->

                  </div><!-- Widget end -->

               </div><!-- Sidebar end -->
            </div><!-- Sidebar Col end -->

            <div class="col-lg-8 col-md-8 col-sm-12">
               <div class="content-inner-page">

                   <h2 class="column-title mrt-0">
                    @php 
                      $heading = isset($services->service_detail[0]) && !empty($services->service_detail[0]->heading) ? : '';
                    @endphp
                     {{ $heading }}
                   </h2>

                  <div class="row">
                     <div class="col-md-12">
                        <p>
                          @php 
                            $description_1 = isset($services->service_detail[0]) && !empty($services->service_detail[0]->description_1) ? $services->service_detail[0]->description_1 : '';
                          @endphp
                            {{ $description_1 }}
                        </p>
                        <p>
                          @php 
                            $description_2 = isset($services->service_detail[0]) && !empty($services->service_detail[0]->description_2) ? : '';
                          @endphp
                          {{ $description_2 }}
                        </p>
                     </div><!-- col end -->
                  </div><!-- 1st row end-->

                  <div class="gap-40"></div>

                  <div id="page-slider" class="owl-carousel owl-theme page-slider page-slider-small owl-loaded owl-drag">
                     

                     
                  <div class="owl-stage-outer">
                    <div class="owl-stage" style="width: 4500px; ">
                      @if(isset($services->service_detail[0]) && !empty($services->service_detail[0]['service_detail_image_slider']))

                      @foreach($services->service_detail[0]['service_detail_image_slider'] as $key => $value)
                      <div class="owl-item @if($key == 0) active @endif" style="width: 750px;">
                        <div class="item">
                  
                          <img src="{{ asset('uploads/service_detail_image_slider/'.'/'.$value->image_name) }}" alt="">
                        </div>
                      </div>
                      @endforeach
                      @endif
              
                 </div>
               </div>
               <div class="owl-nav">
                <div class="owl-prev">
                  <i class="fa fa-angle-left"></i>
                </div>
                <div class="owl-next">
                  <i class="fa fa-angle-right"></i>
                </div>
              </div>
              <div class="owl-dots disabled">
                
              </div>
            </div><!-- Page slider end -->

                  <div class="gap-40"></div>

                  <div class="row">
                     <div class="col-md-6">
                        <h3 class="column-title-small">What Makes Us Different</h3>
                         @php 
                            $what_description_1 = isset($services->service_detail[0]->what_we_make_us_different[0]) && !empty($services->service_detail[0]->what_we_make_us_different[0]->description_1) ? : '';
                          @endphp
                        <p>
                          {{ $what_description_1 }}
                        </p>
                        <p>
                           @php 
                            $what_description_2 = isset($services->service_detail[0]->what_we_make_us_different[0]) && !empty($services->service_detail[0]->what_we_make_us_different[0]->description_2) ? : '';
                          @endphp
                          {{ $what_description_2 }}
                        </p>

                        <ul class="list-arrow">
                           <li>
                            @php 
                              $what_text_point_1 = isset($services->service_detail[0]->what_we_make_us_different[0]) && !empty($services->service_detail[0]->what_we_make_us_different[0]->text_point_1) ? : '';
                            @endphp
                             {{ $what_text_point_1 }}
                           </li>
                           <li>
                            @php 
                              $what_text_point_2 = isset($services->service_detail[0]->what_we_make_us_different[0]) && !empty($services->service_detail[0]->what_we_make_us_different[0]->text_point_2) ? $services->service_detail[0]->what_we_make_us_different[0]->text_point_2 : '';
                            @endphp
                             {{ $what_text_point_2 }}
                           </li>
                           <li>
                            @php 
                              $what_text_point_3 = isset($services->service_detail[0]->what_we_make_us_different[0]) && !empty($services->service_detail[0]->what_we_make_us_different[0]->text_point_3) ? $services->service_detail[0]->what_we_make_us_different[0]->text_point_3 : '';
                            @endphp
                             {{ $what_text_point_3 }}
                           </li>
                           <li>
                            @php 
                              $what_text_point_4 = isset($services->service_detail[0]->what_we_make_us_different[0]) && !empty($services->service_detail[0]->what_we_make_us_different[0]->text_point_4) ?  $services->service_detail[0]->what_we_make_us_different[0]->text_point_4 : '';
                            @endphp
                             {{ $what_text_point_4 }}
                           </li>
                           <li>
                            @php 
                              $what_text_point_5 = isset($services->service_detail[0]->what_we_make_us_different[0]) && !empty($services->service_detail[0]->what_we_make_us_different[0]->text_point_5) ? $services->service_detail[0]->what_we_make_us_different[0]->text_point_5 : '';
                            @endphp
                             {{ $what_text_point_5 }}
                           </li>
                           <li>
                            @php 
                              $what_text_text_point_6 = isset($services->service_detail[0]->what_we_make_us_different[0]) && !empty($services->service_detail[0]->what_we_make_us_different[0]->text_text_point_6) ? $services->service_detail[0]->what_we_make_us_different[0]->text_text_point_6 : '';
                            @endphp
                             {{ $what_text_text_point_6 }}
                           </li>
                        </ul>
                     </div>

                     <div class="col-md-6">
                        <h3 class="column-title-small">You Should Know</h3>

                        <div class="panel-group panel-classic" id="accordion">
                           <div class="panel panel-default">
                               <div class="panel-heading">
                                  <h4 class="panel-title"> 
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                      @php 
                                        $you_title_1 = isset($services->service_detail[0]->you_should_know[0]) && !empty($services->service_detail[0]->you_should_know[0]->title_1) ? $services->service_detail[0]->you_should_know[0]->title_1 : '';
                                      @endphp
                                      {{ $you_title_1 }}
                                    </a> 
                                  </h4>
                               </div>
                               <div id="collapseOne" class="panel-collapse collapse in">
                                 <div class="panel-body">
                                    <p>
                                      @php 
                                        $you_description_1 = isset($services->service_detail[0]->you_should_know[0]) && !empty($services->service_detail[0]->you_should_know[0]->description_1) ? $services->service_detail[0]->you_should_know[0]->description_1 : '';
                                      @endphp
                                      {{ $you_description_1 }}

                                    </p>
                                 </div>
                               </div>
                           </div><!--/ Panel 1 end-->

                           <div class="panel panel-default">
                               <div class="panel-heading">
                                  <h4 class="panel-title">
                                    <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseTwo">
                                      @php 
                                        $you_title_2 = isset($services->service_detail[0]->you_should_know[0]) && !empty($services->service_detail[0]->you_should_know[0]->title_2) ? $services->service_detail[0]->you_should_know[0]->title_2 : '';
                                      @endphp
                                      {{ $you_title_2 }}
                                    </a>
                                 </h4>
                               </div>
                               <div id="collapseTwo" class="panel-collapse collapse">
                                 <div class="panel-body">
                                    <p>
                                      @php 
                                        $you_description_2 = isset($services->service_detail[0]->you_should_know[0]) && !empty($services->service_detail[0]->you_should_know[0]->description_2) ? $services->service_detail[0]->you_should_know[0]->description_2 : '';
                                      @endphp
                                      {{ $you_description_2 }}
                                    </p>
                                 </div>
                               </div>
                           </div><!--/ Panel 2 end-->


                           <div class="panel panel-default">
                               <div class="panel-heading">
                                  <h4 class="panel-title">
                                    <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseThree">
                                      @php 
                                        $you_title_3 = isset($services->service_detail[0]->you_should_know[0]) && !empty($services->service_detail[0]->you_should_know[0]->title_3) ? $services->service_detail[0]->you_should_know[0]->title_3 : '';
                                      @endphp
                                      {{ $you_title_3 }}
                                    </a>
                                 </h4>
                               </div>
                               <div id="collapseThree" class="panel-collapse collapse">
                                 <div class="panel-body">
                                    <p>
                                      @php 
                                        $you_description_3 = isset($services->service_detail[0]->you_should_know[0]) && !empty($services->service_detail[0]->you_should_know[0]->description_3) ? $services->service_detail[0]->you_should_know[0]->description_3 : '';
                                      @endphp
                                      {{ $you_description_3 }}
                                    </p>
                                 </div>
                               </div>
                           </div><!--/ Panel 3 end-->

                        </div><!--/ Accordion end -->
                     </div>
                  </div><!--2nd row end -->

                  <div class="gap-40"></div>

                  <div class="call-to-action classic">
                     <div class="row">
                        <div class="col-md-9">
                           <div class="call-to-action-text">
                              <h3 class="action-title">Interested with this service.</h3>
                           </div>
                        </div><!-- Col end -->
                        <div class="col-md-3">
                           <div class="call-to-action-btn">
                              <a class="btn btn-primary" href="#">Get a Quote</a>
                           </div>
                        </div><!-- col end -->
                     </div><!-- row end -->
                  </div><!-- Action end -->

               </div><!-- Content inner end -->
            </div><!-- Content Col end -->


         </div><!-- Main row end -->
      </div><!-- Conatiner end -->
   </section>



 