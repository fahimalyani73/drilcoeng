<section id="main-container" class="main-container">
      <div class="container">
         <div class="row text-center">
            <h3 class="section-sub-title">What People Said</h3>
         </div><!--/ Title row end -->


         <div class="row">
          @if(isset($testimonial) && !empty($testimonial))
          @foreach($testimonial as $key => $value)
            <div class="col-md-4 col-sm-6">
               <div class="quote-item quote-border">
                  <div class="quote-text-border">
                    {{ $value->description }}
                  </div>

                  <div class="quote-item-footer">
                     <img class="testimonial-thumb" src="{{ asset('uploads/testimonial'.'/'.$value->image_name ) }}" alt="testimonial">
                     <div class="quote-item-info">
                        <h3 class="quote-author"> {{ $value->name }} </h3>
                        <span class="quote-subtext"> {{ $value->designation }} </span>
                     </div>
                  </div>
              </div><!-- Quote item end -->
            </div><!-- End col md 4 -->
            @endforeach
            @endif

            <div class="col-md-4 col-sm-6">
               <div class="quote-item quote-border">
                  <div class="quote-text-border">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                  </div>

                  <div class="quote-item-footer">
                     <img class="testimonial-thumb" src="{{ asset('website_assets/') }}/images/clients/testimonial2.png" alt="testimonial">
                     <div class="quote-item-info">
                        <h3 class="quote-author">Weldon Cash</h3>
                        <span class="quote-subtext">CEO, First Choice Group</span>
                     </div>
                  </div>
              </div><!-- Quote item end -->
            </div><!-- End col md 4 -->

            <div class="col-md-4 col-sm-6">
               <div class="quote-item quote-border">
                  <div class="quote-text-border">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                  </div>

                  <div class="quote-item-footer">
                     <img class="testimonial-thumb" src="{{ asset('website_assets/') }}/images/clients/testimonial3.png" alt="testimonial">
                     <div class="quote-item-info">
                        <h3 class="quote-author">Hyram Izzy</h3>
                        <span class="quote-subtext">Director, AKT Group</span>
                     </div>
                  </div>
              </div><!-- Quote item end -->
            </div><!-- End col md 4 -->

         </div><!-- Content row end -->

      </div><!-- Container end -->
   </section>