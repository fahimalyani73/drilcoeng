@extends('website.master-layout')

@push('css')

@endpush

@section('content')
	
	@include('website.pages.testimonials.includes.banner-section')
	@include('website.pages.testimonials.includes.main-section')

@endsection

@push('js')

@endpush