<section id="main-container" class="main-container">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="isotope-nav" data-isotope-nav="isotope">
                  <ul>
                     <li><a href="#" class="active" data-filter="*">Show All</a></li>
                     @if(isset($projects) && !empty($projects))
                     @foreach($projects as $key => $value)
                     <li>
                        <a href="#" data-filter=".commercial-{{ $key }}">
                           {{ $value->type }}
                        </a>
                     </li>
                     @endforeach
                     @endif
                    {{--  <li><a href="#" data-filter=".education">Education</a></li>
                     <li><a href="#" data-filter=".government">Government</a></li>
                     <li><a href="#" data-filter=".infrastructure">Infrastructure</a></li>
                     <li><a href="#" data-filter=".residential">Residential</a></li>
                     <li><a href="#" data-filter=".healthcare">Healthcare</a></li> --}}
                  </ul>
               </div><!-- Isotope filter end -->
            </div><!-- Filter col end -->
         </div><!-- Filter row end -->

         <div id="isotope" class="isotope" style="position: relative; height: 608px;">
            @if(isset($projects) && !empty($projects))
            @foreach($projects as $key => $value)
               <div class="col-md-4 col-sm-6 col-xs-12 commercial isotope-item" style="position: absolute; left: 0px; top: 0px;">
                  <div class="isotope-img-container">
                     <a class="gallery-popup cboxElement" href="{{ asset('uploads/projects'.'/'.$value->image_name) }}">
                        <img class="img-responsive" src="{{ asset('uploads/projects'.'/'.$value->image_name) }}" alt="">
                        <span class="gallery-icon"><i class="fa fa-plus"></i></span>
                     </a>
                     <div class="project-item-info">
                        <div class="project-item-info-content">
                           <h3 class="project-item-title">
                           <a href="{{ route('service.detail',$value->id) }}"> {{ $value->title }} </a>
                           </h3>
                           <p class="project-cat"> {{ $value->type }} </p>
                        </div>
                     </div>
                  </div>
               </div><!-- Isotope item 1 end -->
               @endforeach
               @endif

               {{-- <div class="col-md-4 col-sm-6 col-xs-12 healthcare isotope-item" style="position: absolute; left: 380px; top: 0px;">
                  <div class="isotope-img-container">
                     <a class="gallery-popup cboxElement" href="{{ asset('website_assets/') }}/images/projects/project2.jpg">
                        <img class="img-responsive" src="{{ asset('website_assets/') }}/images/projects/project2.jpg" alt="">
                        <span class="gallery-icon"><i class="fa fa-plus"></i></span>
                     </a>
                     <div class="project-item-info">
                        <div class="project-item-info-content">
                           <h3 class="project-item-title">
                           <a href="{{ route('service.detail',1) }}">Ghum Touch Hospital</a>
                           </h3>
                           <p class="project-cat">Healthcare</p>
                        </div>
                     </div>
                  </div>
               </div><!-- Isotope item 2 end -->

               <div class="col-md-4 col-sm-6 col-xs-12 government isotope-item" style="position: absolute; left: 760px; top: 0px;">
                  <div class="isotope-img-container">
                     <a class="gallery-popup cboxElement" href="{{ asset('website_assets/') }}/images/projects/project3.jpg">
                        <img class="img-responsive" src="{{ asset('website_assets/') }}/images/projects/project3.jpg" alt="">
                        <span class="gallery-icon"><i class="fa fa-plus"></i></span>
                     </a>
                     <div class="project-item-info">
                        <div class="project-item-info-content">
                           <h3 class="project-item-title">
                           <a href="{{ route('service.detail',1) }}">TNT East Facility</a>
                           </h3>
                           <p class="project-cat">Government</p>
                        </div>
                     </div>
                  </div>
               </div><!-- Isotope item 3 end -->

               <div class="col-md-4 col-sm-6 col-xs-12 education isotope-item" style="position: absolute; left: 0px; top: 304px;">
                  <div class="isotope-img-container">
                     <a class="gallery-popup cboxElement" href="{{ asset('website_assets/') }}/images/projects/project4.jpg">
                        <img class="img-responsive" src="{{ asset('website_assets/') }}/images/projects/project4.jpg" alt="">
                        <span class="gallery-icon"><i class="fa fa-plus"></i></span>
                     </a>
                     <div class="project-item-info">
                        <div class="project-item-info-content">
                           <h3 class="project-item-title">
                           <a href="{{ route('project.detail',1) }}">Narriot Headquarters</a>
                           </h3>
                           <p class="project-cat">Infrastructure</p>
                        </div>
                     </div>
                  </div>
               </div><!-- Isotope item 4 end -->

               <div class="col-md-4 col-sm-6 col-xs-12 infrastructure isotope-item" style="position: absolute; left: 380px; top: 304px;">
                  <div class="isotope-img-container">
                     <a class="gallery-popup cboxElement" href="{{ asset('website_assets/') }}/images/projects/project5.jpg">
                        <img class="img-responsive" src="{{ asset('website_assets/') }}/images/projects/project5.jpg" alt="">
                        <span class="gallery-icon"><i class="fa fa-plus"></i></span>
                     </a>
                     <div class="project-item-info">
                        <div class="project-item-info-content">
                           <h3 class="project-item-title">
                           <a href="{{ route('service.detail',1) }}">Kalas Metrorail</a>
                           </h3>
                           <p class="project-cat">Infrastructure</p>
                        </div>
                     </div>
                  </div>
               </div><!-- Isotope item 5 end -->

               <div class="col-md-4 col-sm-6 col-xs-12 residential isotope-item" style="position: absolute; left: 760px; top: 304px;">
                  <div class="isotope-img-container">
                     <a class="gallery-popup cboxElement" href="{{ asset('website_assets/') }}/images/projects/project6.jpg">
                        <img class="img-responsive" src="{{ asset('website_assets/') }}/images/projects/project6.jpg" alt="">
                        <span class="gallery-icon"><i class="fa fa-plus"></i></span>
                     </a>
                     <div class="project-item-info">
                        <div class="project-item-info-content">
                           <h3 class="project-item-title">
                           <a href="{{ route('service.detail',1) }}">Ancraft Avenue House</a>
                           </h3>
                           <p class="project-cat">Residential</p>
                        </div>
                     </div>
                  </div>
               </div><!-- Isotope item 6 end -->
            </div><!-- Isotop end --> --}}

      </div><!-- Conatiner end -->
   </section>