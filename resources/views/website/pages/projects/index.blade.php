@extends('website.master-layout')

@push('css')

@endpush

@section('content')
	
	@include('website.pages.projects.includes.banner-section')
	@include('website.pages.projects.includes.main-section')

@endsection

@push('js')

@endpush