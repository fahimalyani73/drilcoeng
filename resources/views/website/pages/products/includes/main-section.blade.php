<section id="main-container" class="main-container">
      <div class="container">
         <div class="row text-center">
            <h3 class="section-sub-title">Products</h3>
         </div><!--/ Title row end -->

         <div class="row">
           

            @if(isset($products) && !empty($products))
            @foreach($products as $key => $value)
            <div class="col-md-3">
               <div class="ts-team-wrapper">
                  <div class="team-img-wrapper">
                     <img alt="" src="{{ asset('uploads/products'.'/'.$value->p_image) }}" class="img-responsive"  style="width: 263px !important; height: 299px !important;">
                  </div>
                  <div class="ts-team-content-classic">
                     <h3 class="ts-name"> {{ $value->name }} </h3>
                     <p class="ts-designation"> {{ $value->designation }} </p>
                     <p class="ts-description"> {{ $value->description }} </p>
                    
                  </div>
               </div><!--/ Team wrapper 1 end -->

            </div><!-- Col end -->
            @endforeach
            @endif
            

         </div><!-- Content row 1 end -->

         
      </div><!-- Container end -->
   </section>