@extends('website.master-layout')

@push('css')

@endpush

@section('content')
	
	@include('website.pages.products.includes.banner-section')
	@include('website.pages.products.includes.main-section')

@endsection

@push('js')

@endpush