<section id="main-container" class="main-container">
      <div class="container">
         <div class="row text-center">
            <h3 class="section-sub-title">Our Leaderships</h3>
         </div><!--/ Title row end -->

         <div class="row">
           

            @if(isset($teams) && !empty($teams))
            @foreach($teams as $key => $value)
            <div class="col-md-3">
               <div class="ts-team-wrapper">
                  <div class="team-img-wrapper">
                     <img alt="" src="{{ asset('uploads/our_team'.'/'.$value->image_name) }}" class="img-responsive"  style="width: 263px !important; height: 299px !important;">
                  </div>
                  <div class="ts-team-content-classic">
                     <h3 class="ts-name"> {{ $value->name }} </h3>
                     <p class="ts-designation"> {{ $value->designation }} </p>
                     <p class="ts-description"> {{ $value->description }} </p>
                     <div class="team-social-icons">
                        <a target="_blank" href="{{ $value->facebook_link }}"><i class="fa fa-facebook"></i></a>
                        <a target="_blank" href="{{ $value->twitter_link }}"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="{{ $value->instagram_link }}"><i class="fa fa-linkedin"></i></a>
                     </div><!--/ social-icons-->
                  </div>
               </div><!--/ Team wrapper 1 end -->

            </div><!-- Col end -->
            @endforeach
            @endif
            <div class="col-md-3">
               <div class="ts-team-wrapper">
                  <div class="team-img-wrapper">
                     <img alt="" src="{{ asset('website_assets') }}/images/team/team2.jpg" class="img-responsive">
                  </div>
                  <div class="ts-team-content-classic">
                     <h3 class="ts-name">ANGELA LYOUER</h3>
                     <p class="ts-designation">Innovation Officer</p>
                     <p class="ts-description">Nats Stenman began his career in construction with boots on the ground</p>
                     <div class="team-social-icons">
                        <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-linkedin"></i></a>
                     </div><!--/ social-icons-->
                  </div>
               </div><!--/ Team wrapper 2 end -->
            </div><!-- Col end -->

            <div class="col-md-3">
            </div><!-- Col end -->

         </div><!-- Content row 1 end -->

         <div class="gap-40"></div>

         <div class="row">
            <div class="col-md-3">
               <div class="ts-team-wrapper">
                  <div class="team-img-wrapper">
                     <img alt="" src="{{ asset('website_assets') }}/images/team/team3.jpg" class="img-responsive">
                  </div>
                  <div class="ts-team-content-classic">
                     <h3 class="ts-name">Mark Conter</h3>
                     <p class="ts-designation">Safety Officer</p>
                     <p class="ts-description">Nats Stenman began his career in construction with boots on the ground</p>
                     <div class="team-social-icons">
                        <a target="_blank" href="#"><i class="fa fa-facebook"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-linkedin"></i></a>
                     </div><!--/ social-icons-->
                  </div>
               </div><!--/ Team wrapper 3 end -->
            </div><!-- Col end -->

            <div class="col-md-3">
               <div class="ts-team-wrapper">
                  <div class="team-img-wrapper">
                     <img alt="" src="{{ asset('website_assets') }}/images/team/team4.jpg" class="img-responsive">
                  </div>
                  <div class="ts-team-content-classic">
                     <h3 class="ts-name">AYESHA STEWART</h3>
                     <p class="ts-designation">Finance Officer</p>
                     <p class="ts-description">Nats Stenman began his career in construction with boots on the ground</p>
                     <div class="team-social-icons">
                        <a target="_blank" href="#"><i class="fa fa-facebook"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-linkedin"></i></a>
                     </div><!--/ social-icons-->
                  </div>
               </div><!--/ Team wrapper 4 end -->

            </div><!-- Col end -->

            <div class="col-md-3">
               <div class="ts-team-wrapper">
                  <div class="team-img-wrapper">
                     <img alt="" src="{{ asset('website_assets') }}/images/team/team5.jpg" class="img-responsive">
                  </div>
                  <div class="ts-team-content-classic">
                     <h3 class="ts-name">Dave Clarkte</h3>
                     <p class="ts-designation">Civil Engineer</p>
                     <p class="ts-description">Nats Stenman began his career in construction with boots on the ground</p>
                     <div class="team-social-icons">
                        <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-linkedin"></i></a>
                     </div><!--/ social-icons-->
                  </div>
               </div><!--/ Team wrapper 5 end -->
            </div><!-- Col end -->

            <div class="col-md-3">
               <div class="ts-team-wrapper">
                  <div class="team-img-wrapper">
                     <img alt="" src="{{ asset('website_assets') }}/images/team/team6.jpg" class="img-responsive">
                  </div>
                  <div class="ts-team-content-classic">
                     <h3 class="ts-name">Elton Joe</h3>
                     <p class="ts-designation">Site Supervisor</p>
                     <p class="ts-description">Nats Stenman began his career in construction with boots on the ground</p>
                     <div class="team-social-icons">
                        <a target="_blank" href="#"><i class="fa fa-facebook"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-linkedin"></i></a>
                     </div><!--/ social-icons-->
                  </div>
               </div><!--/ Team wrapper 6 end -->
            </div><!-- Col end -->
         </div><!-- Content row end -->

      </div><!-- Container end -->
   </section>