@extends('website.master-layout')

@push('css')

@endpush

@section('content')
	
	@include('website.pages.aboutus.includes.banner-section')
	@include('website.pages.aboutus.includes.main-section')

@endsection

@push('js')

@endpush