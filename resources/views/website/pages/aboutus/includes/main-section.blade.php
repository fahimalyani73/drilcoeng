<section id="main-container" class="main-container">
      <div class="container">
         <div class="row">
            <div class="col-md-6">
               <h3 class="column-title">
                    @php 
                     $about_title = isset($aboutus) && !empty($aboutus) ? $aboutus->title : 'Who We Are';
                    @endphp 
                     {{ $about_title }}
               </h3>
               <p>
                  @php 
                     $description_1 = isset($aboutus) && !empty($aboutus) ? $aboutus->description_1 : 'when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin.';
                  @endphp 
                     {{ $description_1 }}
               </p>
               <blockquote>
                  <p>
                     @php 
                     $description_2 = isset($aboutus) && !empty($aboutus) ? $aboutus->description_2 : 'Semporibus autem quibusdam et aut officiis debitis aut rerum est aut optio cumque nihil necessitatibus autemn ec tincidunt nunc posuere ut';
                  @endphp 
                     {{ $description_2 }}

                     
                  </p>
               </blockquote>
               <p>
                  @php 
                     $description_3 = isset($aboutus) && !empty($aboutus) ? $aboutus->description_3 : 'He lay on his armour-like  back, and if he lifted. ultrices ultrices sapien, nec tincidunt nunc posuere ut. Lorem ipsum dolor sit amet, consectetur adipiscing elit. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn’t anything embarrassing.';
                  @endphp 
                     {{ $description_3 }}

                  
               </p>

            </div><!-- Col end -->

            <div class="col-md-6">
               
               <div id="page-slider" class="owl-carousel owl-theme page-slider small-bg owl-loaded owl-drag">

                  <!-- Item 1 end -->

                  <!-- Item 1 end -->

                  <!-- Item 1 end -->
               <div class="owl-stage-outer">
                  <div class="owl-stage" style="width: 3885px; transform: translate3d(-1665px, 0px, 0px); transition: all 0s ease 0s;">
                     @if(isset($aboutus_image_slider) && !empty($aboutus_image_slider))
                     @foreach($aboutus_image_slider as $key => $value)
                     <div class="owl-item cloned" style="width: 555px;">
                        <div class="item" style="background-image:url('uploads/aboutus/{{ $value->image_name }}'')">
                           <div class="container">
                              <div class="box-slider-content">
                                 <div class="box-slider-text">
                                    <h2 class="box-slide-title">
                                       {{ $value->name }}
                                    </h2>
                                 </div>    
                              </div>
                           </div>
                        </div>
                     </div>
                     @endforeach
                     @endif
               <div class="owl-item cloned" style="width: 555px;"><div class="item" style="background-image:url(website_assets/images/slider-pages/slide-page3.jpg)">
                     <div class="container">
                        <div class="box-slider-content">
                           <div class="box-slider-text">
                              <h2 class="box-slide-title">Performance</h2>
                           </div>    
                        </div>
                     </div>
                  </div></div><div class="owl-item" style="width: 555px;"><div class="item" style="background-image:url(website_assets/images/slider-pages/slide-page1.jpg)">
                     <div class="container">
                        <div class="box-slider-content">
                           <div class="box-slider-text">
                              <h2 class="box-slide-title">Leadership</h2>
                           </div>    
                        </div>
                     </div>
                  </div></div><div class="owl-item active" style="width: 555px;"><div class="item" style="background-image:url(website_assets/images/slider-pages/slide-page2.jpg)">
                     <div class="container">
                        <div class="box-slider-content">
                           <div class="box-slider-text">
                              <h2 class="box-slide-title">Relationships</h2>
                           </div>    
                        </div>
                     </div>
                  </div></div><div class="owl-item" style="width: 555px;"><div class="item" style="background-image:url(website_assets/images/slider-pages/slide-page3.jpg)">
                     <div class="container">
                        <div class="box-slider-content">
                           <div class="box-slider-text">
                              <h2 class="box-slide-title">Performance</h2>
                           </div>    
                        </div>
                     </div>
                  </div></div><div class="owl-item cloned" style="width: 555px;"><div class="item" style="background-image:url(website_assets/images/slider-pages/slide-page1.jpg)">
                     <div class="container">
                        <div class="box-slider-content">
                           <div class="box-slider-text">
                              <h2 class="box-slide-title">Leadership</h2>
                           </div>    
                        </div>
                     </div>
                  </div></div><div class="owl-item cloned" style="width: 555px;"><div class="item" style="background-image:url(website_assets/images/slider-pages/slide-page2.jpg)">
                     <div class="container">
                        <div class="box-slider-content">
                           <div class="box-slider-text">
                              <h2 class="box-slide-title">Relationships</h2>
                           </div>    
                        </div>
                     </div>
                  </div></div></div></div><div class="owl-nav"><div class="owl-prev"><i class="fa fa-angle-left"></i></div><div class="owl-next"><i class="fa fa-angle-right"></i></div></div><div class="owl-dots disabled"></div></div><!-- Page slider end-->          
            

            </div><!-- Col end -->
         </div><!-- Content row end -->

      </div><!-- Container end -->
   </section>