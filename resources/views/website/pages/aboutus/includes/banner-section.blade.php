
@php 
   $banner_image = isset($aboutus) && !empty($aboutus) ? 'uploads/aboutus/'.$aboutus->banner_image : 'website_assets/images/banner/banner1.jpg';
@endphp

<div id="banner-area" class="banner-area" style="background-image:url('{{ $banner_image }}')">
      <div class="banner-text">
         <div class="container">
            <div class="row">
               <div class="col-xs-12">
                  <div class="banner-heading">
                     <h1 class="banner-title">About Us</h1>
                     <ol class="breadcrumb">
                        <li>Home</li>
                        <li>Company</li>
                        <li><a href="#">About Us</a></li>
                     </ol>
                  </div>
               </div><!-- Col end -->
            </div><!-- Row end -->
         </div><!-- Container end -->
      </div><!-- Banner text end -->
   </div>