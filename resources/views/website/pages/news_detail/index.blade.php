@extends('website.master-layout')

@push('css')

@endpush

@section('content')
	
	@include('website.pages.news_detail.includes.banner-section')
	@include('website.pages.news_detail.includes.main-section')

@endsection

@push('js')

@endpush