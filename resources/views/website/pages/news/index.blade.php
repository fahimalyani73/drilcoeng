@extends('website.master-layout')

@push('css')

@endpush

@section('content')
	
	@include('website.pages.news.includes.banner-section')
	@include('website.pages.news.includes.main-section')

@endsection

@push('js')

@endpush