@extends('website.master-layout')

@push('css')

@endpush

@section('content')
	
	@include('website.pages.services.includes.banner-section')
	@include('website.pages.services.includes.main-section')

@endsection

@push('js')

@endpush