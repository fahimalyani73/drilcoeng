<section id="main-container" class="main-container">
      <div class="container">
         <div class="row">
            @if(isset($services) && !empty($services))
            @foreach($services as $key => $value)
            <div class="col-md-4">
               <div class="ts-service-box">
                  <div class="ts-service-image-wrapper">
                     <img class="img-responsive" src="{{ asset('uploads/services'.'/'.$value->image_name) }}" alt="">
                  </div>
                  <div class="ts-service-box-img pull-left">
                     <img src="{{ asset('uploads/services'.'/'.$value->icon) }}" alt="" class="image-icon">
                  </div>
                  <div class="ts-service-info">
                     <h3 class="service-box-title"><a href="{{ route('service.detail',$value->id) }}"> {{ $value->heading }} </a></h3>
                     <p>
                        {{ $value->description }}
                     </p>
                     <p><a class="learn-more" href="{{ route('service.detail',$value->id) }}"><i class="fa fa-caret-right"></i> Learn More</a></p>  
                  </div>
               </div><!-- Service1 end -->
            </div><!-- Col 1 end -->
            @endforeach
            @endif

            {{-- <div class="col-md-4">
               <div class="ts-service-box">
                  <div class="ts-service-image-wrapper">
                        <img class="img-responsive" src="{{ asset('website_assets/') }}/images/services/service2.jpg" alt="">
                  </div>
                  <div class="ts-service-box-img pull-left">
                     <img src="{{ asset('website_assets/') }}/images/icon-image/service-icon2.png" alt="">
                  </div>
                  <div class="ts-service-info">
                     <h3 class="service-box-title"><a href="{{ route('service.detail') }}">Building Remodels</a></h3>
                     <p>You have ideas, goals, and dreams. We have a culturally diverse, forward thinking team looking for talent like. Lorem ipsum dolor sit amet integer suscipit.</p>
                     <p><a class="learn-more" href="{{ route('service.detail') }}"><i class="fa fa-caret-right"></i> Learn More</a></p>  
                  </div>
               </div><!-- Service2 end -->
            </div><!-- Col 2 end -->

            <div class="col-md-4">
               <div class="ts-service-box">
                  <div class="ts-service-image-wrapper">
                        <img class="img-responsive" src="{{ asset('website_assets/') }}/images/services/service3.jpg" alt="">
                  </div>
                  <div class="ts-service-box-img pull-left">
                     <img src="{{ asset('website_assets/') }}/images/icon-image/service-icon3.png" alt="">
                  </div>
                  <div class="ts-service-info">
                     <h3 class="service-box-title"><a href="{{ route('service.detail') }}">Interior Design</a></h3>
                     <p>You have ideas, goals, and dreams. We have a culturally diverse, forward thinking team looking for talent like. Lorem ipsum dolor sit amet integer suscipit.</p>
                     <p><a class="learn-more" href="{{ route('service.detail') }}"><i class="fa fa-caret-right"></i> Learn More</a></p>  
                  </div>
               </div><!-- Service3 end -->
            </div><!-- Col 3 end -->

            <div class="gap-30"></div>

            <div class="col-md-4">
               <div class="ts-service-box">
                  <div class="ts-service-image-wrapper">
                     <img class="img-responsive" src="{{ asset('website_assets/') }}/images/services/service4.jpg" alt="">
                  </div>
                  <div class="ts-service-box-img pull-left">
                     <img src="{{ asset('website_assets/') }}/images/icon-image/service-icon4.png" alt="">
                  </div>
                  <div class="ts-service-info">
                     <h3 class="service-box-title"><a href="{{ route('service.detail') }}">Exterior Design</a></h3>
                     <p>You have ideas, goals, and dreams. We have a culturally diverse, forward thinking team looking for talent like. Lorem ipsum dolor sit amet integer suscipit.</p>
                     <p><a class="learn-more" href="{{ route('service.detail') }}"><i class="fa fa-caret-right"></i> Learn More</a></p>  
                  </div>
               </div><!-- Service4 end -->
            </div><!-- Col 4 end -->

            <div class="col-md-4">
               <div class="ts-service-box">
                  <div class="ts-service-image-wrapper">
                        <img class="img-responsive" src="{{ asset('website_assets/') }}/images/services/service5.jpg" alt="">
                  </div>
                  <div class="ts-service-box-img pull-left">
                     <img src="{{ asset('website_assets/') }}/images/icon-image/service-icon5.png" alt="">
                  </div>
                  <div class="ts-service-info">
                     <h3 class="service-box-title"><a href="{{ route('service.detail') }}">Renovation</a></h3>
                     <p>You have ideas, goals, and dreams. We have a culturally diverse, forward thinking team looking for talent like. Lorem ipsum dolor sit amet integer suscipit.</p>
                     <p><a class="learn-more" href="{{ route('service.detail') }}"><i class="fa fa-caret-right"></i> Learn More</a></p>  
                  </div>
               </div><!-- Service5 end -->
            </div><!-- Col 5 end -->

            <div class="col-md-4">
               <div class="ts-service-box">
                  <div class="ts-service-image-wrapper">
                        <img class="img-responsive" src="{{ asset('website_assets/') }}/images/services/service6.jpg" alt="">
                  </div>
                  <div class="ts-service-box-img pull-left">
                     <img src="{{ asset('website_assets/') }}/images/icon-image/service-icon6.png" alt="">
                  </div>
                  <div class="ts-service-info">
                     <h3 class="service-box-title"><a href="{{ route('service.detail') }}">Safety Management</a></h3>
                     <p>You have ideas, goals, and dreams. We have a culturally diverse, forward thinking team looking for talent like. Lorem ipsum dolor sit amet integer suscipit.</p>
                     <p><a class="learn-more" href="{{ route('service.detail') }}"><i class="fa fa-caret-right"></i> Learn More</a></p>  
                  </div>
               </div><!-- Service6 end -->
            </div><!-- Col 6 end --> --}}

         </div><!-- Main row end -->
      </div><!-- Conatiner end -->
   </section>