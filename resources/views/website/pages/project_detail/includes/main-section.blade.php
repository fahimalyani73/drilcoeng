<section id="main-container" class="main-container">
      <div class="container">

         <div class="row">
            <div class="col-md-8">
               <div id="page-slider" class="owl-carousel owl-theme page-slider page-slider-small owl-loaded owl-drag">
                  

                  
               <div class="owl-stage-outer"><div class="owl-stage" style="width: 4500px;">
                 @if(!empty($projects['project_detail'][0]['project_detail_image_slider']))
                  @foreach($projects['project_detail'][0]['project_detail_image_slider'] as $key => $value)
                    
                  <div class="owl-item @if($key == 0) active @endif" style="width: 750px;">
                     <div class="item">
                        <img src="{{ asset('uploads/project_detail_image_slider'.'/'.$value->image_name) }}" alt="">
                     </div>
                  </div>
                  @endforeach
                  @endif
                  {{-- @endif --}}
                  {{-- <div class="owl-item cloned animated owl-animated-out fadeOut" style="width: 750px; left: 750px;">
                     <div class="item">
                        <img src="{{ asset('website_assets/') }}/images/projects/project4.jpg" alt="">
                     </div>
                  </div>
                  <div class="owl-item active" style="width: 750px;">
                     <div class="item">
                        <img src="{{ asset('website_assets/') }}/images/projects/project5.jpg" alt="">
                     </div>
                  </div>
                  <div class="owl-item" style="width: 750px;">
                     <div class="item">
                        <img src="{{ asset('website_assets/') }}/images/projects/project4.jpg" alt="">
                     </div>
                  </div>
                  <div class="owl-item cloned" style="width: 750px;">
                     <div class="item">
                        <img src="{{ asset('website_assets/') }}/images/projects/project5.jpg" alt="">
                     </div>
                  </div>
                  <div class="owl-item cloned" style="width: 750px;">
                     <div class="item">
                        <img src="{{ asset('website_assets/') }}/images/projects/project4.jpg" alt="">
                     </div>
                  </div> --}}
               </div>
            </div>
            <div class="owl-nav">
               <div class="owl-prev">
                  <i class="fa fa-angle-left"></i>
               </div>
               <div class="owl-next">
                  <i class="fa fa-angle-right"></i>
               </div>
            </div>
            <div class="owl-dots disabled"></div></div><!-- Page slider end -->
            </div><!-- Slider col end -->

            <div class="col-md-4">

               <h3 class="column-title mrt-0">
                 @if(isset($projects->title)) {{ $projects->title }} @endif
                  
                  
               </h3>
               <p>
                  @if(isset($projects->type)) {{ $projects->type }} @endif
               </p>
               @if(!empty($projects['project_detail'][0]))
               <ul class="project-info unstyled">

                  <li>
                     <div class="project-info-label">Client</div>
                     <div class="project-info-content">
                       @if(isset($projects['project_detail'][0]->client_detail)) {{ $projects['project_detail'][0]->client_detail }} @endif
                     </div>
                  </li>
                  <li>
                     <div class="project-info-label">Architect</div>
                     <div class="project-info-content">
                        {{ $projects['project_detail'][0]->architect }}
                     </div>
                  </li>
                  <li>
                     <div class="project-info-label">Location</div>
                     <div class="project-info-content">
                        {{ $projects['project_detail'][0]->location_detail }}
                     </div>
                  </li>
                  <li>
                     <div class="project-info-label">Size</div>
                     <div class="project-info-content">
                        {{ $projects['project_detail'][0]->size_detail }}
                     </div>
                  </li>
                  <li>
                     <div class="project-info-label">Year Completed</div>
                     <div class="project-info-content">
                        {{ $projects['project_detail'][0]->year_completed }}
                     </div>
                  </li>
                  <li>
                     <div class="project-info-label">Categories</div>
                     <div class="project-info-content">
                        {{ $projects['project_detail'][0]->categories }}
                     </div>
                  </li>
                  <li>
                     <div class="project-link">
                        <a class="btn btn-primary" target="_blank" href="#">View Project</a>
                     </div>
                  </li>
               </ul>
               @endif
            
            </div><!-- Content col end -->

         </div><!-- Row end -->

      </div><!-- Conatiner end -->
   </section>