
@php
   $bg_banner_image = isset($projects) && !empty($projects->banner_image_name) ? '../uploads/projects/'.$projects->banner_image_name : '../website_assets/images/banner/banner2.jpg';
@endphp
@if(isset($projects->banner_text_color) && !empty($projects->banner_text_color))
<style type="text/css">
   .banner-text-color{
      color: {{$projects->banner_text_color}};
   }
   .breadcrumb>li+li:before{
      color: {{$projects->banner_text_color}};
   }
   @endif
</style>
<div id="banner-area" class="banner-area" style="background-image:url({{ $bg_banner_image  }})">
      <div class="banner-text">
         <div class="container">
            <div class="row">
               <div class="col-xs-12">
                  <div class="banner-heading banner-text-color">
                     <h1 class="banner-title banner-text-color">Projects</h1>
                     <ol class="breadcrumb banner-text-color">
                        <li class="color-black">Home</li>
                        <li class="color-black">Projects</li>
                        <li>
                           <a href="#" class="banner-text-color">
                              @php 
                                 $title = isset($projects->title) && !empty($projects->title) ? $projects->title : '';
                              @endphp

                              {{ $title }}
                           </a>
                        </li>
                     </ol>
                  </div>
               </div><!-- Col end -->
            </div><!-- Row end -->
         </div><!-- Container end -->
      </div><!-- Banner text end -->
   </div>