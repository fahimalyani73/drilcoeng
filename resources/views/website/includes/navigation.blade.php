<style type="text/css">
	.image_logo{
		width: 207px;
		height: 40px;
	}
</style>
<!-- Header start -->
		<header id="header" class="header-two">
			<div class="container">
				<div class="row">
					<div class="navbar-header">
						<div class="logo">
							<a href="{{ route('home.index') }}">
								@php 
									if(!is_null(settings()['logo'])){
										$logo = asset('uploads/settings/').'/'.settings()['logo'];
									}else{
										$logo = asset('website_assets/')."/images/logo.png";
									} 
								@endphp
								<img src="{{$logo}}" alt="" class="image_logo">
							</a>
						</div><!-- logo end -->
					</div><!-- Navbar header end -->

					<nav class="site-navigation navigation pull-right">
						<div class="site-nav-inner">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>

							<div class="collapse navbar-collapse navbar-responsive-collapse">
								<ul class="nav navbar-nav">
									<li class="dropdown">
										<a href="{{ route('home.index') }}">Home </a>
										
									</li>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">Services <i
												class="fa fa-angle-down"></i></a>
										<ul class="dropdown-menu" role="menu">
											<li>
												<a href="{{ route('services') }}">
													Services All
												</a>
											</li>
											@if(!is_null(services()))
											@foreach(services() as $key => $value)
												<li>
													<a href="{{ route('service.detail',[str_replace(' ', '-', $value->heading),base64_encode($value->id)]) }}">
														{{ $value->heading }}
													</a>
												</li>
											@endforeach
											@endif
											
										</ul>
									</li>
									

									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">Projects <i
												class="fa fa-angle-down"></i></a>
										<ul class="dropdown-menu" role="menu">
											<li>
												<a href="{{ route('projects') }}">
													Projects All
												</a>
											</li>
											@if(!is_null(projects()))
											@foreach(projects() as $key => $value)
												<li>
													<a href="{{ route('project.detail',[str_replace(' ', '-', $value->title),base64_encode($value->id)]) }}">
														{{ $value->title }}
													</a>
												</li>
											@endforeach
											@endif
										</ul>
									</li>

									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">Products <i
												class="fa fa-angle-down"></i></a>
										<ul class="dropdown-menu" role="menu">
											<li>
												<a href="{{ route('products') }}">
													Products All
												</a>
											</li>
											@if(!is_null(product_category()))
											@foreach(product_category() as $key => $value)
												<li>
													<a href="{{ route('products.category',[str_replace(' ', '-', $value->cat_name),base64_encode($value->id)]) }}">
														{{ $value->cat_name }}
													</a>
												</li>
											@endforeach
											@endif
										</ul>
									</li>
									
						
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">Company <i
												class="fa fa-angle-down"></i></a>
										<ul class="dropdown-menu" role="menu">
											<li><a href="{{ route('about.us') }}">About Us</a></li>
											<li><a href="{{ route('our.people') }}">Our People</a></li>
											<li><a href="{{ route('testimonial') }}">Testimonials</a></li>
											<li><a href="{{ route('faq') }}">Faq</a></li>
										</ul>
									</li>
									

									<!-- <li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">Features <i
												class="fa fa-angle-down"></i></a>
										<ul class="dropdown-menu" role="menu">
											<li><a href="typography.html">Typography</a></li>
											<li><a href="404.html">404</a></li>
											<li class="dropdown-submenu">
												<a href="#.">Parent Menu</a>
												<ul class="dropdown-menu">
													<li><a href="#">Child Menu 1</a></li>
													<li><a href="#">Child Menu 2</a></li>
													<li><a href="#">Child Menu 3</a></li>
												</ul>
											</li>
										</ul>
									</li> -->

									{{-- <li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">News <i class="fa fa-angle-down"></i></a>
										<ul class="dropdown-menu" role="menu">
											<li><a href="{{ route('news') }}">News</a></li>
											<li><a href="{{ route('news.detail') }}">News Single</a></li>
										</ul>
									</li> --}}

									<li><a href="{{ route('contact.us') }}">Contact</a></li>
									<!-- <li class="header-get-a-quote">
										<a class="btn btn-primary" href="#">Get Free Quote</a>
									</li> -->

								</ul>
								<!--/ Nav ul end -->
							</div>
							<!--/ Collapse end -->

						</div><!-- Site Navbar inner end -->

					</nav>
					<!--/ Navigation end -->

				</div><!-- Row end -->
			</div><!-- Container end -->
		</header>
		<!--/ Header end -->