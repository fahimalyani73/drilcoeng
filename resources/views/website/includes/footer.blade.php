
		<footer id="footer" class="footer bg-overlay">
			<div class="footer-main">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-12 footer-widget footer-about">
							<h3 class="widget-title">About Us</h3>
							@php 
								if(!is_null(settings()['logo'])){
									$footer_logo = asset('uploads/settings/').'/'.settings()['logo'];
								}else{
									$footer_logo = asset('website_assets/')."/images/footer-logo.png";
								} 
							@endphp

							<img class="footer-logo" src="{{$footer_logo}}" alt="" />

							<p>
								@if(!is_null(settings()))
									{{settings()['footer_aboutus']?? settings()['footer_aboutus']}}
								@else
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor inci done idunt ut
								labore et dolore magna aliqua.
								@endif
							</p>
							<div class="footer-social">
								<ul>
									<li><a href="https://facebook.com/"><i class="fa fa-facebook"></i></a></li>
									<li><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
									<li><a href="https://instagram.com/"><i class="fa fa-instagram"></i></a></li>
									<li><a href="https://github.com/"><i class="fa fa-github"></i></a></li>
								</ul>
							</div><!-- Footer social end -->
						</div><!-- Col end -->

						<div class="col-md-4 col-sm-12 footer-widget">
							<h3 class="widget-title">Working Hours</h3>
							<div class="working-hours">
								We work 7 days a week, every day excluding major holidays. Contact us if you have an emergency, with our
								Hotline and Contact form.
								<br><br> Monday - Friday: <span class="text-right">10:00 - 16:00 </span>
								<br> Saturday: <span class="text-right">12:00 - 15:00</span>
								<br> Sunday and holidays: <span class="text-right">09:00 - 12:00</span>
							</div>
						</div><!-- Col end -->

						<div class="col-md-4 col-sm-12 footer-widget">
							{{-- <h3 class="widget-title">Services</h3>
							<ul class="list-arrow">
								<li><a href="{{ route('service.detail') }}">Pre-Construction</a></li>
								<li><a href="{{ route('service.detail') }}">General Contracting</a></li>
								<li><a href="{{ route('service.detail') }}">Construction Management</a></li>
								<li><a href="{{ route('service.detail') }}">Design and Build</a></li>
								<li><a href="{{ route('service.detail') }}">Self-Perform Construction</a></li>
							</ul> --}}
						</div><!-- Col end -->


					</div><!-- Row end -->
				</div><!-- Container end -->
			</div><!-- Footer main end -->

			<div class="copyright">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<div class="copyright-info">
								<span>Copyright © {{ date('Y') }}</a></span>
							</div>
						</div>

						<div class="col-xs-12 col-sm-6">
							<div class="footer-menu">
								<ul class="nav unstyled">
									<li><a href="{{ route('about.us') }}">About</a></li>
									<li><a href="{{ route('our.people') }}">Our people</a></li>
									<li><a href="{{ route('faq') }}">Faq</a></li>
									<!-- <li><a href="{{ route('news.detail') }}">Blog</a></li> -->
									
								</ul>
							</div>
						</div>
					</div><!-- Row end -->

					<div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix">
						<button class="btn btn-primary" title="Back to Top">
							<i class="fa fa-angle-double-up"></i>
						</button>
					</div>

				</div><!-- Container end -->
			</div><!-- Copyright end -->

		</footer><!-- Footer end -->


		<!-- Javascript Files
	================================================== -->

		<!-- initialize jQuery Library -->
		<script type="text/javascript" src="{{ asset('website_assets/') }}/js/jquery.js"></script>
		<!-- Bootstrap jQuery -->
		<script type="text/javascript" src="{{ asset('website_assets/') }}/js/bootstrap.min.js"></script>
		<!-- Owl Carousel -->
		<script type="text/javascript" src="{{ asset('website_assets/') }}/js/owl.carousel.min.js"></script>
		<!-- Color box -->
		<script type="text/javascript" src="{{ asset('website_assets/') }}/js/jquery.colorbox.js"></script>
		<!-- Isotope -->
		<script type="text/javascript" src="{{ asset('website_assets/') }}/js/isotope.js"></script>
		<script type="text/javascript" src="{{ asset('website_assets/') }}/js/ini.isotope.js"></script>


    <!-- Google Map API Key-->
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&amp;libraries=places"></script>
		<!-- Google Map Plugin-->
		<script type="text/javascript" src="{{ asset('website_assets/') }}/js/gmap3.js"></script>
 
	 <!-- Template custom -->
	 <script type="text/javascript" src="{{ asset('website_assets/') }}/js/custom.js"></script>

	</div><!-- Body inner end -->

	@stack('js')
</body>

</html>