<!DOCTYPE html>
<html lang="en">

<head>

	<!-- Basic Page Needs
	================================================== -->
	@php
		$meta_title = isset($seo_data) && !empty($seo_data->meta_title) ? $seo_data->meta_title : '';
		$meta_description = isset($seo_data) && !empty($seo_data->meta_description) ? $seo_data->meta_description : '';
		$meta_keywords = isset($seo_data) && !empty($seo_data->meta_keywords) ? $seo_data->meta_keywords : '';
		$meta_canonical = isset($seo_data) && !empty($seo_data->meta_canonical) ? $seo_data->meta_canonical : '';
	@endphp

	<meta charset="utf-8">
	<title> {{ $meta_title }} </title>
	<meta name="description" content="{{ $meta_description }}" />
	<meta name="keywords" content="{{ $meta_keywords }}" />
	<link rel="canonical" href="{{ $meta_canonical }}" />

	<!-- Mobile Specific Metas
	================================================== -->

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

	@php 
		if(!is_null(settings()['favicon'])){
			$favicon = asset('uploads/settings/').'/'.settings()['favicon'];
		}else{
			$favicon = '';
		} 
	@endphp

	<link rel="shortcut icon" type="image/ico" href="{{ $favicon }}" />
	<!-- CSS
	================================================== -->

	<!-- Bootstrap -->
	<link rel="stylesheet" href="{{ asset('website_assets/') }}/css/bootstrap.min.css">
	<!-- Template styles-->
	<link rel="stylesheet" href="{{ asset('website_assets/') }}/css/style.css">
	<!-- Responsive styles-->
	<link rel="stylesheet" href="{{ asset('website_assets/') }}/css/responsive.css">
	<!-- FontAwesome -->
	<link rel="stylesheet" href="{{ asset('website_assets/') }}/css/font-awesome.min.css">
	<!-- Animation -->
	<link rel="stylesheet" href="{{ asset('website_assets/') }}/css/animate.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="{{ asset('website_assets/') }}/css/owl.carousel.min.css">
	<link rel="stylesheet" href="{{ asset('website_assets/') }}/css/owl.theme.default.min.css">
	<!-- Colorbox -->
	<link rel="stylesheet" href="{{ asset('website_assets/') }}/css/colorbox.css">

	@stack('css')
	
{{-- @if(\Settings::get('google_analytics_id'))
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async
            src="https://www.googletagmanager.com/gtag/js?id={{ \Settings::get('google_analytics_id') }}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', "{{ \Settings::get('google_analytics_id') }}");
    </script>
@endif --}}
</head>

<body>

	<div class="body-inner">

		<div id="top-bar" class="top-bar">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
						<ul class="top-info">
							<li><i class="fa fa-mobile">&nbsp;</i>
								<p class="info-text">
									@if(!is_null(settings()))
										{{settings()['mobile_phone_1']?? settings()['mobile_phone_1']}}
									@endif
								</p>
							</li>
						</ul>
					</div>
					<!--/ Top info end -->

					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 top-social text-right">
						<ul class="unstyled">
							<li>
								<a title="Facebook" href="https://facebbok.com/themefisher.com">
									<span class="social-icon"><i class="fa fa-facebook"></i></span>
								</a>
								<a title="Twitter" href="https://twitter.com/themefisher.com">
									<span class="social-icon"><i class="fa fa-twitter"></i></span>
								</a>
								<a title="Instagram" href="https://instagram.com/themefisher.com">
									<span class="social-icon"><i class="fa fa-instagram"></i></span>
								</a>
								<a title="Linkdin" href="https://github.com/themefisher.com">
									<span class="social-icon"><i class="fa fa-github"></i></span>
								</a>
							</li>
						</ul>
					</div>
					<!--/ Top social end -->
				</div>
				<!--/ Content row end -->
			</div>
			<!--/ Container end -->
		</div>
		<!--/ Topbar end -->