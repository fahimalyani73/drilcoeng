@extends('admin.admin-master-layout')

@push('css')

@endpush

@section('content')
	@include('admin.pages.home.includes.sidebar')
	@include('admin.pages.home.includes.main-content-section')
@endsection

@push('js')

@endpush