<div class="right_col" role="main" style="min-height: 674px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
        
         </div>
         <div class="title_right">
            <div class="col-md-5 col-sm-5   form-group pull-right top_search">
               <div class="input-group">
                 <a href="{{ route('project-detail.create') }}" class="btn btn-primary">Add New Record</a>
               </div>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row" style="display: block;">
  
         <div class="col-md-12 col-sm-12  ">
            <div class="x_panel">
               <div class="x_title">
                  <h2>Project Detail</h2>
                  <ul class="nav navbar-right panel_toolbox">
                     <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                           
                        </div>
                     </li>
                     <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
               </div>
               <div class="x_content">
                  
                  <div class="table-responsive">
                     <table class="table table-striped jambo_table bulk_action">
                        <thead>
                           <tr class="headings">
                              <th>
                                #
                              </th>
                              <th>Title </th>
                              <th>client_detail </th>
                              <th>architect</th>
                              <th>location_detail</th>
                              <th>size_detail</th>
                              <th>year_completed</th>
                              <th>categories</th>
                              <th>countries_experience_icon</th>
                              <th>countries_experience</th>
                              <th>Project</th>
                              <th>Status</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           @if(isset($projectd) && !empty($projectd))
                           @foreach($projectd as $key => $value)
                           <tr class="even pointer">
                              <td class="a-center ">
                                 {{ ++$key }}
                              </td>
                              <td> {{ $value->title }} </td>
                              <td> {{ $value->client_detail }} </td>
                              <td> {{ $value->architect }} </td>
                              <td> {{ $value->location_detail }} </td>
                              <td> {{ $value->size_detail }} </td>
                              <td> {{ $value->year_completed }} </td>
                              <td> {{ $value->categories }} </td>
                              <td> {{ $value->countries_experience_icon }} </td>
                              <td> {{ $value->countries_experience }} </td>
                              <td> {{ $value->project_id }} </td>
                              <td>
                                 {{ $value->status == 1 ? 'Active' : 'Inactive'  }}
                              </td>
                              <td>
                                 <a href="{{ route('project-detail.edit',$value->id) }}" class="btn btn-primary">Edit</a>
                                 <a class="delete btn btn-danger" href="javascript::void();" onclick="event.preventDefault();
                                                     document.getElementById('delete-project-detail-{{ $value->id }}').submit();">
                                                  Delete
                                 </a>
                                <form id="delete-project-detail-{{ $value->id }}" action="{{ route('project-detail.destroy',$value->id) }}" method="POST" style="display: none;">
                                 @method('delete')
                                        @csrf
                                </form>

                              </td>
                           </tr>
                           @endforeach
                           @endif
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>