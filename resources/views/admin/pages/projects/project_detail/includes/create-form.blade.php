<style type="text/css">
   .col-form-label {
    padding-top: calc(.375rem + 1px);
    padding-bottom: calc(.375rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5;
    display: block;
    width: 100%;
    text-align: left !important;
 }
</style>
<div class="right_col" role="main" style="min-height: 942px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>Project Detail</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12">
            <div class="x_panel">
               
               <div class="x_content">
                  @if(isset($projectd) && !empty($projectd))
                  
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('project-detail.update',$projectd->id) }}" enctype="multipart/form-data">
                     @method('PUT')
                  @else
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('project-detail.store') }}" enctype="multipart/form-data">
                  
                  @endif
                     @csrf
                  
                     <span class="section">Project Detail</span>
                     <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Service</label>
                           <select class="custom-select" name="project_id" id="project_id">
                              <option value="">Please Select Service</option>
                              @if(isset($projects) && !empty($projects))
                              @foreach($projects as $key => $value)
                                 <option value="{{ $value->id }}">{{ $value->title }}</option>
                              @endforeach
                              @endif
                           </select>
                           @if ($errors->has('project_id'))
                              <span class="text-danger">{{ $errors->first('project_id') }}</span>
                           @endif
                     </div>
                  </div>
                     <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Title<span class="required">*</span>
                        </label>
                           <input id="title" class="form-control" data-validate-length-range="6" data-validate-words="2" name="title" placeholder="" required="" type="text" @if(isset($projectd) && !empty($projectd->title)) value="{{ $projectd->title }}" @endif>
                        @if ($errors->has('title'))
                           <span class="text-danger">{{ $errors->first('title') }}</span>
                        @endif
                     </div>
                  </div>

                     <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">Client Detail<span class="required">*</span>
                        </label>
                           <input type="text" id="client_detail" name="client_detail" required="" class="form-control" @if(isset($projectd) && !empty($projectd->client_detail)) value="{{ $projectd->client_detail }}" @endif>
                        @if ($errors->has('client_detail'))
                           <span class="text-danger">{{ $errors->first('client_detail') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">Architect<span class="required">*</span>
                        </label>
                           <input type="text" id="architect" name="architect" required="" class="form-control" @if(isset($projectd) && !empty($projectd->architect)) value="{{ $projectd->architect }}" @endif>
                        @if ($errors->has('architect'))
                           <span class="text-danger">{{ $errors->first('architect') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">Location Detail<span class="required">*</span>
                        </label>
                           <input type="text" id="location_detail" name="location_detail" required="" class="form-control" @if(isset($projectd) && !empty($projectd->location_detail)) value="{{ $projectd->location_detail }}" @endif>
                        @if ($errors->has('location_detail'))
                           <span class="text-danger">{{ $errors->first('location_detail') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">Size Detail<span class="required">*</span>
                        </label>
                           <input type="text" id="size_detail" name="size_detail" required="" class="form-control" @if(isset($projectd) && !empty($projectd->size_detail)) value="{{ $projectd->size_detail }}" @endif>
                        @if ($errors->has('size_detail'))
                           <span class="text-danger">{{ $errors->first('size_detail') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">Year Completed<span class="required">*</span>
                        </label>
                        <div class="col-md-11 xdisplay_inputx form-group row has-feedback">
                              <input type="text" name="year_completed" class="form-control has-feedback-left" id="single_cal1" placeholder="" aria-describedby="inputSuccess2Status"  @if(isset($projectd) && !empty($projectd->year_completed)) value="{{ $projectd->year_completed }}" @endif>
                              <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                              <span id="inputSuccess2Status" class="sr-only">(success)</span>
                        </div>
                        @if ($errors->has('year_completed'))
                           <span class="text-danger">{{ $errors->first('year_completed') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">Categories<span class="required">*</span>
                        </label>
                           <input type="text" id="categories" name="categories" required="" class="form-control" @if(isset($projectd) && !empty($projectd->categories)) value="{{ $projectd->categories }}" @endif>
                        @if ($errors->has('categories'))
                           <span class="text-danger">{{ $errors->first('categories') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">Countries Experience Icon<span class="required">*</span>
                        </label>
                           <input type="text" id="countries_experience_icon" name="countries_experience_icon" required="" class="form-control" @if(isset($projectd) && !empty($projectd->countries_experience_icon)) value="{{ $projectd->countries_experience_icon }}" @endif>
                        @if ($errors->has('countries_experience_icon'))
                           <span class="text-danger">{{ $errors->first('countries_experience_icon') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">Countries Experience<span class="required">*</span>
                        </label>
                           <input type="text" id="countries_experience" name="countries_experience" required="" class="form-control" @if(isset($projectd) && !empty($projectd->countries_experience)) value="{{ $projectd->countries_experience }}" @endif>
                        @if ($errors->has('countries_experience'))
                           <span class="text-danger">{{ $errors->first('countries_experience') }}</span>
                        @endif
                     </div>
                  </div>
      
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Status</label>
                           <select class="custom-select" name="status" id="status">
                              <option value="">Please select status</option>
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                           </select>
                           @if ($errors->has('status'))
                              <span class="text-danger">{{ $errors->first('status') }}</span>
                           @endif
                     </div>
                  </div>

                     <div class="ln_solid"></div>
                     <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                           <button type="submit" class="btn btn-primary">Cancel</button>
                           <button id="send" type="submit" class="btn btn-success">
                              @if(isset($projectd) && !empty($projectd)) Update @else Submit @endif
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
