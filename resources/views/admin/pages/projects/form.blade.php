@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.projects.includes.create-form')

@endsection
@push('js')
	
	<script type="text/javascript">
		@if(isset($projects) && !empty($projects))
	   		$("#status").val("{{ $projects->status }}");
	   	@endif
	</script>
@endpush