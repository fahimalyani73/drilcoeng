<style type="text/css">
   .col-form-label {
    padding-top: calc(.375rem + 1px);
    padding-bottom: calc(.375rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5;
    display: block;
    width: 100%;
    text-align: left !important;
 }
</style>
<div class="right_col" role="main" style="min-height: 942px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>Projects</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12">
            <div class="x_panel">
               
               <div class="x_content">
                  @if(isset($projects) && !empty($projects))
                  
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('projects.update',$projects->id) }}" enctype="multipart/form-data">
                     @method('PUT')
                  @else
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('projects.store') }}" enctype="multipart/form-data">
                  
                  @endif
                     @csrf
                     
                     <span class="section">Project </span>
                     <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Title<span class="required">*</span>
                        </label>
                           <input id="heading" class="form-control" data-validate-length-range="6" data-validate-words="2" name="title" placeholder="" required="" type="text" @if(isset($projects) && !empty($projects->title)) value="{{ $projects->title }}" @endif>
                        @if ($errors->has('title'))
                           <span class="text-danger">{{ $errors->first('title') }}</span>
                        @endif
                     </div>
                  </div>

                     <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">Type<span class="required">*</span>
                        </label>
                           <input type="text" id="type" name="type" required="" class="form-control" @if(isset($projects) && !empty($projects->type)) value="{{ $projects->type }}" @endif>
                        @if ($errors->has('type'))
                           <span class="text-danger">{{ $errors->first('type') }}</span>
                        @endif
                     </div>
                  </div>
      
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">
                           Image <span class="required">*</span>
                           Image * (360px X 245px size)
                        </label>
                           <input type="file" id="image_name" name="image_name" required="" class="form-control">
                        @if ($errors->has('image_name'))
                           <span class="text-danger">{{ $errors->first('image_name') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">
                           Banner Image <span class="required">*</span>
                           (1600px X 300px size)
                        </label>
                           <input type="file" id="banner_image_name" name="banner_image_name" required="" class="form-control">
                        @if ($errors->has('banner_image_name'))
                           <span class="text-danger">{{ $errors->first('banner_image_name') }}</span>
                        @endif
                     </div>
                  </div>
      
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Status</label>
                           <select class="custom-select" name="status" id="status">
                              <option value="">Please select status</option>
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                           </select>
                           @if ($errors->has('status'))
                              <span class="text-danger">{{ $errors->first('status') }}</span>
                           @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="banner_text_color" class="col-form-label label-align">Banner Text Color</label>
                          <input type="color" id="banner_text_color" name="banner_text_color" value="#ff0000">
                     </div>
                  </div>
                     <div class="ln_solid"></div>
                     <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                           <button type="submit" class="btn btn-primary">Cancel</button>
                           <button id="send" type="submit" class="btn btn-success">
                              @if(isset($projects) && !empty($projects)) Update @else Submit @endif
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
