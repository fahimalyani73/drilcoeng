@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.projects.project_detail_image_slider.includes.create-form')

@endsection
@push('js')
	
	<script type="text/javascript">
		@if(isset($projectdimgs) && !empty($projectdimgs))
	   		$("#status").val("{{ $projectdimgs->status }}");
	   	@endif
	   	@if(isset($projectdimgs) && !empty($projectdimgs))
	   		$("#project_detail_id").val("{{ $projectdimgs->project_detail_id }}");
	   	@endif
	</script>
	
@endpush