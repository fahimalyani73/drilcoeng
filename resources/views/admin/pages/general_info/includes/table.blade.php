<div class="right_col" role="main" style="min-height: 674px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            
         </div>
         <div class="title_right">
            <div class="col-md-5 col-sm-5   form-group pull-right top_search">
               <div class="input-group">
                 <a href="{{ route('general-info.create') }}" class="btn btn-primary">Add New Record</a>
               </div>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row" style="display: block;">
  
         <div class="col-md-12 col-sm-12  ">
            <div class="x_panel">
               <div class="x_title">
                  <h2>General Info</h2>
                  <ul class="nav navbar-right panel_toolbox">
                     <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          
                        </div>
                     </li>
                     <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
               </div>
               <div class="x_content">
                 
                  <div class="table-responsive">
                     <table class="table table-striped jambo_table bulk_action">
                        <thead>
                           <tr class="headings">
                              <th>
                                #
                              </th>
                              <th>total_project_icon</th>
                              <th>staff_member_icon </th>
                              <th>hours_work_icon</th>
                              <th>countries_experience_icon</th>
                              <th>total_projects</th>
                              <th>staff_member</th>
                              <th>hours_work</th>
                              <th>countries_experience</th>
                              <th>Status</th>
                              <th>
                                 Action
                              </th>
                           </tr>
                        </thead>
                        <tbody>
                           @if(isset($general_info) && !empty($general_info))
                           @foreach($general_info as $key => $value)
                           <tr class="even pointer">
                              <td class="a-center ">
                                 {{ ++$key }}
                              </td>
                              
                              <td>
                                 <img src="{{ asset('uploads/general_info'.'/'.$value->total_project_icon) }}" width="100px" height="100px">
                              </td>
                              <td>
                                 <img src="{{ asset('uploads/general_info'.'/'.$value->staff_member_icon) }}" width="100px" height="100px">
                              </td>
                              <td>
                                 <img src="{{ asset('uploads/general_info'.'/'.$value->hours_work_icon) }}" width="100px" height="100px">
                              </td>
                              <td>
                                 <img src="{{ asset('uploads/general_info'.'/'.$value->countries_experience_icon) }}" width="100px" height="100px">
                              </td>
                              <td>{{ $value->total_projects }}</td>
                              <td>{{ $value->staff_member }}</td>
                              <td>{{ $value->hours_work }}</td>
                              <td>{{ $value->countries_experience }}</td>
                              <td>
                                 {{ $value->status == 1 ? 'Active' : 'Inactive'  }}
                              </td>
                              <td>
                                 <a href="{{ route('general-info.edit',$value->id) }}" class="btn btn-primary">Edit</a>
                                 <a class="delete btn btn-danger" href="javascript::void();" onclick="event.preventDefault();
                                                     document.getElementById('delete-general-info-{{ $value->id }}').submit();">
                                                  Delete
                                 </a>
                                <form id="delete-general-info-{{ $value->id }}" action="{{ route('general-info.destroy',$value->id) }}" method="POST" style="display: none;">
                                 @method('delete')
                                        @csrf
                                </form>

                              </td>
                           </tr>
                           @endforeach
                           @endif
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>