<style type="text/css">
   .col-form-label {
    padding-top: calc(.375rem + 1px);
    padding-bottom: calc(.375rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5;
    display: block;
    width: 100%;
    text-align: left !important;
 }
</style>
<div class="right_col" role="main" style="min-height: 942px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>General Info</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12">
            <div class="x_panel">
               
               <div class="x_content">
                  @if(isset($general_info) && !empty($general_info))
                  
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('general-info.update',$general_info->id) }}" enctype="multipart/form-data">
                     @method('PUT')
                  @else
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('general-info.store') }}" enctype="multipart/form-data">
                  
                  @endif
                     @csrf
                    
                     <span class="section">General Info</span>
                     <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Total Projects<span class="required">*</span>
                        </label>
                           <input id="total_projects" class="form-control" data-validate-length-range="6" data-validate-words="2" name="total_projects" placeholder="" required="" type="text" @if(isset($general_info) && !empty($general_info->total_projects)) value="{{ $general_info->total_projects }}" @endif>
                        @if ($errors->has('total_projects'))
                           <span class="text-danger">{{ $errors->first('total_projects') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Staff Member<span class="required">*</span>
                        </label>
                           <input id="staff_member" class="form-control" data-validate-length-range="6" data-validate-words="2" name="staff_member" placeholder="" required="" type="text" @if(isset($general_info) && !empty($general_info->staff_member)) value="{{ $general_info->staff_member }}" @endif>
                        @if ($errors->has('staff_member'))
                           <span class="text-danger">{{ $errors->first('staff_member') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Hours Work<span class="required">*</span>
                        </label>
                           <input id="hours_work" class="form-control" data-validate-length-range="6" data-validate-words="2" name="hours_work" placeholder="" required="" type="text" @if(isset($general_info) && !empty($general_info->hours_work)) value="{{ $general_info->hours_work }}" @endif>
                        @if ($errors->has('hours_work'))
                           <span class="text-danger">{{ $errors->first('hours_work') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Countries Experience<span class="required">*</span>
                        </label>
                           <input id="countries_experience" class="form-control" data-validate-length-range="6" data-validate-words="2" name="countries_experience" placeholder="" required="" type="text" @if(isset($general_info) && !empty($general_info->countries_experience)) value="{{ $general_info->countries_experience }}" @endif>
                        @if ($errors->has('countries_experience'))
                           <span class="text-danger">{{ $errors->first('countries_experience') }}</span>
                        @endif
                     </div>
                  </div>
                  
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="number">Total Project icon<span class="required">*</span>
                        </label>
                           <input type="file" id="total_project_icon" name="total_project_icon" required="" class="form-control">
                        @if ($errors->has('total_project_icon'))
                           <span class="text-danger">{{ $errors->first('total_project_icon') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="number">Staff Member Icon <span class="required">*</span>
                        </label>
                           <input type="file" id="staff_member_icon" name="staff_member_icon" required="" class="form-control">
                        @if ($errors->has('staff_member_icon'))
                           <span class="text-danger">{{ $errors->first('staff_member_icon') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="number">Hours Work icon <span class="required">*</span>
                        </label>
                           <input type="file" id="hours_work_icon" name="hours_work_icon" required="" class="form-control">
                        @if ($errors->has('hours_work_icon'))
                           <span class="text-danger">{{ $errors->first('hours_work_icon') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="number">Countries Experience icon <span class="required">*</span>
                        </label>
                           <input type="file" id="countries_experience_icon" name="countries_experience_icon" required="" class="form-control">
                        @if ($errors->has('countries_experience_icon'))
                           <span class="text-danger">{{ $errors->first('countries_experience_icon') }}</span>
                        @endif
                     </div>
                  </div>
             
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Status</label>
                           <select class="custom-select" name="status" id="status">
                              <option value="">Please select status</option>
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                           </select>
                           @if ($errors->has('status'))
                              <span class="text-danger">{{ $errors->first('status') }}</span>
                           @endif
                     </div>
                  </div>

                     <div class="ln_solid"></div>
                     <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                           <button type="submit" class="btn btn-primary">Cancel</button>
                           <button id="send" type="submit" class="btn btn-success">
                              @if(isset($general_info) && !empty($general_info)) Update @else Submit @endif
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
