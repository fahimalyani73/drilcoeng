@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.general_info.includes.create-form')

@endsection
@push('js')
	@if(isset($general_info) && !empty($general_info))
		<script type="text/javascript">
		   $("#status").val("{{ $general_info->status }}");
		</script>
	@endif
@endpush