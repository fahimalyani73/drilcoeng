<style type="text/css">
   .col-form-label {
    padding-top: calc(.375rem + 1px);
    padding-bottom: calc(.375rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5;
    display: block;
    width: 100%;
    text-align: left !important;
 }
</style>

<div class="right_col" role="main" style="min-height: 942px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>About us</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12">
            <div class="x_panel">
               
               <div class="x_content">
                  @if(isset($aboutusd) && !empty($aboutusd))
                  
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('aboutus-detail.update',$aboutusd->id) }}" enctype="multipart/form-data">
                     @method('PUT')
                  @else
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('aboutus-detail.store') }}" enctype="multipart/form-data">
                  
                  @endif
                     @csrf

                     <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Aboutus </label>
                           <select class="custom-select" name="about_us_id" id="about_us_id">
                              <option value="">Please select Aboutus</option>
                              @if(isset($aboutus) && !empty($aboutus))
                              @foreach($aboutus as $key => $value)
                                 <option value="{{ $value->id }}"> {{ $value->title }} </option>
                              @endforeach
                              @endif
                           </select>
                           @if ($errors->has('about_us_id'))
                              <span class="text-danger">{{ $errors->first('about_us_id') }}</span>
                           @endif
                     </div>
                  </div>

                     <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">name<span class="required">*</span>
                        </label>
                           <input id="name" class="form-control" data-validate-length-range="6" data-validate-words="2" name="name" required="" type="text" @if(isset($aboutusd) && !empty($aboutusd->name)) value="{{ $aboutusd->name }}" @endif>
                        @if ($errors->has('name'))
                           <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                     </div>
                  </div>
      
                  
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="website">Image <span class="required">*</span>
                        </label>
                           <input type="file" id="image_name" name="image_name" required="" placeholder="" class="form-control" @if(isset($aboutusd) && !empty($aboutusd->image_name)) value="{{ $aboutusd->image_name }}" @endif>
                        @if ($errors->has('image_name'))
                           <span class="text-danger">{{ $errors->first('image_name') }}</span>
                        @endif
                     </div>
                  </div>
                 
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Status</label>
                           <select class="custom-select" name="status" id="status">
                              <option value="">Please select status</option>
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                           </select>
                           @if ($errors->has('status'))
                              <span class="text-danger">{{ $errors->first('status') }}</span>
                           @endif
                     </div>
                  </div>

                     <div class="ln_solid"></div>
                     <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                           <button type="submit" class="btn btn-primary">Cancel</button>
                           <button id="send" type="submit" class="btn btn-success">
                              @if(isset($aboutusd) && !empty($aboutusd)) Update @else Submit @endif
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


