@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.aboutus.aboutus_detail.includes.create-form')

@endsection
@push('js')


   
   <script type="text/javascript">
   	@if(isset($aboutusd) && !empty($aboutusd))
      $("#status").val("{{ $aboutusd->status }}");
    @endif
    @if(isset($aboutusd) && !empty($aboutusd))
      $("#about_us_id").val("{{ $aboutusd->about_us_id }}");
    @endif
   </script>
   
@endpush