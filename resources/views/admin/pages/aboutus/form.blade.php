@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.aboutus.includes.create-form')

@endsection
@push('js')


   
   <script type="text/javascript">
   	@if(isset($aboutus) && !empty($aboutus))
      $("#status").val("{{ $aboutus->status }}");
    @endif
   </script>
   
@endpush