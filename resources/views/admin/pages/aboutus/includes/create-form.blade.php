<style type="text/css">
   .col-form-label {
    padding-top: calc(.375rem + 1px);
    padding-bottom: calc(.375rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5;
    display: block;
    width: 100%;
    text-align: left !important;
 }
</style>
<div class="right_col" role="main" style="min-height: 942px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>Form Validation</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12">
            <div class="x_panel">
               
               <div class="x_content">
                  @if(isset($aboutus) && !empty($aboutus))
                  
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('aboutus.update',$aboutus->id) }}" enctype="multipart/form-data">
                     @method('PUT')
                  @else
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('aboutus.store') }}" enctype="multipart/form-data">
                  
                  @endif
                     @csrf
                     <p>For alternative validation library <code>parsleyJS</code> check out in the <a href="form.html">form page</a>
                     </p>
                     <span class="section">Personal Info</span>
                    <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Title<span class="required">*</span>
                        </label>
                           <input id="title" class="form-control" data-validate-length-range="6" data-validate-words="2" name="title" placeholder="" required="" type="text" @if(isset($aboutus) && !empty($aboutus->title)) value="{{ $aboutus->title }}" @endif>
                        @if ($errors->has('title'))
                           <span class="text-danger">{{ $errors->first('title') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Description 1<span class="required">*</span>
                        </label>
                           <input id="description_1" class="form-control" data-validate-length-range="6" data-validate-words="2" name="description_1" placeholder="" required="" type="text" @if(isset($aboutus) && !empty($aboutus->description_1)) value="{{ $aboutus->description_1 }}" @endif>
                        @if ($errors->has('description_1'))
                           <span class="text-danger">{{ $errors->first('description_1') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Description 2<span class="required">*</span>
                        </label>
                           <input id="description_2" class="form-control" data-validate-length-range="6" data-validate-words="2" name="description_2" placeholder="" required="" type="text" @if(isset($aboutus) && !empty($aboutus->description_2)) value="{{ $aboutus->description_2 }}" @endif>
                        @if ($errors->has('description_2'))
                           <span class="text-danger">{{ $errors->first('description_2') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Description 3<span class="required">*</span>
                        </label>
                           <input id="description_3" class="form-control" data-validate-length-range="6" data-validate-words="2" name="description_3" placeholder="" required="" type="text" @if(isset($aboutus) && !empty($aboutus->description_3)) value="{{ $aboutus->description_3 }}" @endif>
                        @if ($errors->has('description_3'))
                           <span class="text-danger">{{ $errors->first('description_3') }}</span>
                        @endif
                     </div>
                  </div>

                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Banner Image<span class="required">*</span>
                        </label>
                           <input id="banner_image" class="form-control" data-validate-length-range="6" data-validate-words="2" name="banner_image" placeholder="" required="" type="file" @if(isset($aboutus) && !empty($aboutus->banner_image)) value="{{ $aboutus->banner_image }}" @endif>
                        @if ($errors->has('banner_image'))
                           <span class="text-danger">{{ $errors->first('banner_image') }}</span>
                        @endif
                     </div>
                  </div>
                 
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Status</label>
                           <select class="custom-select" name="status" id="status">
                              <option value="">Please select status</option>
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                           </select>
                           @if ($errors->has('status'))
                              <span class="text-danger">{{ $errors->first('status') }}</span>
                           @endif
                     </div>
                  </div>

                     <div class="ln_solid"></div>
                     <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                           <button type="submit" class="btn btn-primary">Cancel</button>
                           <button id="send" type="submit" class="btn btn-success">
                              @if(isset($aboutus) && !empty($aboutus)) Update @else Submit @endif
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


