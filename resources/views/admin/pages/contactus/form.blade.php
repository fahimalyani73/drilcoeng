@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.contactus.includes.create-form')

@endsection
@push('js')
	@if(isset($contactus) && !empty($contactus))
		<script type="text/javascript">
		   $("#status").val("{{ $contactus->status }}");
		</script>
	@endif 
@endpush