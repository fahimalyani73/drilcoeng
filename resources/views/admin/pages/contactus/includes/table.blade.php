<div class="right_col" role="main" style="min-height: 674px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">

         </div>
         <div class="title_right">
            <div class="col-md-5 col-sm-5   form-group pull-right top_search">
               <div class="input-group">
                 <a href="{{ route('contactus.create') }}" class="btn btn-primary">Add New Record</a>
               </div>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row" style="display: block;">
  
         <div class="col-md-12 col-sm-12  ">
            <div class="x_panel">
               <div class="x_title">
                  <h2>Contact Us</h2>
                  <ul class="nav navbar-right panel_toolbox">
                     <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                        </div>
                     </li>
                     <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
               </div>
               <div class="x_content">
                  
                  <div class="table-responsive">
                     <table class="table table-striped jambo_table bulk_action">
                        <thead>
                           <tr class="headings">
                              <th>
                                #
                              </th>
                              <th>Banner Image</th>
                              <th>Title</th>
                              <th>Heading</th>
                              <th>Address </th>
                              <th>Email Address </th>
                              <th>Phone 1</th>
                              <th>Phone 2 </th>
                              <th>Phone 3</th>
                              <th>Phone 4</th>
                              <th>Phone 5</th>
                              <th><span class="nobr">Status</span></th>
                              <th>
                                 Action
                              </th>
                           </tr>
                        </thead>
                        <tbody>
                           @if(isset($contactus) && !empty($contactus))
                           @foreach($contactus as $key => $value)
                           <tr class="even pointer">
                              <td class="a-center ">
                                 {{ ++$key }}
                              </td>
                              <td>
                                 <img src="{{ asset('uploads/contactus'.'/'.$value->banner_image) }}" width="100px" height="100px">
                              </td>
                              <td>{{$value->title}}</td>
                              <td>{{$value->heading}}</td>
                              <td>{{$value->address}}</td>
                              <td>{{$value->email_address}}</td>
                              <td>{{$value->phone_number_1}}</td>
                              <td>{{$value->phone_number_2 }}</td>
                              <td>{{$value->phone_number_3 }}</td>
                              <td>{{$value->phone_number_4 }}</td>
                              <td>{{$value->phone_number_5 }}</td>
                              <td>
                                 {{ $value->status == 1 ? 'Active' : 'Inactive'  }}
                              </td>
                              <td>
                                 <a href="{{ route('contactus.edit',$value->id) }}" class="btn btn-primary">Edit</a>

                                 <a class="delete btn btn-danger" href="javascript::void();" onclick="event.preventDefault();
                                                     document.getElementById('delete-contactus-{{ $value->id }}').submit();">
                                                  Delete
                                 </a>
                                <form id="delete-contactus-{{ $value->id }}" action="{{ route('contactus.destroy',$value->id) }}" method="POST" style="display: none;">
                                 @method('delete')
                                        @csrf
                                </form>
                                
                              </td>
                           </tr>
                           @endforeach
                           @endif
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>