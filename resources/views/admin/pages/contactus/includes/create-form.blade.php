<style type="text/css">
   .col-form-label {
    padding-top: calc(.375rem + 1px);
    padding-bottom: calc(.375rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5;
    display: block;
    width: 100%;
    text-align: left !important;
 }
</style>
<div class="right_col" role="main" style="min-height: 942px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>Contact Us</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12">
            <div class="x_panel">
               
               <div class="x_content">
                  @if(isset($contactus) && !empty($contactus))
                  
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('contactus.update',$contactus->id) }}" enctype="multipart/form-data">
                     @method('PUT')
                  @else
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('contactus.store') }}" enctype="multipart/form-data">
                  
                  @endif
                     @csrf
                                
                     <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">Banner Image <span class="required">*</span>
                        </label>
                           <input type="file" id="banner_image" name="banner_image" required="required" class="form-control">
                        @if ($errors->has('banner_image'))
                           <span class="text-danger">{{ $errors->first('banner_image') }}</span>
                        @endif
                     </div>
                  </div>

                     <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">Title<span class="required">*</span>
                        </label>
                           <input type="text" id="title " name="title" required="required" class="form-control" @if(isset($contactus) && !empty($contactus->title )) value="{{ $contactus->title}}" @endif>
                        @if ($errors->has('title'))
                           <span class="text-danger">{{ $errors->first('title') }}</span>
                        @endif
                       
                     </div>
                  </div>
                 <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">Heading<span class="required">*</span>
                        </label>
                           <input type="text" id="heading" name="heading" required="required" class="form-control" @if(isset($contactus) && !empty($contactus->heading)) value="{{ $contactus->heading }}" @endif>
                       @if ($errors->has('heading'))
                           <span class="text-danger">{{ $errors->first('heading') }}</span>
                        @endif
                     </div>
                  </div>
                   <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">Address<span class="required">*</span>
                        </label>
                           <input type="text" id="address" name="address" required="required" class="form-control" @if(isset($contactus) && !empty($contactus->address)) value="{{ $contactus->address}}" @endif>

                        @if ($errors->has('address'))
                           <span class="text-danger">{{ $errors->first('address') }}</span>
                        @endif

                     </div>
                  </div>
                   <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">Email<span class="required">*</span>
                        </label>
                           <input type="email" id="email_address" name="email_address" required="required" class="form-control" @if(isset($contactus) && !empty($contactus->email_address)) value="{{ $contactus->email_address}}" @endif>

                        @if ($errors->has('email_address'))
                           <span class="text-danger">{{ $errors->first('email_address') }}</span>
                        @endif

                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="website">Phone Number 1<span class="required">*</span>
                        </label>
                           <input type="url" id="phone_number_1" name="phone_number_1" required="required" placeholder="" class="form-control" @if(isset($contactus) && !empty($contactus->phone_number_1)) value="{{ $contactus->phone_number_1}}" @endif>
                        @if ($errors->has('phone_number_1'))
                           <span class="text-danger">{{ $errors->first('phone_number_1') }}</span>
                        @endif

                     </div>
                  </div>
                  
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group">
                        <label class="col-form-label label-align" for="occupation">Phone Number 2<span class="required">*</span>
                        </label>
                           <input id="occupation" type="text" name="phone_number_2" class="optional form-control" @if(isset($contactus) && !empty($contactus->phone_number_2)) value="{{ $contactus->phone_number_2}}" @endif>
                        
                        @if ($errors->has('phone_number_2'))
                           <span class="text-danger">{{ $errors->first('phone_number_2') }}</span>
                        @endif

                     </div>
                  </div>
                     <div class="col-md-6 col-sm-6">
                     <div class="item form-group">
                        <label class="col-form-label label-align" for="occupation">Phone Number 3 <span class="required">*</span>
                        </label>
                           <input id="occupation" type="text" name="phone_number_3" class="optional form-control" @if(isset($contactus) && !empty($contactus->phone_number_3)) value="{{ $contactus->phone_number_3 }}" @endif>
                        
                        @if ($errors->has('phone_number_3'))
                           <span class="text-danger">{{ $errors->first('phone_number_3') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group">
                        <label class="col-form-label label-align" for="occupation">Phone Number 4 <span class="required">*</span>
                        </label>
                           <input id="occupation" type="text" name="phone_number_4" class="optional form-control" @if(isset($contactus) && !empty($contactus->phone_number_4)) value="{{ $contactus->phone_number_4 }}" @endif>
                        
                        @if ($errors->has('phone_number_4'))
                           <span class="text-danger">{{ $errors->first('phone_number_4') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group">
                        <label class="col-form-label label-align" for="occupation">Phone Number 5 <span class="required">*</span>
                        </label>
                           <input id="occupation" type="text" name="phone_number_5" class="optional form-control" @if(isset($contactus) && !empty($contactus->phone_number_5)) value="{{ $contactus->phone_number_5 }}" @endif>
                        
                        @if ($errors->has('phone_number_5'))
                           <span class="text-danger">{{ $errors->first('phone_number_5') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Status</label>
                           <select class="custom-select" name="status" id="status">
                              <option value="">Please select status</option>
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                           </select>
                        
                        @if ($errors->has('status'))
                           <span class="text-danger">{{ $errors->first('status') }}</span>
                        @endif

                     </div>
                  </div>

                     <div class="ln_solid"></div>
                     <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                           <button type="submit" class="btn btn-primary">Cancel</button>
                           <button id="send" type="submit" class="btn btn-success">
                              @if(isset($contactus) && !empty($contactus)) Update @else Submit @endif
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


