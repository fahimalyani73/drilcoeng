<style type="text/css">
   .col-form-label {
    padding-top: calc(.375rem + 1px);
    padding-bottom: calc(.375rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5;
    display: block;
    width: 100%;
    text-align: left !important;
 }
</style>
<div class="right_col" role="main" style="min-height: 942px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>Form Validation</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12">
            <div class="x_panel">
               
               <div class="x_content">
                  @if(isset($settings) && !empty($settings))
                  
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('settings.update',$settings->id) }}" enctype="multipart/form-data">
                     @method('PUT')
                  @else
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('settings.store') }}" enctype="multipart/form-data">
                  
                  @endif
                     @csrf
                     <p>For alternative validation library <code>parsleyJS</code> check out in the <a href="form.html">form page</a>
                     </p>
                     <span class="section">Personal Info</span>
                     <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Title<span class="required">*</span>
                        </label>
                           <input id="title" class="form-control" data-validate-length-range="6" data-validate-words="2" name="title" placeholder="Title Name" required="" type="text" @if(isset($settings) && !empty($settings->title)) value="{{ $settings->title }}" @endif>
                        @if ($errors->has('title'))
                           <span class="text-danger">{{ $errors->first('title') }}</span>
                        @endif
                     </div>
                  </div>
                  
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="number">Menu Logo <span class="required">*</span>
                        </label>
                           <input type="file" id="menu_logo" name="menu_logo" required="" class="form-control">
                        @if ($errors->has('menu_logo'))
                           <span class="text-danger">{{ $errors->first('menu_logo') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="number">Footer Logo <span class="required">*</span>
                        </label>
                           <input type="file" id="footer_logo" name="footer_logo" required="" class="form-control">
                        @if ($errors->has('footer_logo'))
                           <span class="text-danger">{{ $errors->first('footer_logo') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="number">Favicon <span class="required">*</span>
                        </label>
                           <input type="file" id="favicon" name="favicon" required="" class="form-control">
                        @if ($errors->has('favicon'))
                           <span class="text-danger">{{ $errors->first('favicon') }}</span>
                        @endif
                     </div>
                  </div>
                      <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">Facebook Link<span class="required">*</span>
                        </label>
                           <input type="text" id="facebook_link" name="facebook_link" required="" class="form-control" @if(isset($settings) && !empty($settings->facebook_link)) value="{{ $settings->facebook_link }}" @endif>
                        @if ($errors->has('facebook_link'))
                           <span class="text-danger">{{ $errors->first('facebook_link') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="website">Instagram Link <span class="required">*</span>
                        </label>
                           <input type="url" id="instagram_link" name="instagram_link" required="" placeholder="www.website.com" class="form-control" @if(isset($settings) && !empty($settings->instagram_link)) value="{{ $settings->instagram_link }}" @endif>
                        @if ($errors->has('instagram_link'))
                           <span class="text-danger">{{ $errors->first('instagram_link') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group">
                        <label class="col-form-label label-align" for="occupation">Twitter Link<span class="required">*</span>
                        </label>
                           <input id="twitter_link" type="text" name="twitter_link" class="optional form-control" @if(isset($settings) && !empty($settings->twitter_link)) value="{{ $settings->twitter_link }}" @endif>
                           @if ($errors->has('twitter_link'))
                              <span class="text-danger">{{ $errors->first('twitter_link') }}</span>
                           @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group">
                        <label class="col-form-label label-align" for="occupation">Youtube Link<span class="required">*</span>
                        </label>
                           <input id="youtube_link" type="text" name="youtube_link" class="optional form-control" @if(isset($settings) && !empty($settings->youtube_link)) value="{{ $settings->youtube_link }}" @endif>
                           @if ($errors->has('youtube_link'))
                              <span class="text-danger">{{ $errors->first('youtube_link') }}</span>
                           @endif
                     </div>
                  </div>
                
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Status</label>
                           <select class="custom-select" name="status" id="status">
                              <option value="">Please select status</option>
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                           </select>
                           @if ($errors->has('status'))
                              <span class="text-danger">{{ $errors->first('status') }}</span>
                           @endif
                     </div>
                  </div>

                     <div class="ln_solid"></div>
                     <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                           <button type="submit" class="btn btn-primary">Cancel</button>
                           <button id="send" type="submit" class="btn btn-success">
                              @if(isset($settings) && !empty($settings)) Update @else Submit @endif
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
