<div class="right_col" role="main" style="min-height: 674px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>Tables <small>Some examples to get you started</small></h3>
         </div>
         <div class="title_right">
            <div class="col-md-5 col-sm-5   form-group pull-right top_search">
               <div class="input-group">
                 <a href="{{ route('settings.create') }}" class="btn btn-primary">Add New Record</a>
               </div>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row" style="display: block;">
  
         <div class="col-md-12 col-sm-12  ">
            <div class="x_panel">
               <div class="x_title">
                  <h2>Table design <small>Custom design</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                     <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                           <a class="dropdown-item" href="#">Settings 1</a>
                           <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                     </li>
                     <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
               </div>
               <div class="x_content">
                  <p>Add class <code>bulk_action</code> to table for bulk actions options on row select</p>
                  <div class="table-responsive">
                     <table class="table table-striped jambo_table bulk_action">
                        <thead>
                           <tr class="headings">
                              <th>
                                #
                              </th>
                              <th>Title</th>
                              <th>Menu Logo </th>
                              <th>Footer Logo</th>
                              <th>Favicon</th>
                              <th>Facebook Link</th>
                              <th>Instagram Link</th>
                              <th>Twitter Link</th>
                              <th>Youtube Link</th>
                              <th><span class="nobr">Status</span></th>
                              <th>
                                 Action
                              </th>
                           </tr>
                        </thead>
                        <tbody>
                           @if(isset($settings) && !empty($settings))
                           @foreach($settings as $key => $value)
                           <tr class="even pointer">
                              <td class="a-center ">
                                 {{ ++$key }}
                              </td>
                              
                              <td class=" ">{{$value->title}}</td>
                              <td>
                                 <img src="{{ asset('uploads/settings'.'/'.$value->menu_logo) }}" width="100px" height="100px">
                              </td>
                              <td>
                                 <img src="{{ asset('uploads/settings'.'/'.$value->footer_logo) }}" width="100px" height="100px">
                              </td>
                              <td>
                                 <img src="{{ asset('uploads/settings'.'/'.$value->favicon) }}" width="100px" height="100px">
                              </td>
                              <td>{{$value->facebook_link}}</td>
                              <td>{{$value->instagram_link}}</td>
                              <td>{{$value->twitter_link}}</td>
                              <td>{{$value->youtube_link}}</td>
                              <td>
                                 {{ $value->status == 1 ? 'Active' : 'Inactive'  }}
                              </td>
                              <td>
                                 <a href="{{ route('settings.edit',$value->id) }}" class="btn btn-primary">Edit</a>
                                 <a class="delete btn btn-danger" href="javascript::void();" onclick="event.preventDefault();
                                                     document.getElementById('delete-settings-{{ $value->id }}').submit();">
                                                  Delete
                                 </a>
                                <form id="delete-settings-{{ $value->id }}" action="{{ route('settings.destroy',$value->id) }}" method="POST" style="display: none;">
                                 @method('delete')
                                        @csrf
                                </form>

                              </td>
                           </tr>
                           @endforeach
                           @endif
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>