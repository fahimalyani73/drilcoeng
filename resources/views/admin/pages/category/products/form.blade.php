@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.category.products.includes.create-form')

@endsection
@push('js')
	
	<script type="text/javascript">
		@if(isset($products) && !empty($products))
	   		$("#status").val("{{ $products->status }}");
	   	@endif
	   	@if(isset($products) && !empty($products))
	   		$("#cat_id").val("{{ $products->cat_id }}");
	   	@endif
	</script>
@endpush