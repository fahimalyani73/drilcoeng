<style type="text/css">
   .col-form-label {
    padding-top: calc(.375rem + 1px);
    padding-bottom: calc(.375rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5;
    display: block;
    width: 100%;
    text-align: left !important;
 }
</style>
<div class="right_col" role="main" style="min-height: 942px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>Project Detail</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12">
            <div class="x_panel">
               
               <div class="x_content">
                  @if(isset($products) && !empty($products))
                  
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('products.update',$products->id) }}" enctype="multipart/form-data">
                     @method('PUT')
                  @else
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('products.store') }}" enctype="multipart/form-data">
                  
                  @endif
                     @csrf
                  
                     <span class="section">Products</span>
                     <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Products</label>
                           <select class="custom-select" name="cat_id" id="cat_id">
                              <option value="">Please Select Category</option>
                              @if(isset($categories) && !empty($categories))
                              @foreach($categories as $key => $value)
                                 <option value="{{ $value->id }}">{{ $value->cat_name }}</option>
                              @endforeach
                              @endif
                           </select>
                           @if ($errors->has('cat_id'))
                              <span class="text-danger">{{ $errors->first('cat_id') }}</span>
                           @endif
                     </div>
                  </div>
                     <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Name<span class="required">*</span>
                        </label>
                           <input id="name" class="form-control" data-validate-length-range="6" data-validate-words="2" name="name" placeholder="" required="" type="text" @if(isset($products) && !empty($products->name)) value="{{ $products->name }}" @endif>
                        @if ($errors->has('name'))
                           <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                     </div>
                  </div>

                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="p_image">
                           Image <span class="required">*</span>
                           Image * (360px X 245px size)
                        </label>
                           <input type="file" id="p_image" name="p_image" required="" class="form-control">
                        @if ($errors->has('p_image'))
                           <span class="text-danger">{{ $errors->first('p_image') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="p_image">
                           Banner Image <span class="required">*</span>
                           (1600px X 300px size)
                        </label>
                           <input type="file" id="banner_image_name" name="banner_image_name" required="" class="form-control">
                        @if ($errors->has('banner_image_name'))
                           <span class="text-danger">{{ $errors->first('banner_image_name') }}</span>
                        @endif
                     </div>
                  </div>

                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Status</label>
                           <select class="custom-select" name="status" id="status">
                              <option value="">Please select status</option>
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                           </select>
                           @if ($errors->has('status'))
                                 <span class="text-danger">{{ $errors->first('status') }}</span>
                           @endif
                     </div>
                  </div>  

                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="banner_text_color" class="col-form-label label-align">Banner Text Color</label>
                          <input type="color" id="banner_text_color" name="banner_text_color" @if(isset($products) && !empty($products->banner_text_color)) value="{{ $products->banner_text_color }}" @else value="#ff0000" @endif>
                     </div>
                  </div>

                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="description" class="col-form-label label-align">Description</label>
                          <input type="text" id="description" name="description" @if(isset($products) && !empty($products->description)) value="{{ $products->description }}" @endif>
                     </div>
                  </div>
                  
                     <div class="ln_solid"></div>
                     <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                           <button type="submit" class="btn btn-primary">Cancel</button>
                           <button id="send" type="submit" class="btn btn-success">
                              @if(isset($products) && !empty($products)) Update @else Submit @endif
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
