@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.category.includes.create-form')

@endsection
@push('js')
	
	<script type="text/javascript">
		@if(isset($categories) && !empty($categories))
	   		$("#status").val("{{ $categories->status }}");
	   	@endif
	</script>
@endpush