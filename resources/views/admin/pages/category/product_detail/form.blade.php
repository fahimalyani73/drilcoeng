@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.projects.project_detail.includes.create-form')

@endsection
@push('js')
	
	<script type="text/javascript">
		@if(isset($projectd) && !empty($projectd))
	   		$("#status").val("{{ $projectd->status }}");
	   	@endif
	   	@if(isset($projectd) && !empty($projectd))
	   		$("#project_id").val("{{ $projectd->project_id }}");
	   	@endif
	</script>
@endpush