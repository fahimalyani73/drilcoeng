<style type="text/css">
   .col-form-label {
    padding-top: calc(.375rem + 1px);
    padding-bottom: calc(.375rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5;
    display: block;
    width: 100%;
    text-align: left !important;
 }
</style>
<div class="right_col" role="main" style="min-height: 942px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>Seo Data</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12">
            <div class="x_panel">
               
               <div class="x_content">
                  @if(isset($seo_data) && !empty($seo_data))
                  
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('seo-data.update',$seo_data->id) }}" enctype="multipart/form-data">
                     @method('PUT')
                  @else
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('seo-data.store') }}" enctype="multipart/form-data">
                  
                  @endif
                     @csrf
                     
                     <span class="section">Seo Data </span>

                     <div class="col-md-6 col-sm-6">
                        <div class="item form-group bad">
                           <label for="password" class="col-form-label label-align">Page Name</label>
                              <select class="custom-select" name="page_name" id="page_name">
                                 <option value="">Select Page Name</option>
                                 <option value="home_page">Home </option>
                                 <option value="about_us_page">About Us </option>
                                 <option value="our_team_page"> Our Team Page </option>
                                 <option value="testimonial_page"> Testimonial Page </option>
                                 <option value="faq_page"> Faq Page </option>
                                 <option value="contact_page"> Contact Page </option>
                                 <option value="all_projects"> All Projects Page </option>
                                 @if(!is_null(projects()))
                                 @foreach(projects() as $key => $value)
                                 @php
                                    $page_name = str_replace(' ', '_', $value->title);
                                 @endphp
                                    <option value="{{ $page_name }}_page"> {{ $value->title }} </option>
                                 @endforeach
                                 @endif
                                 <option value="all_services_page"> All Services Page </option>
                                 @if(!is_null(services()))
                                 @foreach(services() as $key => $value)
                                 @php
                                    $page_name = str_replace(' ', '_', $value->heading);
                                 @endphp
                                    <option value="{{ $page_name }}_page"> {{ $value->heading }} </option>
                                 @endforeach
                                 @endif
                              </select>
                              @if ($errors->has('status'))
                                 <span class="text-danger">{{ $errors->first('status') }}</span>
                              @endif
                        </div>
                     </div>

                     <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Title<span class="required">*</span>
                        </label>
                           <input id="meta_title" class="form-control"  name="meta_title" placeholder="" required="" type="text" @if(isset($seo_data) && !empty($seo_data->meta_title)) value="{{ $seo_data->meta_title }}" @endif>
                        @if ($errors->has('meta_title'))
                           <span class="text-danger">{{ $errors->first('meta_title') }}</span>
                        @endif
                     </div>
                  </div>

                     <div class="col-md-6 col-sm-6">
                        <div class="item form-group bad">
                           <label class="col-form-label label-align" for="email"> Meta Description<span class="required">*</span>
                           </label>
                              <input type="text" id="meta_description" name="meta_description" required="" class="form-control" @if(isset($seo_data) && !empty($seo_data->meta_description)) value="{{ $seo_data->meta_description }}" @endif>
                           @if ($errors->has('meta_description'))
                              <span class="text-danger">{{ $errors->first('meta_description') }}</span>
                           @endif
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-6">
                        <div class="item form-group bad">
                           <label class="col-form-label label-align" for="email"> Meta Keywords <span class="required">*</span>
                           </label>
                              <input type="text" id="meta_keywords" name="meta_keywords" required="" class="form-control" @if(isset($seo_data) && !empty($seo_data->meta_keywords)) value="{{ $seo_data->meta_keywords }}" @endif>
                           @if ($errors->has('meta_keywords'))
                              <span class="text-danger">{{ $errors->first('meta_keywords') }}</span>
                           @endif
                        </div>
                     </div>

                   
                     <div class="col-md-6 col-sm-6">
                        <div class="item form-group bad">
                           <label class="col-form-label label-align" for="email"> Canonical Tag Page URl <span class="required">*</span>
                           </label>
                              <input type="text" id="meta_canonical" name="meta_canonical" required="" class="form-control" @if(isset($seo_data) && !empty($seo_data->meta_canonical)) value="{{ $seo_data->meta_canonical }}" @endif>
                           @if ($errors->has('meta_canonical'))
                              <span class="text-danger">{{ $errors->first('meta_canonical') }}</span>
                           @endif
                        </div>
                     </div>
         

      
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Status</label>
                           <select class="custom-select" name="status" id="status">
                              <option value="">Please select status</option>
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                           </select>
                           @if ($errors->has('status'))
                              <span class="text-danger">{{ $errors->first('status') }}</span>
                           @endif
                     </div>
                  </div>

                     <div class="ln_solid"></div>
                     <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                           <button type="submit" class="btn btn-primary">Cancel</button>
                           <button id="send" type="submit" class="btn btn-success">
                              @if(isset($seo_data) && !empty($seo_data)) Update @else Submit @endif
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
