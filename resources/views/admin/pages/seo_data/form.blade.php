@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.seo_data.includes.create-form')

@endsection
@push('js')
	
	<script type="text/javascript">
		@if(isset($seo_data) && !empty($seo_data))
	   		$("#status").val("{{ $seo_data->status }}");
	   	@endif
	   	@if(isset($seo_data) && !empty($seo_data))
	   		$("#page_name").val("{{ $seo_data->page_name }}");
	   	@endif
	</script>
@endpush