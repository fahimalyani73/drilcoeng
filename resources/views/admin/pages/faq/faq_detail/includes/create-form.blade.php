<style type="text/css">
   .col-form-label {
    padding-top: calc(.375rem + 1px);
    padding-bottom: calc(.375rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5;
    display: block;
    width: 100%;
    text-align: left !important;
 }
</style>
<div class="right_col" role="main" style="min-height: 942px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>FAQ Detail</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12">
            <div class="x_panel">
               
               <div class="x_content">
                  @if(isset($faqd) && !empty($faqd))
                  
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('faq-detail.update',$faqd->id) }}" enctype="multipart/form-data">
                     @method('PUT')
                  @else
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('faq-detail.store') }}" enctype="multipart/form-data">
                  
                  @endif
                     @csrf

                     <span class="section">FAQ Detail</span>
                     <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">FAQ</label>
                           <select class="custom-select" name="faq_id" id="faq_id">
                              <option value="">Please select FAQ</option>
                              @if(isset($faq) && !empty($faq))
                              @foreach($faq as $key => $value)
                                 <option value="{{ $value->id }}"> {{ $value->question_category }} </option>
                              @endforeach
                              @endif
                           </select>
                           @if ($errors->has('faq_id'))
                              <span class="text-danger">{{ $errors->first('faq_id') }}</span>
                           @endif
                     </div>
                  </div>

                     <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Question <span class="required">*</span>
                        </label>
                           <input id="question" class="form-control" data-validate-length-range="6" data-validate-words="2" name="question" placeholder="" required="" type="text" @if(isset($faqd) && !empty($faqd->question)) value="{{ $faqd->question }}" @endif>
                        @if ($errors->has('question'))
                           <span class="text-danger">{{ $errors->first('question') }}</span>
                        @endif
                     </div>
                  </div>
      
                  
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="website">Answer <span class="required">*</span>
                        </label>
                           <input type="text" id="answer" name="answer" required="" placeholder="" class="form-control" @if(isset($faqd) && !empty($faqd->answer)) value="{{ $faqd->answer }}" @endif>
                        @if ($errors->has('answer'))
                           <span class="text-danger">{{ $errors->first('answer') }}</span>
                        @endif
                     </div>
                  </div>
                 
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Status</label>
                           <select class="custom-select" name="status" id="status">
                              <option value="">Please select status</option>
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                           </select>
                           @if ($errors->has('status'))
                              <span class="text-danger">{{ $errors->first('status') }}</span>
                           @endif
                     </div>
                  </div>

                     <div class="ln_solid"></div>
                     <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                           <button type="submit" class="btn btn-primary">Cancel</button>
                           <button id="send" type="submit" class="btn btn-success">
                              @if(isset($faqd) && !empty($faqd)) Update @else Submit @endif
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


