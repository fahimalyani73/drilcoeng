@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.faq.faq_detail.includes.create-form')

@endsection
@push('js')


   
   <script type="text/javascript">
   	@if(isset($faqd) && !empty($faqd))
      $("#status").val("{{ $faqd->status }}");
    @endif
    @if(isset($faqd) && !empty($faqd))
      $("#faq_id").val("{{ $faqd->faq_id }}");
    @endif
   </script>
   
@endpush