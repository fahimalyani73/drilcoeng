@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.faq.includes.create-form')

@endsection
@push('js')


   @if(isset($faq) && !empty($faq))
   <script type="text/javascript">
      $("#status").val("{{ $faq->status }}");
   </script>
   @endif
@endpush