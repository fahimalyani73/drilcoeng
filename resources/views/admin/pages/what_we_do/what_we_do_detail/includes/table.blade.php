<div class="right_col" role="main" style="min-height: 674px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>What we do detail</h3>
         </div>
         <div class="title_right">
            <div class="col-md-5 col-sm-5   form-group pull-right top_search">
               <div class="input-group">
                 <a href="{{ route('what-we-do-detail.create') }}" class="btn btn-primary">Add New Record</a>
               </div>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row" style="display: block;">
  
         <div class="col-md-12 col-sm-12  ">
            <div class="x_panel">
               <div class="x_title">
                 
                  <ul class="nav navbar-right panel_toolbox">
                     <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
             
                        </div>
                     </li>
                     <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
               </div>
               <div class="x_content">
          
                  <div class="table-responsive">
                     <table class="table table-striped jambo_table bulk_action">
                        <thead>
                           <tr class="headings">
                              <th>
                                #
                              </th>
                              <th>Title</th>
                              <th>Description </th>
                              <th>Icon</th>
                              <th>Waht We Do</th>
                              <th>Status</th>
                              <th>
                                 Action
                              </th>
                           </tr>
                        </thead>
                        <tbody>
                           @if(isset($what_we_do_d) && !empty($what_we_do_d))
                           @foreach($what_we_do_d as $key => $value)
                           <tr class="even pointer">
                              <td class="a-center ">
                                 {{ ++$key }}
                              </td>
                              
                              <td class=" ">{{$value->title}}</td>
                              <td> {{ $value->description}} </td>
                              <td>
                                 <img src="{{ asset('uploads/what_we_do'.'/'.$value->icon) }}" width="100px" height="100px">
                              </td>
                              <td> {{ $value->what_we_do_id }} </td>
                              <td>
                                 {{ $value->status == 1 ? 'Active' : 'Inactive'  }}
                              </td>
                              <td>
                                 <a href="{{ route('what-we-do-detail.edit',$value->id) }}" class="btn btn-primary">Edit</a>
                                 <a class="delete btn btn-danger" href="javascript::void();" onclick="event.preventDefault();
                                                     document.getElementById('delete-what-we-do-detail-{{ $value->id }}').submit();">
                                                  Delete
                                 </a>
                                <form id="delete-what-we-do-detail-{{ $value->id }}" action="{{ route('what-we-do-detail.destroy',$value->id) }}" method="POST" style="display: none;">
                                 @method('delete')
                                        @csrf
                                </form>

                              </td>
                           </tr>
                           @endforeach
                           @endif
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>