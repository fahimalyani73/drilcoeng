<style type="text/css">
   .col-form-label {
    padding-top: calc(.375rem + 1px);
    padding-bottom: calc(.375rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5;
    display: block;
    width: 100%;
    text-align: left !important;
 }
</style>
<div class="right_col" role="main" style="min-height: 942px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>What we do detail</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12">
            <div class="x_panel">
               
               <div class="x_content">
                  @if(isset($what_we_do_d) && !empty($what_we_do_d))
                  
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('what-we-do-detail.update',$what_we_do_d->id) }}" enctype="multipart/form-data">
                     @method('PUT')
                  @else
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('what-we-do-detail.store') }}" enctype="multipart/form-data">
                  
                  @endif
                     @csrf
                     
                     <span class="section">What we do detail</span>
                     <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">What We Do detail</label>
                           <select class="custom-select" name="what_we_do_id" id="what_we_do_id">
                              <option value="">Please select What We Do</option>
                              @if(isset($what_we_do) && !empty($what_we_do))
                              @foreach($what_we_do as $value)
                                 <option value="{{ $value->id }}"> {{ $value->title }} </option>
                              @endforeach
                              @endif
                              
                           </select>
                           @if ($errors->has('status'))
                              <span class="text-danger">{{ $errors->first('status') }}</span>
                           @endif
                     </div>
                  </div>
                     <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Title<span class="required">*</span>
                        </label>
                           <input id="title" class="form-control" data-validate-length-range="6" data-validate-words="2" name="title" placeholder="" required="" type="text" @if(isset($what_we_do_d) && !empty($what_we_do_d->title)) value="{{ $what_we_do_d->title }}" @endif>
                        @if ($errors->has('title'))
                           <span class="text-danger">{{ $errors->first('title') }}</span>
                        @endif
                     </div>
                  </div>
                  
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="number">description <span class="required">*</span>
                        </label>
                           <input type="text" id="description" name="description" required="" class="form-control" @if(isset($what_we_do_d) && !empty($what_we_do_d->description)) value="{{ $what_we_do_d->description }}" @endif>
                        @if ($errors->has('description'))
                           <span class="text-danger">{{ $errors->first('description') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="number">Icon <span class="required">*</span>
                        </label>
                           <input type="file" id="icon" name="icon" required="" class="form-control">
                        @if ($errors->has('icon'))
                           <span class="text-danger">{{ $errors->first('icon') }}</span>
                        @endif
                     </div>
                  </div>
                  
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Status</label>
                           <select class="custom-select" name="status" id="status">
                              <option value="">Please select status</option>
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                           </select>
                           @if ($errors->has('status'))
                              <span class="text-danger">{{ $errors->first('status') }}</span>
                           @endif
                     </div>
                  </div>

                     <div class="ln_solid"></div>
                     <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                           <button type="submit" class="btn btn-primary">Cancel</button>
                           <button id="send" type="submit" class="btn btn-success">
                              @if(isset($what_we_do_d) && !empty($what_we_do_d)) Update @else Submit @endif
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
