=]@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.what_we_do.what_we_do_detail.includes.create-form')

@endsection
@push('js')
	<script type="text/javascript">
		@if(isset($what_we_do_d) && !empty($what_we_do_d))
	   		$("#status").val("{{ $what_we_do_d->status }}");
	   	@endif
	   	@if(isset($what_we_do_d) && !empty($what_we_do_d))
	   		$("#what_we_do_id ").val("{{ $what_we_do_d->what_we_do_id  }}");
	   	@endif
	</script>
@endpush