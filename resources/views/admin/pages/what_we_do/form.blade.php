@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.what_we_do.includes.create-form')

@endsection
@push('js')
	@if(isset($what_we_do) && !empty($what_we_do))
		<script type="text/javascript">
		   $("#status").val("{{ $what_we_do->status }}");
		</script>
	@endif
@endpush