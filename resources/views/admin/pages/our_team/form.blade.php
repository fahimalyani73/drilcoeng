@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.our_team.includes.create-form')

@endsection
@push('js')
	@if(isset($our_team) && !empty($our_team))
		<script type="text/javascript">
		   $("#status").val("{{ $our_team->status }}");
		</script>
	@endif
@endpush