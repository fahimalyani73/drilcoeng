<style type="text/css">
   .col-form-label {
    padding-top: calc(.375rem + 1px);
    padding-bottom: calc(.375rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5;
    display: block;
    width: 100%;
    text-align: left !important;
 }
</style>
<div class="right_col" role="main" style="min-height: 942px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>Our Team</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12">
            <div class="x_panel">
               
               <div class="x_content">
                  @if(isset($our_team) && !empty($our_team))
                  
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('our-team.update',$our_team->id) }}" enctype="multipart/form-data">
                     @method('PUT')
                  @else
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('our-team.store') }}" enctype="multipart/form-data">
                  
                  @endif
                     @csrf
                  
                     <span class="section">Our Team</span>
                     <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Name<span class="required">*</span>
                        </label>
                           <input id="name" class="form-control" data-validate-length-range="6" data-validate-words="2" name="name" placeholder="" required="" type="text" @if(isset($our_team) && !empty($our_team->name)) value="{{ $our_team->name }}" @endif>
                        @if ($errors->has('name'))
                           <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                     </div>
                  </div>
                  
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="number">Image <span class="required">*</span>
                        </label>
                           <input type="file" id="image_name" name="image_name" required="" class="form-control">
                        @if ($errors->has('image_name'))
                           <span class="text-danger">{{ $errors->first('image_name') }}</span>
                        @endif
                     </div>
                  </div>

                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group">
                        <label class="col-form-label label-align" for="occupation">Designation<span class="required">*</span>
                        </label>
                           <input id="designation" type="text" name="designation" class="optional form-control" @if(isset($our_team) && !empty($our_team->designation)) value="{{ $our_team->designation }}" @endif>
                           @if ($errors->has('designation'))
                              <span class="text-danger">{{ $errors->first('designation') }}</span>
                           @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group">
                        <label class="col-form-label label-align" for="occupation">Description<span class="required">*</span>
                        </label>
                           <input id="description" type="text" name="description" class="optional form-control" @if(isset($our_team) && !empty($our_team->description)) value="{{ $our_team->description }}" @endif>
                           @if ($errors->has('description'))
                              <span class="text-danger">{{ $errors->first('description') }}</span>
                           @endif
                     </div>
                  </div>
                  
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">Facebook Link<span class="required">*</span>
                        </label>
                           <input type="text" id="facebook_link" name="facebook_link" required="" class="form-control" @if(isset($our_team) && !empty($our_team->facebook_link)) value="{{ $our_team->facebook_link }}" @endif>
                        @if ($errors->has('facebook_link'))
                           <span class="text-danger">{{ $errors->first('facebook_link') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="website">Instagram Link <span class="required">*</span>
                        </label>
                           <input type="text" id="instagram_link" name="instagram_link" required="" placeholder="" class="form-control" @if(isset($our_team) && !empty($our_team->instagram_link)) value="{{ $our_team->instagram_link }}" @endif>
                        @if ($errors->has('instagram_link'))
                           <span class="text-danger">{{ $errors->first('instagram_link') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group">
                        <label class="col-form-label label-align" for="occupation">Twitter Link<span class="required">*</span>
                        </label>
                           <input id="twitter_link" type="text" name="twitter_link" class="optional form-control" @if(isset($our_team) && !empty($our_team->twitter_link)) value="{{ $our_team->twitter_link }}" @endif>
                           @if ($errors->has('twitter_link'))
                              <span class="text-danger">{{ $errors->first('twitter_link') }}</span>
                           @endif
                     </div>
                  </div>
                  
                
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Status</label>
                           <select class="custom-select" name="status" id="status">
                              <option value="">Please select status</option>
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                           </select>
                           @if ($errors->has('status'))
                              <span class="text-danger">{{ $errors->first('status') }}</span>
                           @endif
                     </div>
                  </div>

                     <div class="ln_solid"></div>
                     <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                           <button type="submit" class="btn btn-primary">Cancel</button>
                           <button id="send" type="submit" class="btn btn-success">
                              @if(isset($our_team) && !empty($our_team)) Update @else Submit @endif
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
