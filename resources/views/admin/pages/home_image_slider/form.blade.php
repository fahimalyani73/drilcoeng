@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.home_image_slider.includes.create-form')

@endsection
@push('js')
	@if(isset($homes) && !empty($homes))
	
	<script type="text/javascript">
	   $("#status").val("{{ $homes->status }}");
	</script>
	@endif
@endpush