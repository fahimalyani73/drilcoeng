<style type="text/css">
   .col-form-label {
    padding-top: calc(.375rem + 1px);
    padding-bottom: calc(.375rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5;
    display: block;
    width: 100%;
    text-align: left !important;
 }
</style>
<div class="right_col" role="main" style="min-height: 942px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>Home Image Slider</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12">
            <div class="x_panel">
               
               <div class="x_content">
                  @if(isset($homes) && !empty($homes))
                  
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('home-image-slider.update',$homes->id) }}" enctype="multipart/form-data">
                     @method('PUT')
                  @else
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('home-image-slider.store') }}" enctype="multipart/form-data">
                  
                  @endif
                     @csrf
                    
                     <span class="section">Home Image Slider </span>
                     <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Heading 1<span class="required">*</span>
                        </label>
                           <input id="heading_1" class="form-control" data-validate-length-range="6" data-validate-words="2" name="heading_1" placeholder="" required="" type="text" @if(isset($homes) && !empty($homes->heading_1)) value="{{ $homes->heading_1 }}" @endif>
                        @if ($errors->has('heading_1'))
                           <span class="text-danger">{{ $errors->first('heading_1') }}</span>
                        @endif
                     </div>
                  </div>

                     <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">Heading 2<span class="required">*</span>
                        </label>
                           <input type="text" id="heading_2" name="heading_2" required="" class="form-control" @if(isset($homes) && !empty($homes->heading_2)) value="{{ $homes->heading_2 }}" @endif>
                        @if ($errors->has('heading_2'))
                           <span class="text-danger">{{ $errors->first('heading_2') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">Heading 3<span class="required">*</span>
                        </label>
                           <input type="text" id="heading_3" name="heading_3" required="" class="form-control" @if(isset($homes) && !empty($homes->heading_3)) value="{{ $homes->heading_3 }}" @endif>
                        @if ($errors->has('heading_3'))
                           <span class="text-danger">{{ $errors->first('heading_3') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">
                           Image <span class="required">*</span> (1300px X 550px)
                        </label>
                           <input type="file" id="image_name" name="image_name" required="" class="form-control">
                        @if ($errors->has('image_name'))
                           <span class="text-danger">{{ $errors->first('image_name') }}</span>
                        @endif
                     </div>
                  </div>

                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="website">Button URl <span class="required">*</span>
                        </label>
                           <input type="url" id="button_link" name="button_link" required="" placeholder="" class="form-control" @if(isset($homes) && !empty($homes->button_link)) value="{{ $homes->button_link }}" @endif>
                        @if ($errors->has('button_link'))
                           <span class="text-danger">{{ $errors->first('button_link') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group">
                        <label class="col-form-label label-align" for="occupation">Button Text<span class="required">*</span>
                        </label>
                           <input id="occupation" type="text" name="button_text" class="optional form-control" @if(isset($homes) && !empty($homes->button_text)) value="{{ $homes->button_text }}" @endif>
                           @if ($errors->has('button_text'))
                              <span class="text-danger">{{ $errors->first('button_text') }}</span>
                           @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="slider_text_color" class="col-form-label label-align">Slider Text Color</label>
                          <input type="color" id="slider_text_color" name="slider_text_color" value="#ff0000">
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="btn_bg_color" class="col-form-label label-align">Button Background Color</label>
                          <input type="color" id="btn_bg_color" name="btn_bg_color" value="#ffb600">
                     </div>
                  </div>
                  
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Status</label>
                           <select class="custom-select" name="status" id="status">
                              <option value="">Please select status</option>
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                           </select>
                           @if ($errors->has('status'))
                              <span class="text-danger">{{ $errors->first('status') }}</span>
                           @endif
                     </div>
                  </div>

                     <div class="ln_solid"></div>
                     <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                           <button type="submit" class="btn btn-primary">Cancel</button>
                           <button id="send" type="submit" class="btn btn-success">
                              @if(isset($homes) && !empty($homes)) Update @else Submit @endif
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
