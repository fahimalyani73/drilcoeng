<div class="right_col" role="main" style="min-height: 674px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            
         </div>
         <div class="title_right">
            <div class="col-md-5 col-sm-5   form-group pull-right top_search">
               <div class="input-group">
                 <a href="{{ route('home-image-slider.create') }}" class="btn btn-primary">Add New Record</a>
               </div>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row" style="display: block;">
  
         <div class="col-md-12 col-sm-12  ">
            <div class="x_panel">
               <div class="x_title">
                  <h2>Home Image Slider</h2>
                  <ul class="nav navbar-right panel_toolbox">
                     <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                         
                        </div>
                     </li>
                     <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
               </div>
               <div class="x_content">
               
                  <div class="table-responsive">
                     <table class="table table-striped jambo_table bulk_action">
                        <thead>
                           <tr class="headings">
                              <th>
                                #
                              </th>
                              <th>Heading 1 </th>
                              <th>Heading 2</th>
                              <th>Heading 3</th>
                              <th>Image </th>
                              <th>Button Text</th>
                              <th>Button URl </th>
                              <th><span class="nobr">Status</span></th>
                              <th>Text Color</th>
                              <th>Button Background Color</th>
                              <th>
                                 Action
                              </th>
                           </tr>
                        </thead>
                        <tbody>
                           @if(isset($homes) && !empty($homes))
                           @foreach($homes as $key => $value)
                           <tr class="even pointer">
                              <td class="a-center ">
                                 {{ ++$key }}
                              </td>
                              
                              <td> {{ $value->heading_1 }} </td>
                              <td> {{ $value->heading_2 }} </td>
                              <td> {{ $value->heading_3 }} </td>
                              <td>
                                 <img src="{{ asset('uploads/home_slider'.'/'.$value->image_name) }}" width="100px" height="100px">
                              </td>
                              <td>{{$value->button_text}}</td>
                              <td>{{$value->button_link}}</td>
                              <td>
                                 {{ $value->status == 1 ? 'Active' : 'Inactive'  }}
                              </td>
                              <td>{{$value->slider_text_color}}</td>
                              <td>{{$value->btn_bg_color}}</td>
                              <td>
                                 <a href="{{ route('home-image-slider.edit',$value->id) }}" class="btn btn-primary">Edit</a>
                                 <a class="delete btn btn-danger" href="javascript::void();" onclick="event.preventDefault();
                                                     document.getElementById('delete-homes-{{ $value->id }}').submit();">
                                                  Delete
                                 </a>
                                <form id="delete-homes-{{ $value->id }}" action="{{ route('home-image-slider.destroy',$value->id) }}" method="POST" style="display: none;">
                                 @method('delete')
                                        @csrf
                                </form>

                              </td>
                           </tr>
                           @endforeach
                           @endif
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>