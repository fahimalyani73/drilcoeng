@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.testimonial.includes.create-form')

@endsection
@push('js')
	@if(isset($testimonial) && !empty($testimonial))
		<script type="text/javascript">
		   $("#status").val("{{ $testimonial->status }}");
		</script>
	@endif
@endpush