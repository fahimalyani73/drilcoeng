<style type="text/css">
   .col-form-label {
    padding-top: calc(.375rem + 1px);
    padding-bottom: calc(.375rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5;
    display: block;
    width: 100%;
    text-align: left !important;
 }
</style>
<div class="right_col" role="main" style="min-height: 942px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>Testimonial</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12">
            <div class="x_panel">
               
               <div class="x_content">
                  @if(isset($testimonial) && !empty($testimonial))
                  
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('testimonial.update',$testimonial->id) }}" enctype="multipart/form-data">
                     @method('PUT')
                  @else
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('testimonial.store') }}" enctype="multipart/form-data">
                  
                  @endif
                     @csrf
                     
                     <span class="section">Testimonial</span>
                     <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Title<span class="required">*</span>
                        </label>
                           <input id="title" class="form-control" data-validate-length-range="6" data-validate-words="2" name="title" placeholder="" required="" type="text" @if(isset($testimonial) && !empty($testimonial->title)) value="{{ $testimonial->title }}" @endif>
                        @if ($errors->has('title'))
                           <span class="text-danger">{{ $errors->first('title') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Name<span class="required">*</span>
                        </label>
                           <input id="name" class="form-control" data-validate-length-range="6" data-validate-words="2" name="name" placeholder="" required="" type="text" @if(isset($testimonial) && !empty($testimonial->name)) value="{{ $testimonial->name }}" @endif>
                        @if ($errors->has('name'))
                           <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                     </div>
                  </div>
                  
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="number">Image <span class="required">*</span>
                        </label>
                           <input type="file" id="image_name" name="image_name" required="" class="form-control">
                        @if ($errors->has('image_name'))
                           <span class="text-danger">{{ $errors->first('image_name') }}</span>
                        @endif
                     </div>
                  </div>
                      <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">Designation<span class="required">*</span>
                        </label>
                           <input type="text" id="designation" name="designation" required="" class="form-control" @if(isset($testimonial) && !empty($testimonial->designation)) value="{{ $testimonial->designation }}" @endif>
                        @if ($errors->has('designation'))
                           <span class="text-danger">{{ $errors->first('designation') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="website">Description <span class="required">*</span>
                        </label>
                           <input type="url" id="description" name="description" required="" placeholder="" class="form-control" @if(isset($testimonial) && !empty($testimonial->description)) value="{{ $testimonial->description }}" @endif>
                        @if ($errors->has('description'))
                           <span class="text-danger">{{ $errors->first('description') }}</span>
                        @endif
                     </div>
                  </div>
   
                
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Status</label>
                           <select class="custom-select" name="status" id="status">
                              <option value="">Please select status</option>
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                           </select>
                           @if ($errors->has('status'))
                              <span class="text-danger">{{ $errors->first('status') }}</span>
                           @endif
                     </div>
                  </div>

                     <div class="ln_solid"></div>
                     <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                           <button type="submit" class="btn btn-primary">Cancel</button>
                           <button id="send" type="submit" class="btn btn-success">
                              @if(isset($testimonial) && !empty($testimonial)) Update @else Submit @endif
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
