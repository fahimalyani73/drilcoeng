@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.services.includes.create-form')

@endsection
@push('js')
	
	<script type="text/javascript">
		@if(isset($services) && !empty($services))
	   		$("#status").val("{{ $services->status }}");
	   	@endif
	</script>
@endpush