@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.services.you_should_know.includes.create-form')

@endsection
@push('js')
	
	<script type="text/javascript">
		@if(isset($yshk) && !empty($yshk))
	   		$("#status").val("{{ $yshk->status }}");
	   	@endif
	   	@if(isset($yshk) && !empty($yshk))
	   		$("#service_detail_id").val("{{ $yshk->service_detail_id }}");
	   	@endif
	</script>

@endpush