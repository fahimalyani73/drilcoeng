<div class="right_col" role="main" style="min-height: 674px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>You Should Know</h3>
         </div>
         <div class="title_right">
            <div class="col-md-5 col-sm-5   form-group pull-right top_search">
               <div class="input-group">
                 <a href="{{ route('you-should-know.create') }}" class="btn btn-primary">Add New Record</a>
               </div>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row" style="display: block;">
  
         <div class="col-md-12 col-sm-12  ">
            <div class="x_panel">
               <div class="x_title">
                  <h2>You Should Know</h2>
                  <ul class="nav navbar-right panel_toolbox">
                     <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                           
                        </div>
                     </li>
                     <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
               </div>
               <div class="x_content">
                  
                  <div class="table-responsive">
                     <table class="table table-striped jambo_table bulk_action">
                        <thead>
                           <tr class="headings">
                              <th>
                                #
                              </th>
                              <th>Title 1</th>
                              <th>Description 1</th>
                              <th>Title 2</th>
                              <th>Description 2</th>
                              <th>Title 3</th>
                              <th>Description 3</th>
                              <th>Title 4</th>
                              <th>Description 4</th>
                              <th>Title 5</th>
                              <th>Description 5</th>
                              <th>Service Detail</th>
                              <th>Status</th>
                              <th>
                                 Action
                              </th>
                           </tr>
                        </thead>
                        <tbody>
                           @if(isset($yshk) && !empty($yshk))
                           @foreach($yshk as $key => $value)
                           <tr class="even pointer">
                              <td class="a-center ">
                                 {{ ++$key }}
                              </td>
                              <td> {{ $value->title_1 }} </td>
                              <td> {{ $value->description_1 }} </td>
                              <td> {{ $value->title_2 }} </td>
                              <td> {{ $value->description_2 }} </td>
                              <td> {{ $value->title_3 }} </td>
                              <td> {{ $value->description_3 }} </td>
                              <td> {{ $value->title_4 }} </td>
                              <td> {{ $value->description_4 }} </td>
                              <td> {{ $value->title_5 }} </td>
                              <td> {{ $value->description_5 }} </td>
                              <td> {{ $value->service_detail_id }} </td>
                              <td>
                                 {{ $value->status == 1 ? 'Active' : 'Inactive'  }}
                              </td>
                              <td>
                                 <a href="{{ route('you-should-know.edit',$value->id) }}" class="btn btn-primary">Edit</a>
                                 <a class="delete btn btn-danger" href="javascript::void();" onclick="event.preventDefault();
                                                     document.getElementById('delete-services-{{ $value->id }}').submit();">
                                                  Delete
                                 </a>
                                <form id="delete-services-{{ $value->id }}" action="{{ route('you-should-know.destroy',$value->id) }}" method="POST" style="display: none;">
                                 @method('delete')
                                        @csrf
                                </form>

                              </td>
                           </tr>
                           @endforeach
                           @endif
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>