<style type="text/css">
   .col-form-label {
    padding-top: calc(.375rem + 1px);
    padding-bottom: calc(.375rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5;
    display: block;
    width: 100%;
    text-align: left !important;
 }
</style>
<div class="right_col" role="main" style="min-height: 942px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>You Should Know</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12">
            <div class="x_panel">
               
               <div class="x_content">
                  @if(isset($yshk) && !empty($yshk))
                  
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('you-should-know.update',$yshk->id) }}" enctype="multipart/form-data">
                     @method('PUT')
                  @else
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('you-should-know.store') }}" enctype="multipart/form-data">
                  
                  @endif
                     @csrf
                     
                     <span class="section">You Should Know</span>
                     <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Service Detail</label>
                           <select class="custom-select" name="service_detail_id" id="service_detail_id">
                              <option value="">Please Select Service Detail</option>
                              @if(isset($serviced) && !empty($serviced))
                              @foreach($serviced as $key => $value)
                                 <option value="{{ $value->id }}"> {{ $value->heading }} </option>
                              @endforeach
                              @endif
                           </select>
                           @if ($errors->has('service_detail_id'))
                              <span class="text-danger">{{ $errors->first('service_detail_id') }}</span>
                           @endif
                     </div>
                  </div>

                     <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Title 1 !<span class="required">*</span>
                        </label>
                           <input id="title_1" name="title_1" class="form-control" data-validate-length-range="6" data-validate-words="2" title_1="title_1" placeholder="" required="" type="text" @if(isset($yshk) && !empty($yshk->title_1)) value="{{ $yshk->title_1 }}" @endif>
                        @if ($errors->has('title_1'))
                           <span class="text-danger">{{ $errors->first('title_1') }}</span>
                        @endif
                     </div>
                  </div>

                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Description 1 !<span class="required">*</span>
                        </label>
                           <input id="description_1" name="description_1" class="form-control" data-validate-length-range="6" data-validate-words="2" description_1="description_1" placeholder="" required="" type="text" @if(isset($yshk) && !empty($yshk->description_1)) value="{{ $yshk->description_1 }}" @endif>
                        @if ($errors->has('description_1'))
                           <span class="text-danger">{{ $errors->first('description_1') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Title 2 !<span class="required">*</span>
                        </label>
                           <input id="title_2" name="title_2" class="form-control" data-validate-length-range="6" data-validate-words="2" title_2="title_2" placeholder="" required="" type="text" @if(isset($yshk) && !empty($yshk->title_2)) value="{{ $yshk->title_2 }}" @endif>
                        @if ($errors->has('title_2'))
                           <span class="text-danger">{{ $errors->first('title_2') }}</span>
                        @endif
                     </div>
                  </div>

                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Description 2 !<span class="required">*</span>
                        </label>
                           <input id="description_2" name="description_2" class="form-control" data-validate-length-range="6" data-validate-words="2" description_2="description_2" placeholder="" required="" type="text" @if(isset($yshk) && !empty($yshk->description_2)) value="{{ $yshk->description_2 }}" @endif>
                        @if ($errors->has('description_2'))
                           <span class="text-danger">{{ $errors->first('description_2') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Title 3 !<span class="required">*</span>
                        </label>
                           <input id="title_3" name="title_3" class="form-control" data-validate-length-range="6" data-validate-words="2" title_3="title_3" placeholder="" required="" type="text" @if(isset($yshk) && !empty($yshk->title_3)) value="{{ $yshk->title_3 }}" @endif>
                        @if ($errors->has('title_3'))
                           <span class="text-danger">{{ $errors->first('title_3') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Description 3 !<span class="required">*</span>
                        </label>
                           <input id="description_3" name="description_3" class="form-control" data-validate-length-range="6" data-validate-words="2" description_3="description_3" placeholder="" required="" type="text" @if(isset($yshk) && !empty($yshk->description_3)) value="{{ $yshk->description_3 }}" @endif>
                        @if ($errors->has('description_3'))
                           <span class="text-danger">{{ $errors->first('description_3') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Title 4 !<span class="required">*</span>
                        </label>
                           <input id="title_4" name="title_4" class="form-control" data-validate-length-range="6" data-validate-words="2" title_4="title_4" placeholder="" required="" type="text" @if(isset($yshk) && !empty($yshk->title_4)) value="{{ $yshk->title_4 }}" @endif>
                        @if ($errors->has('title_4'))
                           <span class="text-danger">{{ $errors->first('title_4') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Description 4 <span class="required">*</span>
                        </label>
                           <input id="description_4" name="description_4" class="form-control" data-validate-length-range="6" data-validate-words="2" description_4="description_4" placeholder="" required="" type="text" @if(isset($yshk) && !empty($yshk->description_4)) value="{{ $yshk->description_4 }}" @endif>
                        @if ($errors->has('description_4'))
                           <span class="text-danger">{{ $errors->first('description_4') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Title 5 <span class="required">*</span>
                        </label>
                           <input id="title_5" name="title_5" class="form-control" data-validate-length-range="6" data-validate-words="2" title_5="title_5" placeholder="" required="" type="text" @if(isset($yshk) && !empty($yshk->title_5)) value="{{ $yshk->title_5 }}" @endif>
                        @if ($errors->has('title_5'))
                           <span class="text-danger">{{ $errors->first('title_5') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Description 5 <span class="required">*</span>
                        </label>
                           <input id="description_5" name="description_5" class="form-control" data-validate-length-range="6" data-validate-words="2" description_5="description_5" placeholder="" required="" type="text" @if(isset($yshk) && !empty($yshk->description_5)) value="{{ $yshk->description_5 }}" @endif>
                        @if ($errors->has('description_5'))
                           <span class="text-danger">{{ $errors->first('description_5') }}</span>
                        @endif
                     </div>
                  </div>


                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Status</label>
                           <select class="custom-select" name="status" id="status">
                              <option value="">Please select status</option>
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                           </select>
                           @if ($errors->has('status'))
                              <span class="text-danger">{{ $errors->first('status') }}</span>
                           @endif
                     </div>
                  </div>

                     <div class="ln_solid"></div>
                     <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                           <button type="submit" class="btn btn-primary">Cancel</button>
                           <button id="send" type="submit" class="btn btn-success">
                              @if(isset($yshk) && !empty($yshk)) Update @else Submit @endif
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
