@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.services.service_detail_image_slider.includes.create-form')

@endsection
@push('js')
	
	<script type="text/javascript">
		@if(isset($servicedimgs) && !empty($servicedimgs))
	   		$("#status").val("{{ $servicedimgs->status }}");
	   	@endif
	   	@if(isset($servicedimgs) && !empty($servicedimgs))
	   		$("#service_detail_id").val("{{ $servicedimgs->service_detail_id }}");
	   	@endif
	</script>
	
@endpush