<style type="text/css">
   .col-form-label {
    padding-top: calc(.375rem + 1px);
    padding-bottom: calc(.375rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5;
    display: block;
    width: 100%;
    text-align: left !important;
 }
</style>
<div class="right_col" role="main" style="min-height: 942px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>Service Detail Image Slider</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12">
            <div class="x_panel">
               
               <div class="x_content">
                  @if(isset($servicedimgs) && !empty($servicedimgs))
                  
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('service-detail-image-slider.update',$servicedimgs->id) }}" enctype="multipart/form-data">
                     @method('PUT')
                  @else
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('service-detail-image-slider.store') }}" enctype="multipart/form-data">
                  
                  @endif
                     @csrf
                     
                     <span class="section">Service Detail Image Slider</span>
                     <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Service Detail</label>
                           <select class="custom-select" name="service_detail_id" id="service_detail_id">
                              <option value="">Please Select Service Detail</option>
                              @if(isset($serviced) && !empty($serviced))
                              @foreach($serviced as $key => $value)
                                 <option value="{{ $value->id }}"> {{ $value->heading }} </option>
                              @endforeach
                              @endif
                           </select>
                           @if ($errors->has('service_detail_id'))
                              <span class="text-danger">{{ $errors->first('service_detail_id') }}</span>
                           @endif
                     </div>
                  </div>

                     <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Name<span class="required">*</span>
                        </label>
                           <input id="name" class="form-control" data-validate-length-range="6" data-validate-words="2" name="name" placeholder="" required="" type="text" @if(isset($servicedimgs) && !empty($servicedimgs->name)) value="{{ $servicedimgs->name }}" @endif>
                        @if ($errors->has('name'))
                           <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                     </div>
                  </div>

                
      
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">
                           Image <span class="required">*</span>
                           (750px X 450px)
                        </label>
                           <input type="file" id="image_name" name="image_name" required="" class="form-control">
                        @if ($errors->has('image_name'))
                           <span class="text-danger">{{ $errors->first('image_name') }}</span>
                        @endif
                     </div>
                  </div>

                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Status</label>
                           <select class="custom-select" name="status" id="status">
                              <option value="">Please select status</option>
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                           </select>
                           @if ($errors->has('status'))
                              <span class="text-danger">{{ $errors->first('status') }}</span>
                           @endif
                     </div>
                  </div>

                     <div class="ln_solid"></div>
                     <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                           <button type="submit" class="btn btn-primary">Cancel</button>
                           <button id="send" type="submit" class="btn btn-success">
                              @if(isset($servicedimgs) && !empty($servicedimgs)) Update @else Submit @endif
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
