<style type="text/css">
   .col-form-label {
    padding-top: calc(.375rem + 1px);
    padding-bottom: calc(.375rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5;
    display: block;
    width: 100%;
    text-align: left !important;
 }
</style>
<div class="right_col" role="main" style="min-height: 942px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>What Make us Different</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12">
            <div class="x_panel">
               
               <div class="x_content">
                  @if(isset($wwmudiff) && !empty($wwmudiff))
                  
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('what-we-make-us-different.update',$wwmudiff->id) }}" enctype="multipart/form-data">
                     @method('PUT')
                  @else
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('what-we-make-us-different.store') }}" enctype="multipart/form-data">
                  
                  @endif
                     @csrf
                     
                     <span class="section">What make us different</span>
                     <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Service Detail</label>
                           <select class="custom-select" name="service_detail_id" id="service_detail_id">
                              <option value="">Please Select Service Detail</option>
                              @if(isset($serviced) && !empty($serviced))
                              @foreach($serviced as $key => $value)
                                 <option value="{{ $value->id }}"> {{ $value->heading }} </option>
                              @endforeach
                              @endif
                           </select>
                           @if ($errors->has('service_detail_id'))
                              <span class="text-danger">{{ $errors->first('service_detail_id') }}</span>
                           @endif
                     </div>
                  </div>

                     <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Description 1<span class="required">*</span>
                        </label>
                           <input id="description_1" name="description_1" class="form-control" data-validate-length-range="6" data-validate-words="2" description_1="description_1" placeholder="" required="" type="text" @if(isset($wwmudiff) && !empty($wwmudiff->description_1)) value="{{ $wwmudiff->description_1 }}" @endif>
                        @if ($errors->has('description_1'))
                           <span class="text-danger">{{ $errors->first('description_1') }}</span>
                        @endif
                     </div>
                  </div>

                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Description 2 !<span class="required">*</span>
                        </label>
                           <input id="description_2" name="description_2" class="form-control" data-validate-length-range="6" data-validate-words="2" description_2="description_2" placeholder="" required="" type="text" @if(isset($wwmudiff) && !empty($wwmudiff->description_2)) value="{{ $wwmudiff->description_2 }}" @endif>
                        @if ($errors->has('description_2'))
                           <span class="text-danger">{{ $errors->first('description_2') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Text Point 1 !<span class="required">*</span>
                        </label>
                           <input id="text_point_1" name="text_point_1" class="form-control" data-validate-length-range="6" data-validate-words="2" text_point_1="text_point_1" placeholder="" required="" type="text" @if(isset($wwmudiff) && !empty($wwmudiff->text_point_1)) value="{{ $wwmudiff->text_point_1 }}" @endif>
                        @if ($errors->has('text_point_1'))
                           <span class="text-danger">{{ $errors->first('text_point_1') }}</span>
                        @endif
                     </div>
                  </div>

                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Text Point 2 !<span class="required">*</span>
                        </label>
                           <input id="text_point_2" name="text_point_2" class="form-control" data-validate-length-range="6" data-validate-words="2" text_point_2="text_point_2" placeholder="" required="" type="text" @if(isset($wwmudiff) && !empty($wwmudiff->text_point_2)) value="{{ $wwmudiff->text_point_2 }}" @endif>
                        @if ($errors->has('text_point_2'))
                           <span class="text-danger">{{ $errors->first('text_point_2') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Text Point 3 !<span class="required">*</span>
                        </label>
                           <input id="text_point_3" name="text_point_3" class="form-control" data-validate-length-range="6" data-validate-words="2" text_point_3="text_point_3" placeholder="" required="" type="text" @if(isset($wwmudiff) && !empty($wwmudiff->text_point_3)) value="{{ $wwmudiff->text_point_3 }}" @endif>
                        @if ($errors->has('text_point_3'))
                           <span class="text-danger">{{ $errors->first('text_point_3') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Text Point 4 !<span class="required">*</span>
                        </label>
                           <input id="text_point_4" name="text_point_4" class="form-control" data-validate-length-range="6" data-validate-words="2" text_point_4="text_point_4" placeholder="" required="" type="text" @if(isset($wwmudiff) && !empty($wwmudiff->text_point_4)) value="{{ $wwmudiff->text_point_4 }}" @endif>
                        @if ($errors->has('text_point_4'))
                           <span class="text-danger">{{ $errors->first('text_point_4') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Text Point 5 !<span class="required">*</span>
                        </label>
                           <input id="text_point_5" name="text_point_5" class="form-control" data-validate-length-range="6" data-validate-words="2" text_point_5="text_point_5" placeholder="" required="" type="text" @if(isset($wwmudiff) && !empty($wwmudiff->text_point_5)) value="{{ $wwmudiff->text_point_5 }}" @endif>
                        @if ($errors->has('text_point_5'))
                           <span class="text-danger">{{ $errors->first('text_point_5') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Text Point 6 !<span class="required">*</span>
                        </label>
                           <input id="text_point_6" name="text_point_6" class="form-control" data-validate-length-range="6" data-validate-words="2" text_point_6="text_point_6" placeholder="" required="" type="text" @if(isset($wwmudiff) && !empty($wwmudiff->text_point_6)) value="{{ $wwmudiff->text_point_6 }}" @endif>
                        @if ($errors->has('text_point_6'))
                           <span class="text-danger">{{ $errors->first('text_point_6') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Text Point 7 !<span class="required">*</span>
                        </label>
                           <input id="text_point_7" name="text_point_7" class="form-control" data-validate-length-range="6" data-validate-words="2" text_point_7="text_point_7" placeholder="" required="" type="text" @if(isset($wwmudiff) && !empty($wwmudiff->text_point_7)) value="{{ $wwmudiff->text_point_7 }}" @endif>
                        @if ($errors->has('text_point_7'))
                           <span class="text-danger">{{ $errors->first('text_point_7') }}</span>
                        @endif
                     </div>
                  </div>


                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Status</label>
                           <select class="custom-select" name="status" id="status">
                              <option value="">Please select status</option>
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                           </select>
                           @if ($errors->has('status'))
                              <span class="text-danger">{{ $errors->first('status') }}</span>
                           @endif
                     </div>
                  </div>

                     <div class="ln_solid"></div>
                     <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                           <button type="submit" class="btn btn-primary">Cancel</button>
                           <button id="send" type="submit" class="btn btn-success">
                              @if(isset($wwmudiff) && !empty($wwmudiff)) Update @else Submit @endif
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
