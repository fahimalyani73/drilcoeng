@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.services.what_make_us_different.includes.create-form')

@endsection
@push('js')
	
	<script type="text/javascript">
		@if(isset($wwmudiff) && !empty($wwmudiff))
	   		$("#status").val("{{ $wwmudiff->status }}");
	   	@endif
	   	@if(isset($wwmudiff) && !empty($wwmudiff))
	   		$("#service_detail_id").val("{{ $wwmudiff->service_detail_id }}");
	   	@endif
	</script>
	
@endpush