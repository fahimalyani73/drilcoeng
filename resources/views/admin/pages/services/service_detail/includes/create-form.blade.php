<style type="text/css">
   .col-form-label {
    padding-top: calc(.375rem + 1px);
    padding-bottom: calc(.375rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5;
    display: block;
    width: 100%;
    text-align: left !important;
 }
</style>
<div class="right_col" role="main" style="min-height: 942px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>Service Detail</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12">
            <div class="x_panel">
               
               <div class="x_content">
                  @if(isset($serviced) && !empty($serviced))
                  
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('service-detail.update',$serviced->id) }}" enctype="multipart/form-data">
                     @method('PUT')
                  @else
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('service-detail.store') }}" enctype="multipart/form-data">
                  
                  @endif
                     @csrf
                    
                     <span class="section">Service Detail </span>
                     <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Service</label>
                           <select class="custom-select" name="service_id" id="service_id">
                              <option value="">Please Select Service</option>
                              @if(isset($service) && !empty($service))
                              @foreach($service as $key => $value)
                                 <option value="{{ $value->id }}">{{ $value->heading }}</option>
                              @endforeach
                              @endif
                           </select>
                           @if ($errors->has('service_id'))
                              <span class="text-danger">{{ $errors->first('service_id') }}</span>
                           @endif
                     </div>
                  </div>
                     <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Heading<span class="required">*</span>
                        </label>
                           <input id="heading" class="form-control" data-validate-length-range="6" data-validate-words="2" name="heading" placeholder="" required="" type="text" @if(isset($serviced) && !empty($serviced->heading)) value="{{ $serviced->heading }}" @endif>
                        @if ($errors->has('heading'))
                           <span class="text-danger">{{ $errors->first('heading') }}</span>
                        @endif
                     </div>
                  </div>

                     <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">Description 1<span class="required">*</span>
                        </label>
                           <input type="text" id="description_1" name="description_1" required="" class="form-control" @if(isset($serviced) && !empty($serviced->description_1)) value="{{ $serviced->description_1 }}" @endif>
                        @if ($errors->has('description_1'))
                           <span class="text-danger">{{ $errors->first('description_1') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="email">Description 2<span class="required">*</span>
                        </label>
                           <input type="text" id="description_2" name="description_2" required="" class="form-control" @if(isset($serviced) && !empty($serviced->description_2)) value="{{ $serviced->description_2 }}" @endif>
                        @if ($errors->has('description_2'))
                           <span class="text-danger">{{ $errors->first('description_2') }}</span>
                        @endif
                     </div>
                  </div>
      
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="banner_image">Banner Image <span class="required">*</span>
                        </label>
                           <input type="file" id="banner_image" name="banner_image" required="" class="form-control">
                        @if ($errors->has('banner_image'))
                           <span class="text-danger">{{ $errors->first('banner_image') }}</span>
                        @endif
                     </div>
                  </div>

               
      
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Status</label>
                           <select class="custom-select" name="status" id="status">
                              <option value="">Please select status</option>
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                           </select>
                           @if ($errors->has('status'))
                              <span class="text-danger">{{ $errors->first('status') }}</span>
                           @endif
                     </div>
                  </div>

                     <div class="ln_solid"></div>
                     <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                           <button type="submit" class="btn btn-primary">Cancel</button>
                           <button id="send" type="submit" class="btn btn-success">
                              @if(isset($serviced) && !empty($serviced)) Update @else Submit @endif
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
