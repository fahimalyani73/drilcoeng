@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.services.service_detail.includes.create-form')

@endsection
@push('js')
	
	<script type="text/javascript">
		@if(isset($serviced) && !empty($serviced))
	   		$("#status").val("{{ $serviced->status }}");
	   	@endif
	   	@if(isset($serviced) && !empty($serviced))
	   		$("#service_id").val("{{ $serviced->service_id }}");
	   	@endif
	</script>
@endpush