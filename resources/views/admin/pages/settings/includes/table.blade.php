<div class="right_col" role="main" style="min-height: 674px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>Settings</h3>
         </div>
         <div class="title_right">
            <div class="col-md-5 col-sm-5   form-group pull-right top_search">
               <div class="input-group">
                 <a href="{{ route('settings.create') }}" class="btn btn-primary">Add New Record</a>
               </div>
            </div>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row" style="display: block;">
  
         <div class="col-md-12 col-sm-12  ">
            <div class="x_panel">
               <div class="x_title">
                  <h2>Settings</h2>
                  <ul class="nav navbar-right panel_toolbox">
                     <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                           
                        </div>
                     </li>
                     <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
               </div>
               <div class="x_content">
                  
                  <div class="table-responsive">
                     <table class="table table-striped jambo_table bulk_action">
                        <thead>
                           <tr class="headings">
                              <th>
                                #
                              </th>
                              <th>Logo</th>
                              <th>Favicon</th>
                              <th>Mobile Number 1</th>
                              <th>Mobile Number 2</th>
                              <th>Address 1</th>
                              <th>Address 2</th>
                              <th>Address 3</th>
                              <th>XML Script</th>
                              <th>Footer Aboutus</th>
                              <th> Status </th>
                              <th>
                                 Action
                              </th>
                           </tr>
                        </thead>
                        <tbody>
                           @if(isset($settings) && !empty($settings))
                           @foreach($settings as $key => $value)
                           <tr class="even pointer">
                              <td class="a-center ">
                                 {{ ++$key }}
                              </td>
                              <td class=" ">
                                 <img src="{{ asset('uploads/settings'.'/'.$value->logo) }}" style="width: 100px; height: 100px;">
                              </td>
                              <td class=" ">
                                 <img src="{{ asset('uploads/settings'.'/'.$value->favicon) }}" style="width: 100px; height: 100px;">
                              </td>

                              <td>{{ $value->mobile_phone_1 }}</td>
                              <td>{{ $value->mobile_phone_2 }}</td>
                              <td>{{$value->address_1}}</td>
                              <td>{{$value->address_2}}</td>
                              <td>{{$value->address_3}}</td>
                              <td>{{$value->xml_script}}</td>
                              <td>{{$value->footer_aboutus}}</td>
                              <td>{{ $value->status == 1 ? 'Active' : 'Inactive'  }}</td>
                              <td>
                                 <a href="{{ route('settings.edit',$value->id) }}" class="btn btn-primary">Edit</a>
                                 <a class="delete btn btn-danger" href="javascript::void();" onclick="event.preventDefault();
                                                     document.getElementById('delete-settings-{{ $value->id }}').submit();">
                                                  Delete
                                 </a>
                                <form id="delete-settings-{{ $value->id }}" action="{{ route('settings.destroy',$value->id) }}" method="POST" style="display: none;">
                                 @method('delete')
                                        @csrf
                                </form>

                              </td>
                           </tr>
                           @endforeach
                           @endif
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>