<style type="text/css">
   .col-form-label {
    padding-top: calc(.375rem + 1px);
    padding-bottom: calc(.375rem + 1px);
    margin-bottom: 0;
    font-size: inherit;
    line-height: 1.5;
    display: block;
    width: 100%;
    text-align: left !important;
 }
</style>
<div class="right_col" role="main" style="min-height: 942px;">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>Settings</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12">
            <div class="x_panel">
               
               <div class="x_content">
                  @if(isset($settings) && !empty($settings))
                  
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('settings.update',$settings->id) }}" enctype="multipart/form-data">
                     @method('PUT')
                  @else
                  <form class="form-horizontal form-label-left" novalidate="" method="post" action="{{ route('settings.store') }}" enctype="multipart/form-data">
                  
                  @endif
                     @csrf
                     
                     <span class="section">Settings</span>
                     <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="name">Mobile 1
                        </label>
                           <input id="mobile_phone_1" class="form-control" data-validate-length-range="6" data-validate-words="2" name="mobile_phone_1" placeholder="" required="" type="text" @if(isset($settings) && !empty($settings->mobile_phone_1)) value="{{ $settings->mobile_phone_1 }}" @endif>
                        @if ($errors->has('mobile_phone_1'))
                           <span class="text-danger">{{ $errors->first('mobile_phone_1') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">   
                        <div class="item form-group bad">
                        <label class="col-form-label label-align" for="mobile_phone_2">Mobile 2
                        </label>
                           <input id="mobile_phone_2" class="form-control" data-validate-length-range="6" data-validate-words="2" name="mobile_phone_2" placeholder="" required="" type="text" @if(isset($settings) && !empty($settings->mobile_phone_2)) value="{{ $settings->mobile_phone_2 }}" @endif>
                        @if ($errors->has('mobile_phone_2'))
                           <span class="text-danger">{{ $errors->first('mobile_phone_2') }}</span>
                        @endif
                     </div>
                  </div>
                  
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="number">Logo (207x40) <span class="required">*</span>
                        </label>
                           <input type="file" id="logo" name="logo" required="" class="form-control">
                        @if ($errors->has('logo'))
                           <span class="text-danger">{{ $errors->first('logo') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="number">Shortcut Favicon (16x16) <span class="required">*</span>
                        </label>
                           <input type="file" id="favicon" name="favicon" required="" class="form-control">
                        @if ($errors->has('favicon'))
                           <span class="text-danger">{{ $errors->first('favicon') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="address_1">Address 1
                        </label>
                           <input type="text" id="address_1" name="address_1" required="" class="form-control" @if(isset($settings) && !empty($settings->address_1)) value="{{ $settings->address_1 }}" @endif>
                        @if ($errors->has('address_1'))
                           <span class="text-danger">{{ $errors->first('address_1') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="address_2">Address 2
                        </label>
                           <input type="text" id="address_2" name="address_2" required="" class="form-control" @if(isset($settings) && !empty($settings->address_2)) value="{{ $settings->address_2 }}" @endif>
                        @if ($errors->has('address_2'))
                           <span class="text-danger">{{ $errors->first('address_2') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="address_3">Address 3
                        </label>
                           <input type="text" id="address_3" name="address_3" required="" class="form-control" @if(isset($settings) && !empty($settings->address_3)) value="{{ $settings->address_3 }}" @endif>
                        @if ($errors->has('address_3'))
                           <span class="text-danger">{{ $errors->first('address_3') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="footer_aboutus">Footer Aboutus
                        </label>
                           <input type="text" id="footer_aboutus" name="footer_aboutus" required="" class="form-control" @if(isset($settings) && !empty($settings->footer_aboutus)) value="{{ $settings->footer_aboutus }}" @endif>
                        @if ($errors->has('footer_aboutus'))
                           <span class="text-danger">{{ $errors->first('footer_aboutus') }}</span>
                        @endif
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label class="col-form-label label-align" for="xml_script">XML Script <span class="required">*</span>
                        </label>
                           <textarea  id="descriptionxml_script" name="xml_script"  placeholder="" class="form-control">@if(isset($settings) && !empty($settings->xml_script)){{$settings->xml_script}}@endif</textarea>
                        @if ($errors->has('xml_script'))
                           <span class="text-danger">{{ $errors->first('xml_script') }}</span>
                        @endif
                     </div>
                  </div>
   
                
                  <div class="col-md-6 col-sm-6">
                     <div class="item form-group bad">
                        <label for="password" class="col-form-label label-align">Status</label>
                           <select class="custom-select" name="status" id="status">
                              <option value="">Please select status</option>
                              <option value="1">Active</option>
                              <option value="0">Inactive</option>
                           </select>
                           @if ($errors->has('status'))
                              <span class="text-danger">{{ $errors->first('status') }}</span>
                           @endif
                     </div>
                  </div>

                     <div class="ln_solid"></div>
                     <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                           <button type="submit" class="btn btn-primary">Cancel</button>
                           <button id="send" type="submit" class="btn btn-success">
                              @if(isset($settings) && !empty($settings)) Update @else Submit @endif
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
