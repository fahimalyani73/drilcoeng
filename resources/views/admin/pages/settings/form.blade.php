@extends('admin.admin-master-layout')
@push('css')

@endpush
@section('content')

@include('admin.pages.settings.includes.create-form')

@endsection
@push('js')
	@if(isset($settings) && !empty($settings))
		<script type="text/javascript">
		   $("#status").val("{{ $settings->status }}");
		</script>
	@endif
@endpush