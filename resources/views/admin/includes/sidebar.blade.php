<div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Gentelella Alela!</span></a>
                    </div>
                    <div class="clearfix"></div>

                    <div class="profile clearfix">
                        <div class="profile_pic">
                            <img src="{{ asset('admin_assets/') }}/images/img.jpg" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2>John Doe</h2>
                        </div>
                    </div>

                    <br />

                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3>General</h3>
                            <ul class="nav side-menu">
                                <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-home"></i> Dashboard </a>
                                   
                                </li>
                                <li class="@if(Request::is('admin/home-image-slider') || Request::is('admin/home-image-slider/*')
                                    || Request::is('admin/contactus') || Request::is('admin/contactus/*')
                                    || Request::is('admin/testimonial') || Request::is('admin/testimonial/*')
                                    || Request::is('admin/settings') || Request::is('admin/settings/*')) active @endif">
                                    <a><i class="fa fa-edit"></i> Main Menu <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" @if(Request::is('admin/home-image-slider') || Request::is('admin/home-image-slider/*') || Request::is('admin/contactus') || Request::is('admin/contactus/*')
                                        ||  Request::is('admin/testimonial') || Request::is('admin/testimonial/*')
                                        || Request::is('admin/settings') || Request::is('admin/settings/*')
                                        ) style="display: block;" @endif>
                                        <li class="@if(Request::is('admin/home-image-slider') || Request::is('admin/home-image-slider/*')) current-page active @endif">
                                            <a href="{{ route('home-image-slider.index') }}">Home Image Slider</a>
                                        </li>
                                         <li class="@if(Request::is('admin/contactus') || Request::is('admin/contactus/*')) current-page @endif">
                                            <a href="{{ route('contactus.index') }}">Contact Us</a>
                                        </li>
                                        <li class="@if(Request::is('admin/testimonial') || Request::is('admin/testimonial/*')) current-page @endif">
                                            <a href="{{ route('testimonial.index') }}">Testimonial</a>
                                        </li>
                                        {{-- <li class="@if(Request::is('admin/settings') || Request::is('admin/settings/*')) current-page @endif">
                                            <a href="{{ route('settings.index') }}">Settings</a>
                                        </li> --}}
                                    </ul>
                                </li>

                                <li class="@if(Request::is('admin/services') || Request::is('admin/services/*') || Request::is('admin/service-detail') || Request::is('admin/service-detail/*') || Request::is('admin/service-detail-image-slider') || Request::is('admin/service-detail-image-slider/*') || Request::is('admin/what-we-make-us-different') || Request::is('admin/what-we-make-us-different/*') || Request::is('admin/you-should-know') || Request::is('admin/you-should-know/*')) active @endif">
                                    <a><i class="fa fa-edit"></i> Services <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" @if(Request::is('admin/services') || Request::is('admin/services/*') || Request::is('admin/service-detail') || Request::is('admin/service-detail/*') || Request::is('admin/service-detail-image-slider') || Request::is('admin/service-detail-image-slider/*') || Request::is('admin/what-we-make-us-different') || Request::is('admin/what-we-make-us-different/*')  || Request::is('admin/you-should-know') || Request::is('admin/you-should-know/*')) style="display: block;" @endif>
                                        <li class="@if(Request::is('admin/services') || Request::is('admin/services/*')) current-page active @endif">
                                            <a href="{{ route('services.index') }}">Service</a>
                                        </li>

                                        <li class="@if(Request::is('admin/service-detail') || Request::is('admin/service-detail/*')) current-page active @endif">
                                            <a href="{{ route('service-detail.index') }}">Service Detail</a>
                                        </li>
                                        <li class="@if(Request::is('admin/service-detail-image-slider') || Request::is('admin/service-detail-image-slider/*')) current-page active @endif">
                                            <a href="{{ route('service-detail-image-slider.index') }}">Service Detail Image Slider</a>
                                        </li>
                                        <li class="@if(Request::is('admin/what-we-make-us-different') || Request::is('admin/what-we-make-us-different/*')) current-page active @endif">
                                            <a href="{{ route('what-we-make-us-different.index') }}">What We Make Us Different</a>
                                        </li>
                                        <li class="@if(Request::is('admin/you-should-know') || Request::is('admin/you-should-know/*')) current-page active @endif">
                                            <a href="{{ route('you-should-know.index') }}">You Should Know</a>
                                        </li>
                                      
                                    </ul>
                                </li>


                                <li class="@if(Request::is('admin/projects') || Request::is('admin/projects/*') || Request::is('admin/project-detail') || Request::is('admin/project-detail/*') || Request::is('admin/project-detail-image-slider') || Request::is('admin/project-detail-image-slider/*')) active @endif">
                                    <a><i class="fa fa-edit"></i> Projects <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" @if(Request::is('admin/projects') || Request::is('admin/projects/*') || Request::is('admin/project-detail') || Request::is('admin/project-detail/*') || Request::is('admin/project-detail-image-slider') || Request::is('admin/project-detail-image-slider/*')) style="display: block;" @endif>
                                        <li class="@if(Request::is('admin/projects') || Request::is('admin/projects/*')) current-page active @endif">
                                            <a href="{{ route('projects.index') }}">Projects</a>
                                        </li>

                                        <li class="@if(Request::is('admin/project-detail') || Request::is('admin/project-detail/*')) current-page active @endif">
                                            <a href="{{ route('project-detail.index') }}">Project Detail</a>
                                        </li>
                                        <li class="@if(Request::is('admin/project-detail-image-slider') || Request::is('admin/project-detail-image-slider/*')) current-page active @endif">
                                            <a href="{{ route('project-detail-image-slider.index') }}">Project Detail Image Slider</a>
                                        </li>
                                       
                                    </ul>
                                </li>

                                <li class="@if(Request::is('admin/faq') || Request::is('admin/faq/*') || Request::is('admin/faq-detail') || Request::is('admin/faq-detail/*')) active @endif">
                                    <a><i class="fa fa-edit"></i> FAQ <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" @if(Request::is('admin/faq') || Request::is('admin/faq/*') || Request::is('admin/faq-detail') || Request::is('admin/faq-detail/*')) style="display: block;" @endif>
                                        <li class="@if(Request::is('admin/faq') || Request::is('admin/faq/*')) current-page active @endif">
                                            <a href="{{ route('faq.index') }}">FAQ</a>
                                        </li>

                                        <li class="@if(Request::is('admin/faq-detail') || Request::is('admin/faq-detail/*')) current-page active @endif">
                                            <a href="{{ route('faq-detail.index') }}">FAQ Detail</a>
                                        </li>
                                       
                                    </ul>
                                </li>

                                <li class="@if(Request::is('admin/aboutus') || Request::is('admin/aboutus/*') || Request::is('admin/aboutus-detail') || Request::is('admin/aboutus-detail/*')) active @endif">
                                    <a><i class="fa fa-edit"></i> About Us <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" @if(Request::is('admin/aboutus') || Request::is('admin/aboutus/*') || Request::is('admin/aboutus-detail') || Request::is('admin/aboutus-detail/*')) style="display: block;" @endif>
                                        <li class="@if(Request::is('admin/aboutus') || Request::is('admin/aboutus/*')) current-page active @endif">
                                            <a href="{{ route('aboutus.index') }}">About Us</a>
                                        </li>

                                        <li class="@if(Request::is('admin/aboutus-detail') || Request::is('admin/aboutus-detail/*')) current-page active @endif">
                                            <a href="{{ route('aboutus-detail.index') }}">About Us Image Slider </a>
                                        </li>
                                       
                                    </ul>
                                </li>

                                <li class="@if(Request::is('admin/what-we-do') || Request::is('admin/what-we-do/*') || Request::is('admin/what-we-do-detail') || Request::is('admin/what-we-do-detail/*')) active @endif">
                                    <a><i class="fa fa-edit"></i> What We Do <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" @if(Request::is('admin/what-we-do') || Request::is('admin/what-we-do/*') || Request::is('admin/what-we-do-detail') || Request::is('admin/what-we-do-detail/*')) style="display: block;" @endif>
                                        <li class="@if(Request::is('admin/what-we-do') || Request::is('admin/what-we-do/*')) current-page active @endif">
                                            <a href="{{ route('what-we-do.index') }}">What We Do</a>
                                        </li>

                                        <li class="@if(Request::is('admin/what-we-do-detail') || Request::is('admin/what-we-do-detail/*')) current-page active @endif">
                                            <a href="{{ route('what-we-do-detail.index') }}"> What We Do Detail </a>
                                        </li>
                                       
                                    </ul>
                                </li>

                                <li class="@if(Request::is('admin/our-team') || Request::is('admin/our-team/*')) active @endif">
                                    <a><i class="fa fa-edit"></i> Our Team <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" @if(Request::is('admin/our-team') || Request::is('admin/our-team/*')) style="display: block;" @endif>
                                        <li class="@if(Request::is('admin/our-team') || Request::is('admin/our-team/*')) current-page active @endif">
                                            <a href="{{ route('our-team.index') }}"> Our Team </a>
                                        </li>
                                       
                                    </ul>
                                </li>
                                <li class="@if(Request::is('admin/general-info') || Request::is('admin/general-info/*')) active @endif">
                                    <a><i class="fa fa-edit"></i> General Info <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" @if(Request::is('admin/general-info') || Request::is('admin/general-info/*')) style="display: block;" @endif>
                                        <li class="@if(Request::is('admin/general-info') || Request::is('admin/general-info/*')) current-page active @endif">
                                            <a href="{{ route('general-info.index') }}"> General Info </a>
                                        </li>
                                       
                                    </ul>
                                </li>

                                <li class="@if(Request::is('admin/seo-data') || Request::is('admin/seo-data/*')) active @endif">
                                    <a><i class="fa fa-edit"></i> Seo Data <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" @if(Request::is('admin/seo-data') || Request::is('admin/seo-data/*')) style="display: block;" @endif>
                                        <li class="@if(Request::is('admin/seo-data') || Request::is('admin/seo-data/*')) current-page active @endif">
                                            <a href="{{ route('seo-data.index') }}"> Seo Data </a>
                                        </li>
                                       
                                    </ul>
                                </li>

                                <li class="@if(Request::is('admin/categories') || Request::is('admin/categories/*')) active @endif">
                                    <a><i class="fa fa-edit"></i> Product Category <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" @if(Request::is('admin/categories') || Request::is('admin/categories/*')) style="display: block;" @endif>
                                        <li class="@if(Request::is('admin/categories') || Request::is('admin/categories/*')) current-page active @endif">
                                            <a href="{{ route('categories.index') }}"> Category </a>
                                        </li>
                                       
                                    </ul>
                                </li>
                            
                                <li class="@if(Request::is('admin/products') || Request::is('admin/products/*')) active @endif">
                                    <a><i class="fa fa-edit"></i> Product <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" @if(Request::is('admin/products') || Request::is('admin/products/*')) style="display: block;" @endif>
                                        <li class="@if(Request::is('admin/products') || Request::is('admin/products/*')) current-page active @endif">
                                            <a href="{{ route('products.index') }}"> Products </a>
                                        </li>
                                       
                                    </ul>
                                </li>

                                <li class="@if(Request::is('admin/settings') || Request::is('admin/settings/*')) active @endif">
                                    <a><i class="fa fa-setting"></i> Settings <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" @if(Request::is('admin/settings') || Request::is('admin/settings/*')) style="display: block;" @endif>
                                        <li class="@if(Request::is('admin/settings') || Request::is('admin/settings/*')) current-page active @endif">
                                            <a href="{{ route('settings.index') }}"> Settings </a>
                                        </li>
                                       
                                    </ul>
                                </li>

                            </ul>
                        </div>
                       
                    </div>

                    <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>

                </div>
            </div>
