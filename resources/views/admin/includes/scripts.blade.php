</div>
    </div>
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}

    <script type="text/javascript" src="{{ asset('admin_assets/') }}/js/jquery.min.js"></script>
    <script src="{{ asset('admin_assets/') }}/vendors/jquery/dist/jquery.min.js" type="24403eabc13ef5b1d8424a32-text/javascript"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>

    <script src="{{ asset('admin_assets/') }}/vendors/bootstrap/dist/js/bootstrap.bundle.min.js" type="24403eabc13ef5b1d8424a32-text/javascript"></script>

    <script src="{{ asset('admin_assets/') }}/vendors/fastclick/lib/fastclick.js" type="24403eabc13ef5b1d8424a32-text/javascript"></script>

    <script src="{{ asset('admin_assets/') }}/vendors/nprogress/nprogress.js" type="24403eabc13ef5b1d8424a32-text/javascript"></script>

    <script src="{{ asset('admin_assets/') }}/vendors/Chart.js/dist/Chart.min.js" type="24403eabc13ef5b1d8424a32-text/javascript"></script>

    <script src="{{ asset('admin_assets/') }}/vendors/jquery-sparkline/dist/jquery.sparkline.min.js" type="24403eabc13ef5b1d8424a32-text/javascript"></script>

    <script src="{{ asset('admin_assets/') }}/vendors/Flot/jquery.flot.js" type="24403eabc13ef5b1d8424a32-text/javascript"></script>
    <script src="{{ asset('admin_assets/') }}/vendors/Flot/jquery.flot.pie.js" type="24403eabc13ef5b1d8424a32-text/javascript"></script>
    <script src="{{ asset('admin_assets/') }}/vendors/Flot/jquery.flot.time.js" type="24403eabc13ef5b1d8424a32-text/javascript"></script>
    <script src="{{ asset('admin_assets/') }}/vendors/Flot/jquery.flot.stack.js" type="24403eabc13ef5b1d8424a32-text/javascript"></script>
    <script src="{{ asset('admin_assets/') }}/vendors/Flot/jquery.flot.resize.js" type="24403eabc13ef5b1d8424a32-text/javascript"></script>

    <script src="{{ asset('admin_assets/') }}/vendors/flot.orderbars/js/jquery.flot.orderBars.js" type="24403eabc13ef5b1d8424a32-text/javascript"></script>
    <script src="{{ asset('admin_assets/') }}/vendors/flot-spline/js/jquery.flot.spline.min.js" type="24403eabc13ef5b1d8424a32-text/javascript"></script>
    <script src="{{ asset('admin_assets/') }}/vendors/flot.curvedlines/curvedLines.js" type="24403eabc13ef5b1d8424a32-text/javascript"></script>

    <script src="{{ asset('admin_assets/') }}/vendors/DateJS/build/date.js" type="24403eabc13ef5b1d8424a32-text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>

    <script src="{{ asset('admin_assets/') }}/vendors/bootstrap-daterangepicker/daterangepicker.js" type="24403eabc13ef5b1d8424a32-text/javascript"></script>
    <script src="{{ asset('admin_assets/') }}/vendors/validator/validator.js" type="14437636492191d3ec93c0b8-text/javascript"></script>
    <script src="{{ asset('admin_assets/') }}/vendors/iCheck/icheck.min.js" type="72332ff616cdedbf57e6b4a0-text/javascript"></script>

    <script src="{{ asset('admin_assets/') }}/build/js/custom.min.js" type="24403eabc13ef5b1d8424a32-text/javascript"></script>
    <script src="{{ asset('admin_assets/') }}/rocket-loader.min.js" data-cf-settings="24403eabc13ef5b1d8424a32-|49" defer=""></script>
    <script src="{{ asset('admin_assets/') }}/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js" ></script>

    @include('admin.includes.notification')
    @stack('js')

    <script type="text/javascript">
        $('#single_cal1').datetimepicker({
            format: 'YYYY-MM-DD'
        });
    </script>
</body>

</html>