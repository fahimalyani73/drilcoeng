<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> </title>

    <link href="{{ asset('admin_assets/') }}/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('admin_assets/') }}/vendors/nprogress/nprogress.css" rel="stylesheet">
    <link href="{{ asset('admin_assets/') }}/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="{{ asset('admin_assets/') }}/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <link href="{{ asset('admin_assets/') }}/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="{{ asset('website_assets/font-awesome/css/') }}/font-awesome.min.css" rel="stylesheet">
    <link href="{{ asset('admin_assets/') }}/build/css/custom.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/') }}/css/font-awesome.min.css">
    


    @stack('css')
    <style type="text/css">
     @font-face {
        font-family: "Lato-Bold";
        src: url("./assets/fonts/Lato-Bold.woff2");
    }
    @font-face {
        font-family: "Lato-Light";
        src: url("./assets/fonts/Lato-Light.woff2");
    }
    @font-face {
        font-family: "Lato-Regular";
        src: url("./assets/fonts/Lato-Regular.woff2");
    }

         body,h1,h2,h3,h4,h5,h6,p,span, a,button, input, optgroup, select, textarea {
            font-family: "Lato-Regular",Helvetica,Arial,sans-serif !important;
        }
    </style>
</head>
<body class="nav-md">
    <div class="container body">
        <div class="main_container">