<?php 

use App\Models\Service;
use App\Models\Project;
use App\Models\Setting;
use App\Models\Category;


if(!function_exists('services')){
	function services(){
		$services = Service::select('id','heading')->where('status',1)->get();
		return $services;
	}
};


if(!function_exists('projects')){
	function projects(){
		$projects = Project::select('id','title')->where('status',1)->get();
		return $projects;
	}
};

if(!function_exists('settings')){
	function settings(){
		$settings = Setting::where('status',1)->first();
		return $settings;
	}
};

if(!function_exists('product_category')){
	function product_category(){
		$p_categories = Category::select('id','cat_name')->where('status',1)->get();
		return $p_categories;
	}
};

	



/**Save Images Copies In different Sizes
     *
     * @param $img_name
     * @return string|string[]
     */

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

if(!function_exists('saveImagesCopies')){
    function saveImagesCopies($img_name,$path)
    {
        $name = pathinfo(asset($img_name),PATHINFO_FILENAME);
        $image = Image::make(asset($img_name))->fit(130, 110)->encode('webp');
        Storage::disk('local')->put($path.__DIR__.$name.'-S.webp',$image);
        $image = Image::make(asset($img_name))->fit(370, 250)->encode('webp');
        Storage::disk('local')->put($path.__DIR__.$name.'-M.webp',$image);
        $image = Image::make(asset($img_name))->fit(700, 450)->encode('webp');
        Storage::disk('local')->put($path.__DIR__.$name.'-L.webp',$image);
        $image = Image::make(asset($img_name))->fit(9030, 730)->encode('webp');
        Storage::disk('local')->put($path.__DIR__.$name.'-XL.webp' ,$image);
        return $name;
    }
}