<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaqDetail extends Model
{
    protected $table = 'faq_details';
 	protected $primaryKey = 'id';

 	public function faq(){
 		return $this->hasOne(Faq::class,'id');
 	}
}
