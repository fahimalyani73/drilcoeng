<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeImageSlider extends Model
{
    protected $table = 'home_image_sliders';
 	protected $primaryKey = 'id';
}
