<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $table = 'faqs';
 	protected $primaryKey = 'id';

 	public function faq_detail(){
 		return $this->hasMany(FaqDetail::class,'faq_id');
 	}
}
