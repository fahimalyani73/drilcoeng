<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'projects';
 	protected $primaryKey = 'id';

 	public function project_detail(){
 		return $this->hasMany(ProjectDetail::class,'project_id');
 	}
}
