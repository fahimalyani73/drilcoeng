<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceDetail extends Model
{
    protected $table = 'service_details';
 	protected $primaryKey = 'id';

 	public function service(){
 		return $this->hasOne(Service::class,'id');
 	}

 	public function service_detail_image_slider(){
 		return $this->hasMany(serviceDetailImageSlider::class,'service_detail_id');
 	}

 	public function you_should_know(){
 		return $this->hasMany(YouShouldKnow::class,'service_detail_id');
 	}

 	public function what_we_make_us_different(){
 		return $this->hasMany(WhatMakeUsDifferent::class,'service_detail_id');
 	}
}
