<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WhatMakeUsDifferent extends Model
{
    protected $table = 'what_make_us_differents';
 	protected $primaryKey = 'id';


 	public function service_detail(){
 		return $this->belongsTo(ServiceDetail::class,'id');
 	}
}
