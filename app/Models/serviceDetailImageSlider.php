<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class serviceDetailImageSlider extends Model
{
    protected $table = 'service_detail_image_sliders';
 	protected $primaryKey = 'id';

 	public function service_detail(){
 		return $this->belongsTo(ServiceDetail::class,'service_id');
 	}
}
