<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AboutUsImageSlider extends Model
{
    protected $table = 'about_us_image_sliders';
 	protected $primaryKey = 'id';
}
