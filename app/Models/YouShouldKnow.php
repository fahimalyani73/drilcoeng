<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class YouShouldKnow extends Model
{
    protected $table = 'you_should_knows';
 	protected $primaryKey = 'id';


 	public function service_detail(){
 		return $this->belongsTo(ServiceDetail::class,'id');
 	}
 	
}
