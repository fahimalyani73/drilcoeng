<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectDetailImageSlider extends Model
{
    protected $table = 'project_detail_image_sliders';
 	protected $primaryKey = 'id';

 	public function project_detail(){
 		return $this->belongsTo(ProjectDetail::class,'project_id');
 	}
}
