<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectDetail extends Model
{
    protected $table = 'project_details';
 	protected $primaryKey = 'id';

 	public function project(){
 		return $this->hasOne(Project::class,'id');
 	}

 	public function project_detail_image_slider(){
 		return $this->hasMany(ProjectDetailImageSlider::class,'project_detail_id');
 	}
}
