<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WhatWeDoDetail extends Model
{
    protected $table = 'what_we_do_details';
 	protected $primaryKey = 'id';
}
