<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'services';
 	protected $primaryKey = 'id';


 	public function service_detail(){
 		return $this->hasMany(ServiceDetail::class,'service_id');
 	}
}
