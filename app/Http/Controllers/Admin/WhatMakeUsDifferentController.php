<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ServiceDetail;
use App\Models\WhatMakeUsDifferent;
use Illuminate\Support\Facades\Session;

class WhatMakeUsDifferentController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wwmudiff = WhatMakeUsDifferent::paginate('20');
        return view('admin.pages.services.what_make_us_different.index',compact('wwmudiff'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $serviced = ServiceDetail::all();
        return view('admin.pages.services.what_make_us_different.form',compact('serviced'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
     
        $request->validate([
                'service_detail_id' => 'required',
                'status' => 'required'
            ], [
                'service_detail_id.required' => 'Banner image text is required',
                'status.required' => 'Button URl is required'
            ]);
        $wwmudiff = new WhatMakeUsDifferent();
        $wwmudiff->service_detail_id  = $request->service_detail_id ;
        $wwmudiff->description_1 = $request->description_1;
        $wwmudiff->description_2 = $request->description_2;
        $wwmudiff->text_point_1 = $request->text_point_1;
        $wwmudiff->text_point_2 = $request->text_point_2;
        $wwmudiff->text_point_3 = $request->text_point_3;
        $wwmudiff->text_point_4 = $request->text_point_4;
        $wwmudiff->text_point_5 = $request->text_point_5;
        $wwmudiff->text_point_6 = $request->text_point_6;
        $wwmudiff->text_point_7 = $request->text_point_7;
        $wwmudiff->status = $request->status;
        $wwmudiff->save();
        
        Session::flash('success','Record Save successfully !');

        return redirect()->route('what-we-make-us-different.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $serviced = ServiceDetail::all();
        $wwmudiff = WhatMakeUsDifferent::find($id);
        return view('admin.pages.services.what_make_us_different.form',compact('serviced','wwmudiff'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
                'service_detail_id' => 'required',
                'status' => 'required'
            ], [
                'service_detail_id.required' => 'Banner image text is required',
                'status.required' => 'Button URl is required'
            ]);
        $wwmudiff = WhatMakeUsDifferent::find($id);
        $wwmudiff->service_detail_id  = $request->service_detail_id ;
        $wwmudiff->description_1 = $request->description_1;
        $wwmudiff->description_2 = $request->description_2;
        $wwmudiff->text_point_1 = $request->text_point_1;
        $wwmudiff->text_point_2 = $request->text_point_2;
        $wwmudiff->text_point_3 = $request->text_point_3;
        $wwmudiff->text_point_4 = $request->text_point_4;
        $wwmudiff->text_point_5 = $request->text_point_5;
        $wwmudiff->text_point_6 = $request->text_point_6;
        $wwmudiff->text_point_7 = $request->text_point_7;
        $wwmudiff->status = $request->status;
        $wwmudiff->save();

        Session::flash('success','Record Updated successfully !');

        return redirect()->route('what-we-make-us-different.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $wwmudiff = WhatMakeUsDifferent::find($id);
        $wwmudiff->delete();
        Session::flash('info','Record Delete successfully !');

        return back();
    }
}
