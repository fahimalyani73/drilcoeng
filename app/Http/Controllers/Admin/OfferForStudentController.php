<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\OfferForStudent;
use Illuminate\Support\Facades\Session;


class OfferForStudentController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offerforstudent = OfferForStudent::paginate('20');
        return view('admin.pages.offerforstudent.index',compact('offerforstudent'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.offerforstudent.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
                'title' => 'required',
                'sub_title' => 'required',
                'button_text' => 'required',
                'button_url' => 'required',
                'status' => 'required'
            ], [
                'title.required' => 'Title is required',
                'sub_title.required' => 'sub_title is required',
                'button_text.required' => 'Button text is required',
                'button_url.required' => 'Button URl is required',
                'statue.required' => 'Status is required'
            ]);

        $offerforstudent = new offerforstudent();
        $offerforstudent->title = $request->title;
        $offerforstudent->sub_title = $request->sub_title;
        $offerforstudent->button_text = $request->button_text;
        $offerforstudent->button_url = $request->button_url;
        $offerforstudent->status = $request->status;

        $offerforstudent->save();

        Session::flash('success','Record Save successfully !');

        return redirect()->route('offerforstudent.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $offerforstudent = OfferForStudent::find($id);
        return view('admin.pages.offerforstudent.form',compact('offerforstudent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
                'title' => 'required',
                'sub_title' => 'required',
                'button_text' => 'required',
                'button_url' => 'required',
                'status' => 'required'
            ], [
                'title.required' => 'Title is required',
                'sub_title.required' => 'sub_title is required',
                'button_text.required' => 'Button text is required',
                'button_url.required' => 'Button URl is required',
                'statue.required' => 'Status is required'
            ]);

        $offerforstudent = offerforstudent::find($id);
        $offerforstudent->title = $request->title;
        $offerforstudent->sub_title = $request->sub_title;
        $offerforstudent->button_text = $request->button_text;
        $offerforstudent->button_url = $request->button_url;
        $offerforstudent->status = $request->status;

        $offerforstudent->save();

        Session::flash('success','Record Updated successfully !');

        return redirect()->route('offerforstudent.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        offerforstudent::find($id)->delete();
        
        Session::flash('info','Record Delete successfully !');

        return back();
    }
}
