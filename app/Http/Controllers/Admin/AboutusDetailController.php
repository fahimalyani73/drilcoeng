<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AboutUs;
use App\Models\AboutUsImageSlider;
use Illuminate\Support\Facades\Session;

class AboutusDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aboutusd = AboutUsImageSlider::paginate('20');
        return view('admin.pages.aboutus.aboutus_detail.index',compact('aboutusd'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $aboutus = AboutUs::all();
        return view('admin.pages.aboutus.aboutus_detail.form',compact('aboutus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
           $request->validate([
                'name' => 'required',
                'about_us_id' => 'required',
                'image_name' => 'required',
                'status' => 'required'
            ], [
                'name.required' => 'Image is required',
                'about_us_id.required' => 'Image is required',
                'image_name.required' => 'Image is required',
                'statue.required' => 'Status is required'
            ]);
        $aboutusd = new AboutUsImageSlider();
        $aboutusd->name = $request->name;
        $aboutusd->about_us_id  = $request->about_us_id;
        $aboutusd->status = $request->status;

        if ($request->file('image_name')) {
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $aboutusd->image_name = $filename;
            $path = public_path('uploads/aboutus');
            $file->move($path, $filename);
        }

        $aboutusd->save();

         Session::flash('success','Record Updated successfully !'); 
        return redirect()->route('aboutus-detail.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $aboutus = AboutUs::all();
        $aboutusd = AboutUsImageSlider::find($id);
        return view('admin.pages.aboutus.aboutus_detail.form',compact('aboutusd','aboutus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
                'name' => 'required',
                'about_us_id' => 'required',
                'status' => 'required'
            ], [
                'name.required' => 'Image is required',
                'about_us_id.required' => 'Image is required',
                'statue.required' => 'Status is required'
            ]);
        $aboutusd = AboutUsImageSlider::find($id);
        $aboutusd->name = $request->name;
        $aboutusd->about_us_id  = $request->about_us_id;
        $aboutusd->status = $request->status;

        if ($request->file('image_name')) {
            $image_name = $aboutusd->image_name;
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $aboutusd->image_name = $filename;
            $path = public_path('uploads/aboutus');
            $file->move($path, $filename);

            $file_path = public_path('uploads/aboutus' . DIRECTORY_SEPARATOR . $image_name);
            if (is_file($file_path)) {
                unlink($file_path);
            }

        }
        $aboutusd->save();

        Session::flash('success','Record Updated successfully !');
        return redirect()->route('aboutus-detail.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  
        $aboutusd = AboutUsImageSlider::find($id);
        if(!is_null($aboutusd)){
            $file_path = public_path('uploads/aboutus' . DIRECTORY_SEPARATOR . $aboutusd->image_name);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }

        $aboutusd->delete();
        Session::flash('info','Record Delete successfully !');
        return back();
    }
}
