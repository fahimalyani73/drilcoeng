<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting;
use Illuminate\Support\Facades\Session;


class SettingController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Setting::paginate('20');

        return view('admin.pages.settings.index',compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.settings.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $settings = new Setting();
        $settings->mobile_phone_1 = $request->mobile_phone_1;
        $settings->mobile_phone_2 = $request->mobile_phone_2;
        $settings->address_1 = $request->address_1;
        $settings->address_2 = $request->address_2;
        $settings->address_3 = $request->address_3;
        $settings->xml_script = $request->xml_script;
        $settings->footer_aboutus = $request->footer_aboutus;
        $settings->status = $request->status;

        if ($request->file('logo')) {
            $file = $request->logo;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $settings->logo = $filename;
            $path = public_path('uploads/settings');
            $file->move($path, $filename);
        }
        if ($request->file('favicon')) {
            $file = $request->favicon;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $settings->favicon = $filename;
            $path = public_path('uploads/settings');
            $file->move($path, $filename);
        }
        

        $settings->save();
        Session::flash('success','Record Save successfully !');
        return redirect()->route('settings.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $settings = Setting::find($id);
        return view('admin.pages.settings.form',compact('settings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $settings = Setting::find($id);
        $settings->mobile_phone_1 = $request->mobile_phone_1;
        $settings->mobile_phone_2 = $request->mobile_phone_2;
        $settings->address_1 = $request->address_1;
        $settings->address_2 = $request->address_2;
        $settings->address_3 = $request->address_3;
        $settings->xml_script = $request->xml_script;
        $settings->footer_aboutus = $request->footer_aboutus;
        $settings->status = $request->status;

        if ($request->file('logo')) {
            $logo = $settings->logo;
            $file = $request->logo;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $settings->logo = $filename;
            $path = public_path('uploads/settings');
            $file->move($path, $filename);

            if(!is_null($settings)){
            $file_path = public_path('uploads/settings' . DIRECTORY_SEPARATOR . $logo);
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }
        if ($request->file('favicon')) {
            $favicon = $settings->favicon;
            $file = $request->favicon;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $settings->favicon = $filename;
            $path = public_path('uploads/settings');
            $file->move($path, $filename);

            if(!is_null($settings)){
            $file_path = public_path('uploads/settings' . DIRECTORY_SEPARATOR . $favicon);
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }
        

        $settings->save();

        Session::flash('success','Record Updated successfully !');

        return redirect()->route('settings.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $settings = Setting::find($id);

        if(!is_null($settings)){
            $file_path = public_path('uploads/settings' . DIRECTORY_SEPARATOR . $settings->logo);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        $settings->delete();

        Session::flash('info','Record Delete successfully !');

        return back();
    }
}
