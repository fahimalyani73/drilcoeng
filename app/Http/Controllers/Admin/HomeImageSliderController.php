<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HomeImageSlider;
use Illuminate\Support\Facades\Session;

class HomeImageSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $homes = HomeImageSlider::paginate('20');
        
        return view('admin.pages.home_image_slider.index',compact('homes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.home_image_slider.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
                'image_name' => 'required',
                'heading_1' => 'required',
                'heading_2' => 'required',
                'heading_3' => 'required',
                'button_text' => 'required',
                'button_link' => 'required',
                'status' => 'required'
            ], [
                'image_name.required' => 'Title is required',
                'heading_1.required' => 'heading_1 is required',
                'heading_2.required' => 'heading 2 is required',
                'heading_3.required' => 'heading 3 is required',
                'button_text.required' => 'Button text is required',
                'button_link.required' => 'Button URl is required',
                'statue.required' => 'Status is required'
            ]);

        $homes = new HomeImageSlider();
        $homes->heading_1 = $request->heading_1;
        $homes->heading_2 = $request->heading_2;
        $homes->heading_3 = $request->heading_3;
        $homes->button_text = $request->button_text;
        $homes->button_link = $request->button_link;
        $homes->status = $request->status;
        $homes->slider_text_color = $request->slider_text_color;
        $homes->btn_bg_color = $request->btn_bg_color;
        
        if ($request->file('image_name')) {
            $image_name = $homes->image_name;
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $homes->image_name = $filename;
            $path = public_path('uploads/home_slider');
            $file->move($path, $filename);

            $file_path = public_path('uploads/home_slider' . DIRECTORY_SEPARATOR . $image_name);
            if (is_file($file_path)) {
                unlink($file_path);
            }

        }

        $homes->save();
        Session::flash('success','Record Save successfully !');
        return redirect()->route('home-image-slider.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $homes = HomeImageSlider::find($id);
        return view('admin.pages.home_image_slider.form',compact('homes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
                'heading_1' => 'required',
                'heading_2' => 'required',
                'heading_3' => 'required',
                'button_text' => 'required',
                'button_link' => 'required',
                'status' => 'required'
            ], [
                'heading_1.required' => 'heading_1 is required',
                'heading_2.required' => 'heading 2 is required',
                'heading_3.required' => 'heading 3 is required',
                'button_text.required' => 'Button text is required',
                'button_link.required' => 'Button URl is required',
                'statue.required' => 'Status is required'
            ]);

        $homes = HomeImageSlider::find($id);
        $homes->heading_1 = $request->heading_1;
        $homes->heading_2 = $request->heading_2;
        $homes->heading_3 = $request->heading_3;
        $homes->button_text = $request->button_text;
        $homes->button_link = $request->button_link;
        $homes->status = $request->status;
        $homes->slider_text_color = $request->slider_text_color;
        $homes->btn_bg_color = $request->btn_bg_color;
         
        if ($request->file('image_name')) {
            $image_name = $homes->image_name;
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $homes->image_name = $filename;
            $path = public_path('uploads/home_slider');
            $file->move($path, $filename);

            $file_path = public_path('uploads/home_slider' . DIRECTORY_SEPARATOR . $image_name);
            if (is_file($file_path)) {
                unlink($file_path);
            }

        }

        $homes->save();
        Session::flash('success','Record Updated successfully !');

        return redirect()->route('home-image-slider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $homes = HomeImageSlider::find($id);

        if(!is_null($homes)){
            $file_path = public_path('uploads/home_slider' . DIRECTORY_SEPARATOR . $homes->image);
            if (is_file($file_path)) {
                unlink($file_path);
            }
            $file_path = public_path('uploads/home_slider' . DIRECTORY_SEPARATOR . $homes->logo);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        $homes->delete();
        
        Session::flash('info','Record Delete successfully !');
        return back();
    }
}
