<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\WhatWeDo;
use Illuminate\Support\Facades\Session;

class WhatWeDoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $what_we_do = WhatWeDo::paginate('20');
        return view('admin.pages.what_we_do.index',compact('what_we_do'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.what_we_do.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

           $request->validate([
                'title' => 'required',
                'image_name' => 'required',
                'heading' => 'required',
                'status' => 'required'
            ], [
                'title.required' => 'Image is required',
                'image_name.required' => 'Image is required',
                'heading.required' => 'Image is required',
                'statue.required' => 'Status is required'
            ]);
        $what_we_do = new WhatWeDo();
        $what_we_do->title = $request->title;
        $what_we_do->heading = $request->heading;
        $what_we_do->status = $request->status;

        if ($request->file('image_name')) {
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $what_we_do->image_name = $filename;
            $path = public_path('uploads/what_we_do');
            $file->move($path, $filename);
        }

        $what_we_do->save();

         Session::flash('success','Record Updated successfully !'); 
        return redirect()->route('what-we-do.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $what_we_do = WhatWeDo::find($id);
        return view('admin.pages.what_we_do.form',compact('what_we_do'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
                'title' => 'required',
                'heading' => 'required',
                'status' => 'required'
            ], [
                'title.required' => 'Image is required',
                'heading.required' => 'Image is required',
                'statue.required' => 'Status is required'
            ]);
        $what_we_do = WhatWeDo::find($id);
        $what_we_do->title = $request->title;
        $what_we_do->heading = $request->heading;
        $what_we_do->status = $request->status;

        if ($request->file('image_name')) {
            $image_name = $what_we_do->image_name;
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $what_we_do->image_name = $filename;
            $path = public_path('uploads/what_we_do');
            $file->move($path, $filename);

            $file_path = public_path('uploads/what_we_do' . DIRECTORY_SEPARATOR . $image_name);
            if (is_file($file_path)) {
                unlink($file_path);
            }

        }
        $what_we_do->save();

        Session::flash('success','Record Updated successfully !');
        return redirect()->route('what-we-do.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  
        $what_we_do = WhatWeDo::find($id);
        if(!is_null($what_we_do)){
            $file_path = public_path('uploads/what_we_do' . DIRECTORY_SEPARATOR . $what_we_do->image_name);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        $what_we_do->delete();
        Session::flash('info','Record Delete successfully !');
        return back();
    }
}
