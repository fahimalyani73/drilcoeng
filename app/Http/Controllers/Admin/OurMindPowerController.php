<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\OurMindPower;
use Illuminate\Support\Facades\Session;

class OurMindPowerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ourmindpower = OurMindPower::paginate('20');
        return view('admin.pages.ourmindpower.index',compact('ourmindpower'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.ourmindpower.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $request->validate([
                'name' => 'required',
                'designation' => 'required',
                'image' => 'required',
                'facebook_link' => 'required',
                'twitter_link' => 'required',
                'instagram_link' => 'required',
                'youtube_link' => 'required',
                'status' => 'required'
            ], [
                'name.required' => 'name is required',
                'designation.required' => 'designation is required',
                'image.required' => 'image is required',
                'facebook_link.required' => 'facebook_linkis required',
                'twitter_link.required' => 'twitter_link is required',
                'instagram_link.required' => 'instagram_linkis required',
                'youtube_link.required' => 'youtube_link is required',
                 'statue.required' => 'Status is required',
                 
            ]);


        $ourmindpower = new OurMindPower();
        $ourmindpower->name = $request->name;
        $ourmindpower->designation= $request->designation;
        $ourmindpower->facebook_link = $request->facebook_link;
        $ourmindpower->twitter_link = $request->twitter_link;
        $ourmindpower->instagram_link = $request->instagram_link;
        $ourmindpower->youtube_link = $request->youtube_link;
        $ourmindpower->status = $request->status;

             if ($request->file('image')) {
            $file = $request->image;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $ourmindpower->image = $filename;
            $path = public_path('uploads/ourmindpower');
            $file->move($path, $filename);
        }
       
        

        $ourmindpower->save();
         Session::flash('success','Record Save successfully !');
        return redirect()->route('ourmindpower.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ourmindpower = OurMindPower::find($id);
        return view('admin.pages.ourmindpower.form',compact('ourmindpower'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
                'name' => 'required',
                'designation' => 'required',
                'facebook_link' => 'required',
                'twitter_link' => 'required',
                'instagram_link' => 'required',
                'youtube_link' => 'required',
                'status' => 'required'
            ], [
                'name.required' => 'name is required',
                'designation.required' => 'designation is required',
                'facebook_link.required' => 'facebook_linkis required',
                'twitter_link.required' => 'twitter_link is required',
                'instagram_link.required' => 'instagram_linkis required',
                'youtube_link.required' => 'youtube_link is required',
                 'statue.required' => 'status is required'
                 
            ]);

        $ourmindpower = ourmindpower::find($id);
        $ourmindpower->name = $request->name;
        $ourmindpower->designation= $request->designation;
        $ourmindpower->facebook_link = $request->facebook_link;
        $ourmindpower->twitter_link = $request->twitter_link;
        $ourmindpower->instagram_link = $request->instagram_link;
        $ourmindpower->youtube_link = $request->youtube_link;
        $ourmindpower->status = $request->status;
         if ($request->file('image')) {
            $image_name = $ourmindpower->image;
            $file = $request->image;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $ourmindpower->image = $filename;
            $path = public_path('uploads/ourmindpower');
            $file->move($path, $filename);

            $file_path = public_path('uploads/ourmindpower' . DIRECTORY_SEPARATOR . $image_name);
            if (is_file($file_path)) {
                unlink($file_path);
            }

        }

        $ourmindpower->save();
         Session::flash('success','Record Updated successfully !');
        return redirect()->route('ourmindpower.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
     {
        $ourmindpower = OurMindPower::find($id);

        if(!is_null($ourmindpower)){
            $file_path = public_path('uploads/ourmindpower' . DIRECTORY_SEPARATOR . $ourmindpower->image);
            if (is_file($file_path)) {
                unlink($file_path);
            }
                    }
        $ourmindpower->delete();
        
        Session::flash('info','Record Delete successfully !');
        return back();
    }
}
