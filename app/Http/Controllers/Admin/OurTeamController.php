<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\OurTeam;
use Illuminate\Support\Facades\Session;

class OurTeamController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $our_team = OurTeam::paginate('20');
        return view('admin.pages.our_team.index',compact('our_team'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.our_team.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
                'name' => 'required',
                'image_name' => 'required',
                'designation' => 'required',
                'description' => 'required',
                'status' => 'required'
            ], [
                'name.required' => 'Banner image is required',
                'image_name.required' => 'Banner image text is required',
                'designation.required' => 'Banner image text is required',
                'description.required' => 'Phone number is required',
                'status.required' => 'Button URl is required'
            ]);

        $our_team = new OurTeam();
        $our_team->name = $request->name;
        $our_team->designation = $request->designation;
        $our_team->description = $request->description;
        $our_team->facebook_link = $request->facebook_link;
        $our_team->twitter_link = $request->twitter_link;
        $our_team->instagram_link = $request->instagram_link;
        $our_team->status = $request->status;

        if ($request->file('image_name')) {
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $our_team->image_name = $filename;
            $path = public_path('uploads/our_team');
            $file->move($path, $filename);
        }

        $our_team->save();
        
        Session::flash('success','Record Save successfully !');

        return redirect()->route('our-team.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $our_team = OurTeam::find($id);
        return view('admin.pages.our_team.form',compact('our_team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
                'name' => 'required',
                'designation' => 'required',
                'description' => 'required',
                'status' => 'required'
            ], [
                'name.required' => 'Banner image is required',
                'designation.required' => 'Banner image text is required',
                'description.required' => 'Phone number is required',
                'status.required' => 'Button URl is required'
            ]);

        $our_team = OurTeam::find($id);
        $our_team->name = $request->name;
        $our_team->designation = $request->designation;
        $our_team->description = $request->description;
        $our_team->facebook_link = $request->facebook_link;
        $our_team->twitter_link = $request->twitter_link;
        $our_team->instagram_link = $request->instagram_link;
        $our_team->status = $request->status;

        if ($request->file('image_name')) {
            $image_name = $our_team->image_name;
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $our_team->image_name = $filename;
            $path = public_path('uploads/our_team');
            $file->move($path, $filename);

            if(!is_null($our_team)){
            $file_path = public_path('uploads/our_team' . DIRECTORY_SEPARATOR . $image_name);
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }

        $our_team->save();
        

        Session::flash('success','Record Updated successfully !');

        return redirect()->route('our-team.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $our_team = our_team::find($id);
        if(!is_null($our_team)){
            $file_path = public_path('uploads/our_team' . DIRECTORY_SEPARATOR . $our_team->image_name);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        $our_team->delete();
        Session::flash('info','Record Delete successfully !');

        return back();
    }
}
