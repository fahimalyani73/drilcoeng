<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\SeoData;


class SeoDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seo_data = SeoData::paginate('20');
        return view('admin.pages.seo_data.index',compact('seo_data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.seo_data.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        // dd($request->all());

           $request->validate([
                'page_name' => 'required',
                'meta_title' => 'required',
                'meta_description' => 'required',
                'meta_canonical' => 'required',
                'meta_keywords' => 'required',
                'status' => 'required'
            ], [
                'meta_title.required' => 'Meta title is required',
                'meta_description.required' => 'Meta Description is required',
                'meta_canonical.required' => 'Nonfollow link tag is required',
                'page_name.required' => 'Image is required',
                'meta_keywords.required' => 'Meta Keywords is required',
                'statue.required' => 'Status is required'
            ]);
        $seo_data = new SeoData();
        $seo_data->meta_title = $request->meta_title;
        $seo_data->meta_description = $request->meta_description;
        $seo_data->meta_canonical = $request->meta_canonical;
        $seo_data->page_name = $request->page_name;
        $seo_data->meta_keywords = $request->meta_keywords;
        $seo_data->status = $request->status;

        $seo_data->save();

         Session::flash('success','Record Updated successfully !'); 
        return redirect()->route('seo-data.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seo_data = SeoData::find($id);
        return view('admin.pages.seo_data.form',compact('seo_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $request->validate([
                'page_name' => 'required',
                'meta_title' => 'required',
                'meta_description' => 'required',
                'meta_canonical' => 'required',
                'meta_keywords' => 'required',
                'status' => 'required'
            ], [
                'meta_title.required' => 'Meta title is required',
                'meta_description.required' => 'Meta Description is required',
                'meta_canonical.required' => 'Nonfollow link tag is required',
                'page_name.required' => 'Image is required',
                'meta_keywords.required' => 'Meta Keywords is required',
                'statue.required' => 'Status is required'
            ]);
        $seo_data = SeoData::find($id);
        $seo_data->meta_title = $request->meta_title;
        $seo_data->meta_description = $request->meta_description;
        $seo_data->meta_canonical = $request->meta_canonical;
        $seo_data->page_name = $request->page_name;
        $seo_data->meta_keywords = $request->meta_keywords;
        $seo_data->status = $request->status;

        $seo_data->save();

        Session::flash('success','Record Updated successfully !');
        return redirect()->route('seo-data.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  
        $seo_data = SeoData::find($id);
        if(!is_null($seo_data)){
            $file_path = public_path('uploads/seo_data' . DIRECTORY_SEPARATOR . $seo_data->image_alt_tag);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        $seo_data->delete();
        Session::flash('info','Record Delete successfully !');
        return back();
    }
}
