<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GeneralInfo;
use Illuminate\Support\Facades\Session;

class GeneralInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $general_info = GeneralInfo::paginate('20');
        
        return view('admin.pages.general_info.index',compact('general_info'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.general_info.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
                'total_project_icon' => 'required',
                'total_projects' => 'required',
                'staff_member_icon' => 'required',
                'staff_member' => 'required',
                'hours_work_icon' => 'required',
                'countries_experience_icon' => 'required',
                'countries_experience' => 'required',
                'hours_work' => 'required',
                'status' => 'required'
            ], [
                'total_project_icon.required' => 'Title is required',
                'total_projects.required' => 'total_projects is required',
                'staff_member_icon.required' => 'heading 2 is required',
                'staff_member.required' => 'heading 3 is required',
                'hours_work_icon.required' => 'Button text is required',
                'countries_experience_icon.required' => 'Button URl is required',
                'countries_experience.required' => 'Button URl is required',
                'hours_work.required' => 'Button URl is required',
                'statue.required' => 'Status is required'
            ]);

        $general_info = new GeneralInfo();
        $general_info->total_projects = $request->total_projects;
        $general_info->staff_member = $request->staff_member;
        $general_info->countries_experience = $request->countries_experience;
        $general_info->hours_work = $request->hours_work;
        $general_info->status = $request->status;
         
        if ($request->file('total_project_icon')) {
            $file = $request->total_project_icon;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $general_info->total_project_icon = $filename;
            $path = public_path('uploads/general_info');
            $file->move($path, $filename);
        }
        if ($request->file('staff_member_icon')) {
            $file = $request->staff_member_icon;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $general_info->staff_member_icon = $filename;
            $path = public_path('uploads/general_info');
            $file->move($path, $filename);
        }
        if ($request->file('hours_work_icon')) {
            $file = $request->hours_work_icon;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $general_info->hours_work_icon = $filename;
            $path = public_path('uploads/general_info');
            $file->move($path, $filename);
        }
        if ($request->file('countries_experience_icon')) {
            $file = $request->countries_experience_icon;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $general_info->countries_experience_icon = $filename;
            $path = public_path('uploads/general_info');
            $file->move($path, $filename);
        }

        $general_info->save();
        Session::flash('success','Record Save successfully !');
        return redirect()->route('general-info.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $general_info = GeneralInfo::find($id);
        return view('admin.pages.general_info.form',compact('general_info'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
                'total_projects' => 'required',
                'staff_member' => 'required',
                'countries_experience' => 'required',
                'hours_work' => 'required',
                'status' => 'required'
            ], [
                'total_projects.required' => 'total_projects is required',
                'staff_member.required' => 'heading 3 is required',
                'countries_experience.required' => 'Button URl is required',
                'hours_work.required' => 'Button URl is required',
                'statue.required' => 'Status is required'
            ]);

        $general_info = GeneralInfo::find($id);
        $general_info->total_projects = $request->total_projects;
        $general_info->staff_member = $request->staff_member;
        $general_info->countries_experience = $request->countries_experience;
        $general_info->hours_work = $request->hours_work;
        $general_info->status = $request->status;
         
        if ($request->file('total_project_icon')) {
            $total_project_icon = $general_info->total_project_icon;
            $file = $request->total_project_icon;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $general_info->total_project_icon = $filename;
            $path = public_path('uploads/general_info');
            $file->move($path, $filename);

            $file_path = public_path('uploads/general_info' . DIRECTORY_SEPARATOR . $total_project_icon);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        if ($request->file('staff_member_icon')) {
            $staff_member_icon = $general_info->staff_member_icon;
            $file = $request->staff_member_icon;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $general_info->staff_member_icon = $filename;
            $path = public_path('uploads/general_info');
            $file->move($path, $filename);

            $file_path = public_path('uploads/general_info' . DIRECTORY_SEPARATOR . $staff_member_icon);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        if ($request->file('hours_work_icon')) {
            $hours_work_icon = $general_info->hours_work_icon;
            $file = $request->hours_work_icon;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $general_info->hours_work_icon = $filename;
            $path = public_path('uploads/general_info');
            $file->move($path, $filename);

            $file_path = public_path('uploads/general_info' . DIRECTORY_SEPARATOR . $hours_work_icon);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        if ($request->file('countries_experience_icon')) {
            $countries_experience_icon = $general_info->countries_experience_icon;
            $file = $request->countries_experience_icon;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $general_info->countries_experience_icon = $filename;
            $path = public_path('uploads/general_info');
            $file->move($path, $filename);

            $file_path = public_path('uploads/general_info' . DIRECTORY_SEPARATOR . $countries_experience_icon);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }

        $general_info->save();

        Session::flash('success','Record Updated successfully !');

        return redirect()->route('general-info.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $general_info = GeneralInfo::find($id);

        if(!is_null($general_info)){
            $file_path = public_path('uploads/general_info' . DIRECTORY_SEPARATOR . $general_info->total_project_icon);
            if (is_file($file_path)) {
                unlink($file_path);
            }
            $file_path = public_path('uploads/general_info' . DIRECTORY_SEPARATOR . $general_info->staff_member_icon);
            if (is_file($file_path)) {
                unlink($file_path);
            }
            $file_path = public_path('uploads/general_info' . DIRECTORY_SEPARATOR . $general_info->hours_work_icon);
            if (is_file($file_path)) {
                unlink($file_path);
            }
            $file_path = public_path('uploads/general_info' . DIRECTORY_SEPARATOR . $general_info->countries_experience_icon);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        $general_info->delete();
        
        Session::flash('info','Record Delete successfully !');
        return back();
    }
}
