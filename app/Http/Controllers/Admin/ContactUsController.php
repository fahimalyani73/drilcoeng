<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ContactUs;
use Illuminate\Support\Facades\Session;

class ContactUsController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contactus = ContactUs::paginate('20');
        return view('admin.pages.contactus.index',compact('contactus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.contactus.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
                'banner_image' => 'required',
                'title' => 'required',
                'heading' => 'required',
                'address' => 'required',
                'email_address' => 'required',
                'phone_number_1' => 'required',
                'status' => 'required'
            ], [
                'banner_image.required' => 'Banner image is required',
                'title.required' => 'Banner image text is required',
                'heading.required' => 'Phone text is required',
                'address.required' => 'Phone number is required',
                'email_address.required' => 'Email text is required',
                'phone_number_1.required' => 'Email address is required',
                'status.required' => 'Button URl is required'
            ]);

        $contactus = new ContactUs();
        $contactus->title = $request->title;
        $contactus->heading = $request->heading;
        $contactus->address = $request->address;
        $contactus->phone_number_1 = $request->phone_number_1;
        $contactus->email_address = $request->email_address;
        $contactus->phone_number_2 = $request->phone_number_2;
        $contactus->phone_number_3 = $request->phone_number_3;
        $contactus->phone_number_4 = $request->phone_number_4;
        $contactus->phone_number_5 = $request->phone_number_5;
        $contactus->status = $request->status;


        if ($request->file('banner_image')) {
            $file = $request->banner_image;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $contactus->banner_image = $filename;
            $path = public_path('uploads/contactus');
            $file->move($path, $filename);
        }
        
        $contactus->save();
        
        Session::flash('success','Record Save successfully !');

        return redirect()->route('contactus.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contactus = ContactUs::find($id);
        return view('admin.pages.contactus.form',compact('contactus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
                'title' => 'required',
                'heading' => 'required',
                'address' => 'required',
                'email_address' => 'required',
                'phone_number_1' => 'required',
                'status' => 'required'
            ], [
                'title.required' => 'Banner image text is required',
                'heading.required' => 'Phone text is required',
                'address.required' => 'Phone number is required',
                'email_address.required' => 'Email text is required',
                'phone_number_1.required' => 'Email address is required',
                'status.required' => 'Button URl is required'
            ]);

        $contactus = ContactUs::find($id);
        $contactus->title = $request->title;
        $contactus->heading = $request->heading;
        $contactus->address = $request->address;
        $contactus->phone_number_1 = $request->phone_number_1;
        $contactus->email_address = $request->email_address;
        $contactus->phone_number_2 = $request->phone_number_2;
        $contactus->phone_number_3 = $request->phone_number_3;
        $contactus->phone_number_4 = $request->phone_number_4;
        $contactus->phone_number_5 = $request->phone_number_5;
        $contactus->status = $request->status;

        if ($request->file('banner_image')) {
            $banner_image = $contactus->banner_image;
            $file = $request->banner_image;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $contactus->banner_image = $filename;
            $path = public_path('uploads/contactus');
            $file->move($path, $filename);

            if(!is_null($contactus)){
            $file_path = public_path('uploads/contactus' . DIRECTORY_SEPARATOR . $banner_image);
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }

        $contactus->save();

        Session::flash('success','Record Updated successfully !');

        return redirect()->route('contactus.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contactus = ContactUs::find($id);

        if(!is_null($contactus)){
            $file_path = public_path('uploads/contactus' . DIRECTORY_SEPARATOR . $contactus->banner_image);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        $contactus->delete();

        Session::flash('info','Record Delete successfully !');

        return back();
    }
}
