<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\School;


class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $school = School::paginate('20');
        return view('admin.pages.school.index',compact('school'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.school.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $requests = $request->validate([
                'name' => 'required',
                'logo' => 'required',
                'branch_name' => 'required',
                'best_school' => 'required',
                'featured_school' => 'required',
                'popular_school' => 'required',
                'address' => 'required',
                'history' => 'required',
                'status' => 'required'
            ], [
                'name.required' => 'name is required',
                'logo.required' => 'logo is required',
                'branch_name.required' => 'branch_name is required',
                'best_school.required' => 'best school is required',
                'featured_school.required' => 'featured school is required',
                'popular_school.required' => 'popular school is required',
                'address.required' => 'address is required',
                'history.required' => 'history is required',
                'statue.required' => 'Status is required'
            ]);

        $school = new School();
        $school->name = $request->name;
        $school->branch_name = $request->branch_name;
        $school->best_school = $request->best_school;
        $school->new_school = 1;
        $school->featured_school = $request->featured_school;
        $school->popular_school= $request->popular_school;
        $school->address = $request->address;
        $school->history = $request->history;
        $school->status = $request->status;
      
        if ($request->file('logo')) {
            $file = $request->logo;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $school->logo = $filename;
            $path = public_path('uploads/school');
            $file->move($path, $filename);
        }

        $school->save();
        
        Session::flash('success','Record Save successfully !');
        return redirect()->route('school.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $school = School::find($id);
        return view('admin.pages.school.form',compact('school'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

          $request->validate([
                'name' => 'required',
                'branch_name' => 'required',
                'best_school' => 'required',
                'featured_school' => 'required',
                'popular_school' => 'required',
                'address' => 'required',
                'history' => 'required',
                'status' => 'required'
            ], [
                'name.required' => 'name is required',
                'branch_name.required' => 'branch_name is required',
                'best_school.required' => 'best_school is required',
                'featured_school.required' => 'featured_school is required',
                'popular_school.required' => 'popular_school is required',
                'address.required' => 'address is required',
                'history.required' => 'history is required',
                'statue.required' => 'Status is required'
                 
            ]);


            $school = School::find($id);
            $school->name = $request->name;
            $school->branch_name = $request->branch_name;
            $school->best_school = $request->best_school;
            $school->featured_school = $request->featured_school;
            $school->popular_school= $request->popular_school;
            $school->address = $request->address;
            $school->history = $request->history;
            $school->status = $request->status;

       
        if ($request->file('logo')) {
            $file = $request->logo;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $school->logo = $filename;
            $path = public_path('uploads/school');
            $file->move($path, $filename);
        }

        $school->save();
        Session::flash('success','Record Updated successfully !');
        return redirect()->route('school.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    $school = School::find($id);

        if(!is_null($school)){
            $file_path = public_path('uploads/school' . DIRECTORY_SEPARATOR . $school->logo);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        $school->delete();
        
        Session::flash('info','Record Delete successfully !');
        return back();
    }
}
