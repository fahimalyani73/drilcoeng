<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Service;
use Illuminate\Support\Facades\Session;


class ServiceController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::paginate('20');
        return view('admin.pages.services.index',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.services.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
                'image_name' => 'required',
                'heading' => 'required',
                'description' => 'required',
                'icon' => 'required',
                'status' => 'required'
            ], [
                'image_name.required' => 'Sevice image is required',
                'heading.required' => 'Banner image text is required',
                'description.required' => 'Phone text is required',
                'icon.required' => 'Phone number is required',
                'status.required' => 'Button URl is required'
            ]);

        $service = new Service();
        $service->heading = $request->heading;
        $service->description = $request->description;
        $service->status = $request->status;
        $service->banner_text_color = $request->banner_text_color;

        if ($request->file('image_name')) {
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $service->image_name = $filename;
            $path = public_path('uploads/services');
            $file->move($path, $filename);
        }
        if ($request->file('banner_image_name')) {
            $file = $request->banner_image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $service->banner_image_name = $filename;
            $path = public_path('uploads/services');
            $file->move($path, $filename);
        }
        if ($request->file('icon')) {
            $file = $request->icon;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $service->icon = $filename;
            $path = public_path('uploads/services');
            $file->move($path, $filename);
        }
        
        $service->save();
        
        Session::flash('success','Record Save successfully !');

        return redirect()->route('services.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $services = Service::find($id);
        return view('admin.pages.services.form',compact('services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
                'heading' => 'required',
                'description' => 'required',
                'status' => 'required'
            ], [
                'heading.required' => 'Banner image text is required',
                'description.required' => 'Phone text is required',
                'status.required' => 'Button URl is required'
            ]);

        $service = Service::find($id);
        $service->heading = $request->heading;
        $service->description = $request->description;
        $service->status = $request->status;
        $service->banner_text_color = $request->banner_text_color;
        if ($request->file('image_name')) {
            $image_name = $service->image_name;
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $service->image_name = $filename;
            $path = public_path('uploads/services');
            $file->move($path, $filename);

            if(!is_null($service)){
            $file_path = public_path('uploads/services' . DIRECTORY_SEPARATOR . $image_name);
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }

        if ($request->file('banner_image_name')) {
            $banner_image_name = $service->banner_image_name;
            $file = $request->banner_image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $service->banner_image_name = $filename;
            $path = public_path('uploads/services');
            $file->move($path, $filename);

            if(!is_null($service)){
            $file_path = public_path('uploads/services' . DIRECTORY_SEPARATOR . $banner_image_name);
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }

        if ($request->file('icon')) {
            $icon = $service->icon;
            $file = $request->icon;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $service->icon = $filename;
            $path = public_path('uploads/services');
            $file->move($path, $filename);

            if(!is_null($service)){
            $file_path = public_path('uploads/services' . DIRECTORY_SEPARATOR . $icon);
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }

        $service->save();

        Session::flash('success','Record Updated successfully !');

        return redirect()->route('services.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::find($id);

        if(!is_null($service)){
            $file_path = public_path('uploads/services' . DIRECTORY_SEPARATOR . $service->image_name);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        if(!is_null($service)){
            $file_path = public_path('uploads/services' . DIRECTORY_SEPARATOR . $service->banner_image_name);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        $service->delete();

        Session::flash('info','Record Delete successfully !');

        return back();
    }
}
