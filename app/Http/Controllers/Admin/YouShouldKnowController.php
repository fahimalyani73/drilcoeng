<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ServiceDetail;
use App\Models\YouShouldKnow;
use Illuminate\Support\Facades\Session;

class YouShouldKnowController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $yshk = YouShouldKnow::paginate('20');
        return view('admin.pages.services.you_should_know.index',compact('yshk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $serviced = ServiceDetail::all();
        return view('admin.pages.services.you_should_know.form',compact('serviced'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
     
        $request->validate([
                'service_detail_id' => 'required',
                'status' => 'required'
            ], [
                'service_detail_id.required' => 'Banner image text is required',
                'status.required' => 'Button URl is required'
            ]);
        $yshk = new YouShouldKnow();
        $yshk->service_detail_id  = $request->service_detail_id ;
        $yshk->title_1 = $request->title_1;
        $yshk->description_1 = $request->description_1;
        $yshk->title_2 = $request->title_2;
        $yshk->description_2 = $request->description_2;
        $yshk->title_3 = $request->title_3;
        $yshk->description_3 = $request->description_3;
        $yshk->title_4 = $request->title_4;
        $yshk->description_4 = $request->description_4;
        $yshk->title_5 = $request->title_5;
        $yshk->description_5 = $request->description_5;
        $yshk->status = $request->status;
        $yshk->save();
        
        Session::flash('success','Record Save successfully !');

        return redirect()->route('you-should-know.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $serviced = ServiceDetail::all();
        $yshk = YouShouldKnow::find($id);
        return view('admin.pages.services.you_should_know.form',compact('serviced','yshk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
                'service_detail_id' => 'required',
                'status' => 'required'
            ], [
                'service_detail_id.required' => 'Banner image text is required',
                'status.required' => 'Button URl is required'
            ]);
        $yshk = YouShouldKnow::find($id);
        $yshk->service_detail_id  = $request->service_detail_id ;
        $yshk->title_1 = $request->title_1;
        $yshk->description_1 = $request->description_1;
        $yshk->title_2 = $request->title_2;
        $yshk->description_2 = $request->description_2;
        $yshk->title_3 = $request->title_3;
        $yshk->description_3 = $request->description_3;
        $yshk->title_4 = $request->title_4;
        $yshk->description_4 = $request->description_4;
        $yshk->title_5 = $request->title_5;
        $yshk->description_5 = $request->description_5;
        $yshk->status = $request->status;
        $yshk->save();

        Session::flash('success','Record Updated successfully !');

        return redirect()->route('you-should-know.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $yshk = YouShouldKnow::find($id);
        $yshk->delete();
        Session::flash('info','Record Delete successfully !');

        return back();
    }
}
