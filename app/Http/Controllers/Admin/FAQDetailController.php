<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Faq;
use App\Models\FaqDetail;
use Illuminate\Support\Facades\Session;

class FAQDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faqd = FaqDetail::paginate('20');
        // dd($faq);
        return view('admin.pages.faq.faq_detail.index',compact('faqd'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $faq = Faq::all();
        return view('admin.pages.faq.faq_detail.form',compact('faq'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
           $request->validate([
                'question' => 'required',
                'answer' => 'required',
                'faq_id' => 'required',
                'status' => 'required'
            ], [
                'question.required' => 'Image is required',
                'answer.required' => 'Title is required',
                'faq_id.required' => 'Title is required',
                'statue.required' => 'Status is required'
            ]);
        $faqd = new FaqDetail();
        $faqd->question = $request->question;
        $faqd->answer = $request->answer;
        $faqd->faq_id  = $request->faq_id ;
        $faqd->status = $request->status;
        $faqd->save();

         Session::flash('success','Record Updated successfully !'); 
        return redirect()->route('faq-detail.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faq = Faq::all();
        $faqd = FaqDetail::find($id);
        return view('admin.pages.faq.faq_detail.form',compact('faqd','faq'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
                'question' => 'required',
                'answer' => 'required',
                'faq_id' => 'required',
                'status' => 'required'
            ], [
                'question.required' => 'Image is required',
                'answer.required' => 'Title is required',
                'faq_id.required' => 'Title is required',
                'statue.required' => 'Status is required'
            ]);
        $faqd = FaqDetail::find($id);
        $faqd->question = $request->question;
        $faqd->answer = $request->answer;
        $faqd->faq_id  = $request->faq_id ;
        $faqd->status = $request->status;
        $faqd->save();

        Session::flash('success','Record Updated successfully !');
        return redirect()->route('faq-detail.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  
        $faqd = FaqDetail::find($id);
        $faqd->delete();
        Session::flash('info','Record Delete successfully !');
        return back();
    }
}
