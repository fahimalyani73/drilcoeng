<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Testimonial;
use Illuminate\Support\Facades\Session;


class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonial = Testimonial::paginate('20');

        return view('admin.pages.testimonial.index',compact('testimonial'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.testimonial.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
                'name' => 'required',
                'image_name' => 'required',
                'title' => 'required',
                'designation' => 'required',
                'description' => 'required',
                'status' => 'required'
            ], [
                'name.required' => 'Name is required',
                'image_name.required' => 'Image is required',
                'title.required' => 'title is required',
                'designation.required' => 'Designation is required',
                'description.required' => 'Description is required',
                'status.required' => 'Status is required'
            ]);

        $testimonial = new Testimonial();
        $testimonial->designation = $request->designation;
        $testimonial->title = $request->title;
        $testimonial->name = $request->name;
        $testimonial->description = $request->description;
        $testimonial->status = $request->status;

        if ($request->file('image_name')) {
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $testimonial->image_name = $filename;
            $path = public_path('uploads/testimonial');
            $file->move($path, $filename);
        }

        $testimonial->save();
        Session::flash('success','Record Save successfully !');
        return redirect()->route('testimonial.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $testimonial = Testimonial::find($id);
        return view('admin.pages.testimonial.form',compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


         $request->validate([
                'name' => 'required',
                'title' => 'required',
                'designation' => 'required',
                'description' => 'required',
                'status' => 'required'
            ], [
                'name.required' => 'Name is required',
                'title.required' => 'title is required',
                'designation.required' => 'Designation is required',
                'description.required' => 'Description is required',
                'status.required' => 'Status is required'
            ]);

        $testimonial = Testimonial::find($id);
        $testimonial->designation = $request->designation;
        $testimonial->title = $request->title;
        $testimonial->name = $request->name;
        $testimonial->description = $request->description;
        $testimonial->status = $request->status;


        if ($request->file('image_name')) {
            $image_name = $testimonial->image_name;
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $testimonial->image_name = $filename;
            $path = public_path('uploads/testimonial');
            $file->move($path, $filename);

            if(!is_null($testimonial)){
            $file_path = public_path('uploads/testimonial' . DIRECTORY_SEPARATOR . $image_name);
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }

        $testimonial->save();

        Session::flash('success','Record Updated successfully !');

        return redirect()->route('testimonial.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testimonial = Testimonial::find($id);

        if(!is_null($testimonial)){
            $file_path = public_path('uploads/testimonial' . DIRECTORY_SEPARATOR . $testimonial->image_name);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        $testimonial->delete();

        Session::flash('info','Record Delete successfully !');

        return back();
    }
}
