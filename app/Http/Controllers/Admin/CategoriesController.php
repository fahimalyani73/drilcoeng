<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Session;

class CategoriesController extends Controller
{
     
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::paginate('20');
        return view('admin.pages.category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.category.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
                'cat_name' => 'required',
                'status' => 'required'
            ], [
                'cat_name.required' => 'Banner image is required',
                'status.required' => 'Button URl is required'
            ]);

        $categories = new Category();
        $categories->cat_name = $request->cat_name;
        $categories->cat_description = $request->cat_description;
        $categories->status = $request->status;
        $categories->banner_text_color = $request->banner_text_color;

        if ($request->file('cat_image')) {
            $file = $request->cat_image;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $categories->cat_image = $filename;
            $path = public_path('uploads/categories');
            $file->move($path, $filename);
        }
        if ($request->file('banner_image_name')) {
            $file = $request->banner_image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $categories->banner_image_name = $filename;
            $path = public_path('uploads/categories');
            $file->move($path, $filename);
        }
        
        $categories->save();
        
        Session::flash('success','Record Save successfully !');

        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::find($id);
        return view('admin.pages.category.form',compact('categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
                'cat_name' => 'required',
                'status' => 'required'
            ], [
                'cat_name.required' => 'Banner image is required',
                'status.required' => 'Button URl is required'
            ]);

        $categories = Category::find($id);
        $categories->cat_name = $request->cat_name;
        $categories->cat_description = $request->cat_description;
        $categories->status = $request->status;
        $categories->banner_text_color = $request->banner_text_color;


        if ($request->file('cat_image')) {
            $cat_image = $categories->cat_image;
            $file = $request->cat_image;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $categories->cat_image = $filename;
            $path = public_path('uploads/categories');
            $file->move($path, $filename);

            if(!is_null($categories)){
            $file_path = public_path('uploads/categories' . DIRECTORY_SEPARATOR . $cat_image);
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }

        if ($request->file('banner_image_name')) {
            $banner_image_name = $categories->banner_image_name;
            $file = $request->banner_image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $categories->banner_image_name = $filename;
            $path = public_path('uploads/categories');
            $file->move($path, $filename);

            if(!is_null($categories)){
            $file_path = public_path('uploads/categories' . DIRECTORY_SEPARATOR . $banner_image_name);
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }

        $categories->save();

        Session::flash('success','Record Updated successfully !');

        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories = Category::find($id);

        if(!is_null($categories)){
            $file_path = public_path('uploads/categories' . DIRECTORY_SEPARATOR . $categories->cat_image);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        if(!is_null($categories)){
            $file_path = public_path('uploads/categories' . DIRECTORY_SEPARATOR . $categories->banner_image_name);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        
        $categories->delete();

        Session::flash('info','Record Delete successfully !');

        return back();
    }
}
