<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\WebsiteSetting;
use Illuminate\Support\Facades\Session;


class WebsiteSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = WebsiteSetting::paginate('20');
        return view('admin.pages.websitesetting.index',compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.websitesetting.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
                'title' => 'required',
                'menu_logo' => 'required',
                'footer_logo' => 'required',
                'favicon' => 'required',
                'status' => 'required'
            ], [
                'title.required' => 'Title is required',
                'menu_logo.required' => 'Menu Logo is required',
                'footer_logo.required' => 'Footer Logo is required',
                'favicon.required' => 'Favicon is required',
                'status.required' => 'Button URl is required'
            ]);

        $settings = new WebsiteSetting();
        $settings->title = $request->title;
        $settings->facebook_link = $request->facebook_link;
        $settings->instagram_link = $request->instagram_link;
        $settings->twitter_link = $request->twitter_link;
        $settings->youtube_link = $request->youtube_link;
        $settings->status = $request->status;

        if ($request->file('menu_logo')) {
            $file = $request->menu_logo;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $settings->menu_logo = $filename;
            $path = public_path('uploads/settings');
            $file->move($path, $filename);
        }
        if ($request->file('footer_logo')) {
            $file = $request->footer_logo;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $settings->footer_logo = $filename;
            $path = public_path('uploads/settings');
            $file->move($path, $filename);
        }
        if ($request->file('favicon')) {
            $file = $request->favicon;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $settings->favicon = $filename;
            $path = public_path('uploads/settings');
            $file->move($path, $filename);
        }

        $settings->save();
        Session::flash('success','Record Save successfully !');
        return redirect()->route('settings.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $settings = WebsiteSetting::find($id);
        return view('admin.pages.websitesetting.form',compact('settings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
                'title' => 'required',
                'status' => 'required'
            ], [
                'title.required' => 'Title is required',
                'status.required' => 'Status is required'
            ]);

        $settings = new WebsiteSetting();
        $settings->title = $request->title;
        $settings->facebook_link = $request->facebook_link;
        $settings->instagram_link = $request->instagram_link;
        $settings->twitter_link = $request->twitter_link;
        $settings->youtube_link = $request->youtube_link;
        $settings->status = $request->status;

        if ($request->file('menu_logo')) {
            $menu_logo = $settings->menu_logo;
            $file = $request->menu_logo;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $settings->menu_logo = $filename;
            $path = public_path('uploads/settings');
            $file->move($path, $filename);

            if(!is_null($settings)){
            $file_path = public_path('uploads/settings' . DIRECTORY_SEPARATOR . $menu_logo);
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }
        if ($request->file('footer_logo')) {
            $footer_logo = $settings->footer_logo;
            $file = $request->footer_logo;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $settings->footer_logo = $filename;
            $path = public_path('uploads/settings');
            $file->move($path, $filename);

            if(!is_null($settings)){
            $file_path = public_path('uploads/settings' . DIRECTORY_SEPARATOR . $footer_logo);
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }
        if ($request->file('favicon')) {
            $favicon = $settings->favicon;
            $file = $request->favicon;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $settings->favicon = $filename;
            $path = public_path('uploads/settings');
            $file->move($path, $filename);

            if(!is_null($settings)){
            $file_path = public_path('uploads/settings' . DIRECTORY_SEPARATOR . $favicon);
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }

        $settings->save();
        Session::flash('success','Record Updated successfully !');
        return redirect()->route('settings.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $settings = WebsiteSetting::find($id);

        if(!is_null($settings)){
            $file_path = public_path('uploads/settings' . DIRECTORY_SEPARATOR . $settings->menu_logo);
            if (is_file($file_path)) {
                unlink($file_path);
            }
            $file_path = public_path('uploads/settings' . DIRECTORY_SEPARATOR . $settings->footer_logo);
            if (is_file($file_path)) {
                unlink($file_path);
            }
            $file_path = public_path('uploads/settings' . DIRECTORY_SEPARATOR . $settings->favicon);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        $settings->delete();

        Session::flash('info','Record Delete successfully !');

        return back();
    }
}
