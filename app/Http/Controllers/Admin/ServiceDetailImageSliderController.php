<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\serviceDetailImageSlider;
use App\Models\ServiceDetail;
use App\Models\Service;
use Illuminate\Support\Facades\Session;

class ServiceDetailImageSliderController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servicedimgs = serviceDetailImageSlider::paginate('20');

        return view('admin.pages.services.service_detail_image_slider.index',compact('servicedimgs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $serviced = Service::all();
        // dd($serviced->toArray());
        return view('admin.pages.services.service_detail_image_slider.form',compact('serviced'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

    
        $request->validate([
                'service_detail_id' => 'required',
                'name' => 'required',
                'status' => 'required',
                'image_name' => 'required'
            ], [
                'service_detail_id.required' => 'Banner image text is required',
                'name.required' => 'Banner image is required',
                'status.required' => 'Button URl is required',
                'image_name.required' => 'Image Name is required'
            ]);

        $servicedimgs = new serviceDetailImageSlider();
        $servicedimgs->name = $request->name;
        $servicedimgs->service_detail_id  = $request->service_detail_id ;
        $servicedimgs->status = $request->status;

        if ($request->file('image_name')) {
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $servicedimgs->image_name = $filename;
            $path = public_path('uploads/service_detail_image_slider');
            $file->move($path, $filename);
        }

        $servicedimgs->save();
        
        Session::flash('success','Record Save successfully !');

        return redirect()->route('service-detail-image-slider.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $serviced = Service::all();
        // dd($serviced->toArray());
        $servicedimgs = serviceDetailImageSlider::find($id);
        return view('admin.pages.services.service_detail_image_slider.form',compact('servicedimgs','serviced'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
                'name' => 'required',
                'service_detail_id' => 'required',
                'status' => 'required'
            ], [
                'name.required' => 'Banner image is required',
                'service_detail_id.required' => 'Banner image text is required',
                'status.required' => 'Button URl is required'
            ]);

        $servicedimgs = serviceDetailImageSlider::find($id);
        $servicedimgs->name = $request->name;
        $servicedimgs->service_detail_id  = $request->service_detail_id ;
        $servicedimgs->status = $request->status;

        if ($request->file('image_name')) {
            $image_name = $servicedimgs->image_name;
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $servicedimgs->image_name = $filename;
            $path = public_path('uploads/service_detail_image_slider');
            $file->move($path, $filename);

            if(!is_null($servicedimgs)){
            $file_path = public_path('uploads/service_detail_image_slider' . DIRECTORY_SEPARATOR . $image_name);
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }


        $servicedimgs->save();

        Session::flash('success','Record Updated successfully !');

        return redirect()->route('service-detail-image-slider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = serviceDetailImageSlider::find($id);

        if(!is_null($service)){
            $file_path = public_path('uploads/service_detail_image_slider' . DIRECTORY_SEPARATOR . $service->image_name);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        $service->delete();

        Session::flash('info','Record Delete successfully !');

        return back();
    }
}
