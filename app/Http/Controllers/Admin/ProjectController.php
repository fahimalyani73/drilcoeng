<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Project;
use Illuminate\Support\Facades\Session;

class ProjectController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::paginate('20');
        return view('admin.pages.projects.index',compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.projects.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
                'image_name' => 'required',
                'title' => 'required',
                'type' => 'required',
                'status' => 'required'
            ], [
                'image_name.required' => 'Banner image is required',
                'title.required' => 'Banner image text is required',
                'type.required' => 'Phone text is required',
                'status.required' => 'Button URl is required'
            ]);

        $project = new Project();
        $project->title = $request->title;
        $project->type = $request->type;
        $project->status = $request->status;
        $project->banner_text_color = $request->banner_text_color;

        if ($request->file('image_name')) {
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $project->image_name = $filename;
            $path = public_path('uploads/projects');
            $file->move($path, $filename);
        }
        if ($request->file('banner_image_name')) {
            $file = $request->banner_image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $project->banner_image_name = $filename;
            $path = public_path('uploads/projects');
            $file->move($path, $filename);
        }
        
        $project->save();
        
        Session::flash('success','Record Save successfully !');

        return redirect()->route('projects.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $projects = Project::find($id);
        return view('admin.pages.projects.form',compact('projects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
                'title' => 'required',
                'type' => 'required',
                'status' => 'required'
            ], [

                'title.required' => 'Banner image text is required',
                'type.required' => 'Phone text is required',
                'status.required' => 'Button URl is required'
            ]);

        $project = Project::find($id);
        $project->title = $request->title;
        $project->type = $request->type;
        $project->status = $request->status;
        $project->banner_text_color = $request->banner_text_color;

        if ($request->file('image_name')) {
            $image_name = $project->image_name;
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $project->image_name = $filename;
            $path = public_path('uploads/projects');
            $file->move($path, $filename);

            if(!is_null($project)){
            $file_path = public_path('uploads/projects' . DIRECTORY_SEPARATOR . $image_name);
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }

        if ($request->file('banner_image_name')) {
            $banner_image_name = $project->banner_image_name;
            $file = $request->banner_image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $project->banner_image_name = $filename;
            $path = public_path('uploads/projects');
            $file->move($path, $filename);

            if(!is_null($project)){
            $file_path = public_path('uploads/projects' . DIRECTORY_SEPARATOR . $banner_image_name);
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }

        $project->save();

        Session::flash('success','Record Updated successfully !');

        return redirect()->route('projects.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::find($id);

        if(!is_null($project)){
            $file_path = public_path('uploads/projects' . DIRECTORY_SEPARATOR . $project->image_name);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        if(!is_null($project)){
            $file_path = public_path('uploads/projects' . DIRECTORY_SEPARATOR . $project->banner_image_name);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        
        $project->delete();

        Session::flash('info','Record Delete successfully !');

        return back();
    }
}
