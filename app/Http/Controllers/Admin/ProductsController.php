<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Session;
use App\Models\Product;


class ProductsController extends Controller
{
    
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $products = Product::paginate('20');

        return view('admin.pages.category.products.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.pages.category.products.form',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
                'name' => 'required',
                'status' => 'required'
            ], [
                'name.required' => 'Banner image is required',
                'status.required' => 'Button URl is required'
            ]);

        $products = new Product();
        $products->name = $request->name;
        $products->description = $request->description;
        $products->status = $request->status;
        $products->banner_text_color = $request->banner_text_color;
        $products->cat_id = $request->cat_id;

        if ($request->file('p_image')) {
            $file = $request->p_image;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $products->p_image = $filename;
            $path = public_path('uploads/products');
            $file->move($path, $filename);
        }
        if ($request->file('banner_image_name')) {
            $file = $request->banner_image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $products->banner_image_name = $filename;
            $path = public_path('uploads/products');
            $file->move($path, $filename);
        }
        
        $products->save();
        
        Session::flash('success','Record Save successfully !');

        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$categories = Category::all();
        $products = Product::find($id);
        return view('admin.pages.category.products.form',compact('categories','products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $request->validate([
                'name' => 'required',
                'status' => 'required'
            ], [
                'name.required' => 'Banner image is required',
                'status.required' => 'Button URl is required'
            ]);

        $products = Product::find($id);
        $products->name = $request->name;
        $products->description = $request->description;
        $products->status = $request->status;
        $products->banner_text_color = $request->banner_text_color;
        $products->cat_id = $request->cat_id;

        if ($request->file('p_image')) {
            $p_image = $products->p_image;
            $file = $request->p_image;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $products->p_image = $filename;
            $path = public_path('uploads/products');
            $file->move($path, $filename);

            if(!is_null($products)){
            $file_path = public_path('uploads/products' . DIRECTORY_SEPARATOR . $p_image);
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }

        if ($request->file('banner_image_name')) {
            $banner_image_name = $products->banner_image_name;
            $file = $request->banner_image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $products->banner_image_name = $filename;
            $path = public_path('uploads/products');
            $file->move($path, $filename);

            if(!is_null($products)){
            $file_path = public_path('uploads/products' . DIRECTORY_SEPARATOR . $banner_image_name);
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }

        $products->save();

        Session::flash('success','Record Updated successfully !');

        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $products = Category::find($id);

        if(!is_null($categories)){
            $file_path = public_path('uploads/products' . DIRECTORY_SEPARATOR . $products->p_image);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        if(!is_null($categories)){
            $file_path = public_path('uploads/products' . DIRECTORY_SEPARATOR . $products->banner_image_name);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        
        $categories->delete();

        Session::flash('info','Record Delete successfully !');

        return back();
    }
}
