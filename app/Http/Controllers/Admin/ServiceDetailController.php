<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ServiceDetail;
use App\Models\Service;
use Illuminate\Support\Facades\Session;

class ServiceDetailController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $serviced = ServiceDetail::paginate('20');
        return view('admin.pages.services.service_detail.index',compact('serviced'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $service = Service::all();
        return view('admin.pages.services.service_detail.form',compact('service'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
                'heading' => 'required',
                'description_1' => 'required',
                'description_2' => 'required',
                'banner_image' => 'required',
                'service_id' => 'required',
                'status' => 'required'
            ], [
                'heading.required' => 'Banner image is required',
                'description_1.required' => 'Banner image text is required',
                'description_2.required' => 'Banner image text is required',
                'banner_image.required' => 'Phone number is required',
                'service_id.required' => 'Service id is required',
                'status.required' => 'Button URl is required'
            ]);

        $serviced = new ServiceDetail();
        $serviced->heading = $request->heading;
        $serviced->description_1 = $request->description_1;
        $serviced->description_2 = $request->description_2;
        $serviced->service_id = $request->service_id;
        $serviced->status = $request->status;

        if ($request->file('banner_image')) {
            $file = $request->banner_image;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $serviced->banner_image = $filename;
            $path = public_path('uploads/service_details');
            $file->move($path, $filename);
        }
        
        $serviced->save();
        
        Session::flash('success','Record Save successfully !');

        return redirect()->route('service-detail.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::all();
        $serviced = ServiceDetail::find($id);
        return view('admin.pages.services.service_detail.form',compact('serviced','service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
                'heading' => 'required',
                'description_1' => 'required',
                'description_2' => 'required',
                'service_id' => 'required',
                'status' => 'required'
            ], [
                'heading.required' => 'Banner image is required',
                'description_1.required' => 'Banner image text is required',
                'description_2.required' => 'Banner image text is required',
                'service_id.required' => 'Service id is required',
                'status.required' => 'Button URl is required'
            ]);

        $serviced = ServiceDetail::find($id);
        $serviced->heading = $request->heading;
        $serviced->description_1 = $request->description_1;
        $serviced->description_2 = $request->description_2;
        $serviced->service_id = $request->service_id;
        $serviced->status = $request->status;

        if ($request->file('banner_image')) {
            $banner_image = $serviced->banner_image;
            $file = $request->banner_image;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $serviced->banner_image = $filename;
            $path = public_path('uploads/service_details');
            $file->move($path, $filename);

            if(!is_null($serviced)){
            $file_path = public_path('uploads/service_details' . DIRECTORY_SEPARATOR . $banner_image);
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }


        $serviced->save();

        Session::flash('success','Record Updated successfully !');

        return redirect()->route('service-detail.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $serviced = ServiceDetail::find($id);

        if(!is_null($serviced)){
            $file_path = public_path('uploads/service_details' . DIRECTORY_SEPARATOR . $serviced->banner_image);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        $serviced->delete();

        Session::flash('info','Record Delete successfully !');

        return back();
    }
}
