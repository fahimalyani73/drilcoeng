<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\OurPresence;
use Illuminate\Support\Facades\Session;
class OurPresenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ourpresence =OurPresence::paginate('20');
        return view('admin.pages.ourpresence.index',compact('ourpresence'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.ourpresence.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
                'province_name' => 'required',
                'map_iframe' => 'required',
                'latitudes' => 'required',
                'longitudes' => 'required',
                'status' => 'required',
                'address' => 'required'
          
            ], [
                'province_name.required' => 'Province_name is required',
                'map_iframe.required' => 'map_iframe is required',
                'latitudes.required' => 'latitudes is required',
                'longitudes.required' => 'longitudes is required',
                'address.required' => 'Image is required',
                 'statue.required' => 'Status is required'
                 
            ]);

        $ourpresence = new OurPresence();
        $ourpresence ->province_name = $request->province_name;
        $ourpresence ->map_iframe = $request->map_iframe;
        $ourpresence ->longitudes = $request->longitudes;
        $ourpresence ->latitudes = $request->latitudes;
        $ourpresence ->address = $request->address;
        $ourpresence ->status = $request->status;
        $ourpresence ->save();
        Session::flash('success','Record Save successfully !');
        return redirect()->route('ourpresence.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ourpresence = OurPresence::find($id);
        return view('admin.pages.ourpresence.form',compact('ourpresence'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $request->validate([
                'province_name' => 'required',
                'map_iframe' => 'required',
                'latitudes' => 'required',
                'longitudes' => 'required',
                'status' => 'required',
                'address' => 'required'
          
            ], [
                'province_name.required' => 'Province_name is required',
                'map_iframe.required' => 'map_iframe is required',
                'latitudes.required' => 'latitudes is required',
                'longitudes.required' => 'longitudes is required',
                'address.required' => 'Image is required',
                 'statue.required' => 'Status is required'
                 
            ]);
        
        $ourpresence  = OurPresence::find($id);
        $ourpresence ->province_name = $request->province_name;
        $ourpresence ->map_iframe = $request->map_iframe;
        $ourpresence ->longitudes = $request->longitudes;
        $ourpresence ->latitudes = $request->latitudes;
        $ourpresence ->address = $request->address;
        $ourpresence ->status = $request->status;

        $ourpresence ->save();
         Session::flash('success','Record Updated successfully !');
        return redirect()->route('ourpresence.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        OurPresence::find($id)->delete();

        return back();
    }
}
