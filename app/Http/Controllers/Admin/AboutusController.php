<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AboutUs;
use Illuminate\Support\Facades\Session;

class AboutusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aboutus = AboutUs::paginate('20');
        return view('admin.pages.aboutus.index',compact('aboutus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.aboutus.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

           $request->validate([
                'title' => 'required',
                'banner_image' => 'required',
                'description_1' => 'required',
                'description_2' => 'required',
                'description_3' => 'required',
                'status' => 'required'
            ], [
                'title.required' => 'Image is required',
                'banner_image.required' => 'Image is required',
                'description_1.required' => 'Image is required',
                'description_2.required' => 'Image is required',
                'description_3.required' => 'Image is required',
                'statue.required' => 'Status is required'
            ]);
        $aboutus = new AboutUs();
        $aboutus->title = $request->title;
        $aboutus->description_1 = $request->description_1;
        $aboutus->description_2 = $request->description_2;
        $aboutus->description_3 = $request->description_3;
        $aboutus->title = $request->title;
        $aboutus->status = $request->status;

        if ($request->file('banner_image')) {
            $file = $request->banner_image;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $aboutus->banner_image = $filename;
            $path = public_path('uploads/aboutus');
            $file->move($path, $filename);
        }

        $aboutus->save();

         Session::flash('success','Record Updated successfully !'); 
        return redirect()->route('aboutus.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $aboutus = AboutUs::find($id);
        return view('admin.pages.aboutus.form',compact('aboutus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
                'title' => 'required',
                'description_1' => 'required',
                'description_2' => 'required',
                'description_3' => 'required',
                'status' => 'required'
            ], [
                'title.required' => 'Image is required',
                'description_1.required' => 'Image is required',
                'description_2.required' => 'Image is required',
                'description_3.required' => 'Image is required',
                'statue.required' => 'Status is required'
            ]);
        $aboutus = AboutUs::find($id);
        $aboutus->title = $request->title;
        $aboutus->description_1 = $request->description_1;
        $aboutus->description_2 = $request->description_2;
        $aboutus->description_3 = $request->description_3;
        $aboutus->title = $request->title;
        $aboutus->status = $request->status;

        if ($request->file('banner_image')) {
            $banner_image = $aboutus->banner_image;
            $file = $request->banner_image;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $aboutus->banner_image = $filename;
            $path = public_path('uploads/aboutus');
            $file->move($path, $filename);

            $file_path = public_path('uploads/aboutus' . DIRECTORY_SEPARATOR . $banner_image);
            if (is_file($file_path)) {
                unlink($file_path);
            }

        }
        $aboutus->save();

        Session::flash('success','Record Updated successfully !');
        return redirect()->route('aboutus.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  
        $aboutus = AboutUs::find($id);
        $aboutus->delete();
        Session::flash('info','Record Delete successfully !');
        return back();
    }
}
