<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProjectDetail;
use App\Models\Project;
use Illuminate\Support\Facades\Session;

class ProjectDetailController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projectd = ProjectDetail::paginate('20');
        return view('admin.pages.projects.project_detail.index',compact('projectd'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projects = Project::all();
        return view('admin.pages.projects.project_detail.form',compact('projects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
                'title' => 'required',
                'client_detail' => 'required',
                'architect' => 'required',
                'location_detail' => 'required',
                'size_detail' => 'required',
                'year_completed' => 'required',
                'categories' => 'required',
                'countries_experience_icon' => 'required',
                'countries_experience' => 'required',
                'project_id' => 'required',
                'status' => 'required'
            ], [
                'title.required' => 'Banner image is required',
                'client_detail.required' => 'Banner image text is required',
                'architect.required' => 'Banner image text is required',
                'location_detail.required' => 'location detail is required',
                'size_detail.required' => 'size detail is required',
                'year_completed.required' => 'year completed is required',
                'categories.required' => 'categories is required',
                'countries_experience_icon.required' => 'countries experience icon is required',
                'countries_experience.required' => 'countries_ experience is required',
                'project_id.required' => 'Service id is required',
                'status.required' => 'Button URl is required'
            ]);

        $projectd = new ProjectDetail();
        $projectd->title = $request->title;
        $projectd->client_detail = $request->client_detail;
        $projectd->architect = $request->architect;
        $projectd->location_detail = $request->location_detail;
        $projectd->size_detail = $request->size_detail;
        $projectd->year_completed = date("Y-m-d", strtotime($request->year_completed));
        $projectd->categories = $request->categories;
        $projectd->countries_experience_icon = $request->countries_experience_icon;
        $projectd->countries_experience = $request->countries_experience;
        $projectd->project_id = $request->project_id;
        $projectd->status = $request->status;
        $projectd->save();
        
        Session::flash('success','Record Save successfully !');

        return redirect()->route('project-detail.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $projects = Project::all();
        $projectd = ProjectDetail::find($id);
        return view('admin.pages.projects.project_detail.form',compact('projectd','projects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
                'title' => 'required',
                'client_detail' => 'required',
                'architect' => 'required',
                'location_detail' => 'required',
                'size_detail' => 'required',
                'year_completed' => 'required',
                'categories' => 'required',
                'countries_experience_icon' => 'required',
                'countries_experience' => 'required',
                'project_id' => 'required',
                'status' => 'required'
            ], [
                'title.required' => 'Banner image is required',
                'client_detail.required' => 'Banner image text is required',
                'architect.required' => 'Banner image text is required',
                'location_detail.required' => 'Phone number is required',
                'size_detail.required' => 'Phone number is required',
                'year_completed.required' => 'Phone number is required',
                'categories.required' => 'Phone number is required',
                'countries_experience_icon.required' => 'Phone number is required',
                'countries_experience.required' => 'Phone number is required',
                'project_id.required' => 'Service id is required',
                'status.required' => 'Button URl is required'
            ]);

        $projectd = ProjectDetail::find($id);
        $projectd->title = $request->title;
        $projectd->client_detail = $request->client_detail;
        $projectd->architect = $request->architect;
        $projectd->location_detail = $request->location_detail;
        $projectd->size_detail = $request->size_detail;
        $projectd->year_completed = date("Y-m-d", strtotime($request->year_completed));
        $projectd->categories = $request->categories;
        $projectd->countries_experience_icon = $request->countries_experience_icon;
        $projectd->countries_experience = $request->countries_experience;
        $projectd->project_id = $request->project_id;
        $projectd->status = $request->status;
        $projectd->save();
        

        Session::flash('success','Record Updated successfully !');

        return redirect()->route('project-detail.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $projectd = ProjectDetail::find($id);
        $projectd->delete();
        Session::flash('info','Record Delete successfully !');

        return back();
    }
}
