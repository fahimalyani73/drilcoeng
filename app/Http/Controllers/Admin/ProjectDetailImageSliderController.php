<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProjectDetailImageSlider;
use App\Models\ProjectDetail;
use Illuminate\Support\Facades\Session;

class ProjectDetailImageSliderController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projectdimgs = ProjectDetailImageSlider::paginate('20');

        return view('admin.pages.projects.project_detail_image_slider.index',compact('projectdimgs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projectd = ProjectDetail::all();
        return view('admin.pages.projects.project_detail_image_slider.form',compact('projectd'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

    
        $request->validate([
                'project_detail_id' => 'required',
                'name' => 'required',
                'status' => 'required',
                'image_name' => 'required'
            ], [
                'project_detail_id.required' => 'Banner image text is required',
                'name.required' => 'Banner image is required',
                'status.required' => 'Button URl is required',
                'image_name.required' => 'Image Name is required'
            ]);

        $projectdimgs = new ProjectDetailImageSlider();
        $projectdimgs->name = $request->name;
        $projectdimgs->project_detail_id  = $request->project_detail_id ;
        $projectdimgs->status = $request->status;

        if ($request->file('image_name')) {
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $projectdimgs->image_name = $filename;
            $path = public_path('uploads/project_detail_image_slider');
            $file->move($path, $filename);
        }

        $projectdimgs->save();
        
        Session::flash('success','Record Save successfully !');

        return redirect()->route('project-detail-image-slider.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $projectd = ProjectDetail::all();
        $projectdimgs = ProjectDetailImageSlider::find($id);
        return view('admin.pages.projects.project_detail_image_slider.form',compact('projectdimgs','projectd'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
                'project_detail_id' => 'required',
                'name' => 'required',
                'status' => 'required'
            ], [
                'project_detail_id.required' => 'Banner image text is required',
                'name.required' => 'Banner image is required',
                'status.required' => 'Button URl is required'
            ]);

        $projectdimgs = ProjectDetailImageSlider::find($id);
        $projectdimgs->name = $request->name;
        $projectdimgs->project_detail_id  = $request->project_detail_id ;
        $projectdimgs->status = $request->status;

        if ($request->file('image_name')) {
            $image_name = $projectdimgs->image_name;
            $file = $request->image_name;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $projectdimgs->image_name = $filename;
            $path = public_path('uploads/project_detail_image_slider');
            $file->move($path, $filename);

            if(!is_null($projectdimgs)){
            $file_path = public_path('uploads/project_detail_image_slider' . DIRECTORY_SEPARATOR . $image_name);
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
        }


        $projectdimgs->save();

        Session::flash('success','Record Updated successfully !');

        return redirect()->route('project-detail-image-slider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $projectdimgs = ProjectDetailImageSlider::find($id);

        if(!is_null($projectdimgs)){
            $file_path = public_path('uploads/project_detail_image_slider' . DIRECTORY_SEPARATOR . $projectdimgs->image_name);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        $projectdimgs->delete();

        Session::flash('info','Record Delete successfully !');

        return back();
    }
}
