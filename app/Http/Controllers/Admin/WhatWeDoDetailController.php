<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\WhatWeDo;
use App\Models\WhatWeDoDetail;
use Illuminate\Support\Facades\Session;

class WhatWeDoDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $what_we_do_d = WhatWeDoDetail::paginate('20');
        return view('admin.pages.what_we_do.what_we_do_detail.index',compact('what_we_do_d'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $what_we_do = WhatWeDo::all();
        return view('admin.pages.what_we_do.what_we_do_detail.form',compact('what_we_do'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

           $request->validate([
                'title' => 'required',
                'icon' => 'required',
                'description' => 'required',
                'what_we_do_id' => 'required',
                'status' => 'required'
            ], [
                'title.required' => 'Image is required',
                'icon.required' => 'Image is required',
                'description.required' => 'Image is required',
                'what_we_do_id.required' => 'Image is required',
                'statue.required' => 'Status is required'
            ]);
        $what_we_do_d = new WhatWeDoDetail();
        $what_we_do_d->title = $request->title;
        $what_we_do_d->description = $request->description;
        $what_we_do_d->what_we_do_id = $request->what_we_do_id;
        $what_we_do_d->status = $request->status;

        if ($request->file('icon')) {
            $file = $request->icon;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $what_we_do_d->icon = $filename;
            $path = public_path('uploads/what_we_do');
            $file->move($path, $filename);
        }
        
        $what_we_do_d->save();

         Session::flash('success','Record Updated successfully !'); 
        return redirect()->route('what-we-do-detail.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $what_we_do = WhatWeDo::all();
        $what_we_do_d = WhatWeDoDetail::find($id);
        return view('admin.pages.what_we_do.what_we_do_detail.form',compact('what_we_do','what_we_do_d'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
                'title' => 'required',
                'description' => 'required',
                'what_we_do_id' => 'required',
                'status' => 'required'
            ], [
                'title.required' => 'Image is required',
                'description.required' => 'Image is required',
                'what_we_do_id.required' => 'Image is required',
                'statue.required' => 'Status is required'
            ]);
        $what_we_do_d = WhatWeDoDetail::find($id);
        $what_we_do_d->title = $request->title;
        $what_we_do_d->description = $request->description;
        $what_we_do_d->what_we_do_id = $request->what_we_do_id;
        $what_we_do_d->status = $request->status;

        if ($request->file('icon')) {
            $icon = $what_we_do_d->icon;
            $file = $request->icon;
            $extension = $file->getClientOriginalExtension();
            $filename = $this->currentDatetimString(). '.' . $extension;
            $what_we_do_d->icon = $filename;
            $path = public_path('uploads/what_we_do');
            $file->move($path, $filename);

            $file_path = public_path('uploads/what_we_do' . DIRECTORY_SEPARATOR . $icon);
            if (is_file($file_path)) {
                unlink($file_path);
            }

        }
        $what_we_do_d->save();

        Session::flash('success','Record Updated successfully !');
        return redirect()->route('what-we-do-detail.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  
        $what_we_do_d = WhatWeDoDetail::find($id);
        if(!is_null($what_we_do)){
            $file_path = public_path('uploads/what_we_do' . DIRECTORY_SEPARATOR . $what_we_do->image_name);
            if (is_file($file_path)) {
                unlink($file_path);
            }
        }
        $what_we_do_d->delete();
        Session::flash('info','Record Delete successfully !');
        return back();
    }
}
