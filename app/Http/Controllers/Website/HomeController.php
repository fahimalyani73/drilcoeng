<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HomeImageSlider;
use App\Models\Service;
use App\Models\GeneralInfo;
use App\Models\WhatWeDo;
use App\Models\WhatWeDoDetail;
use App\Models\Project;
use App\Models\Testimonial;
use App\Models\AboutUs;
use App\Models\AboutUsImageSlider;
use App\Models\OurTeam;
use App\Models\Faq;
use App\Models\ProjectDetail;
use App\Models\SeoData; 
use App\Models\Category;
use App\Models\Product;

class HomeController extends Controller
{
    
    public function index(){
        $home_slider = HomeImageSlider::where('status',1)->get();
        $service = Service::where('status',1)->limit(6)->get();
        $general_info = GeneralInfo::where('status',1)->get();
        $what_we_do = WhatWeDo::where('status',1)->first();
        $what_we_do_id = isset($what_we_do) && !empty($what_we_do) ? $what_we_do->id : 1;
        $what_we_do_detail = WhatWeDoDetail::where('what_we_do_id',$what_we_do_id)->get();
        $projects = Project::where('status',1)->get();
        $recent_projects = Project::where('recent_project',1)->where('status',1)->get();
        $testimonial = Testimonial::where('status',1)->get();

        $seo_data = SeoData::where('page_name','home_page')->where('status',1)->first();
    	$product = Category::where('status',1)->get()->toArray();
        $products = array_chunk($product, 4);

    	return view('website.pages.home.index',compact('recent_projects','testimonial','projects','what_we_do_detail','what_we_do','home_slider','service','general_info','seo_data','products'));
    }
    public function about(){
        $aboutus = AboutUs::where('status',1)->first();
        $about_us_id  = isset($aboutus) && !empty($aboutus) ? $aboutus->about_us_id : 0;
        $aboutus_image_slider = AboutUsImageSlider::where('about_us_id',$about_us_id)->where('status',1)->get();
        $seo_data = SeoData::where('page_name','about_us_page')->where('status',1)->first();
        
    	return view('website.pages.aboutus.index',compact('aboutus','aboutus_image_slider','seo_data'));
    }
    public function ourpeople(){
        $teams = OurTeam::where('status',1)->get();
        $seo_data = SeoData::where('page_name','our_team_page')->where('status',1)->first();
    	return view('website.pages.ourpeople.index',compact('seo_data','teams'));
    }
    public function testimonial(){
        $testimonial = Testimonial::where('status',1)->get();
        $seo_data = SeoData::where('page_name','testimonial_page')->where('status',1)->first();
        return view('website.pages.testimonials.index',compact('seo_data','testimonial'));
    }
    public function faq(){
        $faq = Faq::with('faq_detail')->where('status',1)->get();
        $recent_projects = Project::where('recent_project',1)->where('status',1)->get();
        $seo_data = SeoData::where('page_name','faq_page')->where('status',1)->first();
        
        return view('website.pages.faq.index',compact('seo_data','faq','recent_projects'));
    }
    public function contactus(){
        $seo_data = SeoData::where('page_name','contact_page')->where('status',1)->first();
        return view('website.pages.contactus.index',compact('seo_data'));
    }

    public function projects(){
        $projects = Project::where('status',1)->get();
        $seo_data = SeoData::where('page_name','all_projects')->where('status',1)->first();
        return view('website.pages.projects.index',compact('seo_data','projects'));
    }
    public function project_detail($name,$id){
        $projects = Project::with(['project_detail' => function($q) use($id) {
                $q->where('project_id', base64_decode($id))->first();
            },'project_detail.project_detail_image_slider'])->where('id',base64_decode($id))->where('status',1)->first();
        $project_name = isset($projects) && !empty($projects->title) ? str_replace(' ', '_', $projects->title) : '';
        if (!empty($project_name)) {
            $seo_data = SeoData::where('page_name',$project_name)->where('status',1)->first();    
        }else{
            $seo_data = '';
        }
        
        return view('website.pages.project_detail.index',compact('seo_data','projects'));
    }
    public function services(){
        $services = Service::where('status',1)->get();
        $seo_data = SeoData::where('page_name','all_services_page')->where('status',1)->first();
        return view('website.pages.services.index',compact('seo_data','services'));
    }
    public function service_detail($name,$id){
        $service = Service::where('status',1)->get();
        $services = Service::with(['service_detail' => function($q) use($id) {
                $q->where('service_id', base64_decode($id))->first();
            },'service_detail.service_detail_image_slider','service_detail.you_should_know','service_detail.what_we_make_us_different'])->where('id',base64_decode($id))->where('status',1)->first();
       $project_name = isset($projects) && !empty($projects->heading) ? str_replace(' ', '_', $projects->heading) : '';
        if (!empty($project_name)) {
            $seo_data = SeoData::where('page_name',$project_name)->where('status',1)->first();    
        }else{
            $seo_data = '';
        }

        return view('website.pages.service_detail.index',compact('seo_data','service','services'));
    }
    public function news(){
        return view('website.pages.news.index');
    }
    public function news_detail(){
        return view('website.pages.news_detail.index');
    }

    public function product_list(){
        $products = Product::where('status',1)->get();
        return view('website.pages.products.index',compact('products'));
    }
    public function product_bycat($name,$id){
        $products = Product::where('status',1)->where('cat_id',base64_decode($id))->get();
        return view('website.pages.products.index',compact('products'));   
    }
    
}



