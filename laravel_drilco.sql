-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 15, 2021 at 06:35 PM
-- Server version: 5.7.32
-- PHP Version: 5.6.40-38+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_drilco`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_3` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `about_us_image_sliders`
--

CREATE TABLE `about_us_image_sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `about_us_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(255) NOT NULL,
  `cat_description` text,
  `cat_image` varchar(255) NOT NULL,
  `status` tinyint(3) NOT NULL,
  `banner_text_color` varchar(20) NOT NULL DEFAULT '#FFFFFF',
  `banner_image_name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `cat_name`, `cat_description`, `cat_image`, `status`, `banner_text_color`, `banner_image_name`, `created_at`, `updated_at`) VALUES
(1, 'Category 1', 'Description', '31536.jpg', 1, '#ff0000', '62902.jpg', '2021-02-14 04:09:47', '2021-02-14 07:56:05'),
(2, 'Category 1', 'Description', '82508.jpg', 1, '#00ffee', '23448.jpg', '2021-02-14 04:10:16', '2021-02-14 07:57:30');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `heading` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question_category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faq_details`
--

CREATE TABLE `faq_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `faq_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `general_infos`
--

CREATE TABLE `general_infos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `total_project_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_projects` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_member_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_member` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hours_work_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hours_work` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `countries_experience_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `countries_experience` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `home_image_sliders`
--

CREATE TABLE `home_image_sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `heading_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `heading_2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `heading_3` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `button_text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `button_link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `slider_text_color` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#FFFFFF',
  `btn_bg_color` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#ffb600',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `home_image_sliders`
--

INSERT INTO `home_image_sliders` (`id`, `image_name`, `heading_1`, `heading_2`, `heading_3`, `button_text`, `button_link`, `status`, `slider_text_color`, `btn_bg_color`, `created_at`, `updated_at`) VALUES
(1, '96821.jpg', 'dkfjnsdklfjhlk', 'jhkjdfhgkljdfhgkljh', 'hkdjfgkldsfh', 'jhkdfkdg', 'hkjfghkdfjh', 1, '#ff0000', '#ff7b00', '2021-02-13 07:49:22', '2021-02-13 13:09:57'),
(2, '61511.jpg', 'jhkgkygj', 'jhkjdfhgkljdfhgkljh', 'hkdjfgkldsfh', 'FSDFGSDFGSDF', 'hkjfghkdfjh', 1, '#66ff00', '#ffb600', '2021-02-13 07:52:56', '2021-02-13 08:46:04');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_03_18_155254_create_home_image_sliders_table', 1),
(5, '2020_03_18_155843_create_services_table', 1),
(6, '2020_03_18_161047_create_service_details_table', 1),
(7, '2020_04_12_084149_create_service_detail_image_sliders_table', 1),
(8, '2020_04_12_084402_create_what_make_us_differents_table', 1),
(9, '2020_04_12_085028_create_you_should_knows_table', 1),
(10, '2020_04_12_085256_create_general_infos_table', 1),
(11, '2020_04_12_085448_create_what_we_dos_table', 1),
(12, '2020_04_12_085550_create_what_we_do_details_table', 1),
(13, '2020_04_12_085742_create_projects_table', 1),
(14, '2020_04_12_085916_create_project_details_table', 1),
(15, '2020_04_12_090159_create_project_detail_image_sliders_table', 1),
(16, '2020_04_12_090411_create_about_us_table', 1),
(17, '2020_04_12_090529_create_about_us_image_sliders_table', 1),
(18, '2020_04_12_090734_create_our_teams_table', 1),
(19, '2020_04_12_091001_create_testimonials_table', 1),
(20, '2020_04_12_091130_create_faqs_table', 1),
(21, '2020_04_12_091207_create_faq_details_table', 1),
(22, '2020_04_12_091321_create_contact_us_table', 1),
(23, '2020_04_26_112511_create_seo_data_table', 1),
(26, '2021_03_05_110508_create_settings_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `our_teams`
--

CREATE TABLE `our_teams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `p_image` varchar(255) DEFAULT NULL,
  `description` text,
  `p_price` varchar(20) DEFAULT NULL,
  `cat_id` int(11) NOT NULL,
  `banner_text_color` varchar(20) NOT NULL DEFAULT '#FFFFFF',
  `status` tinyint(3) NOT NULL,
  `banner_image_name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `p_image`, `description`, `p_price`, `cat_id`, `banner_text_color`, `status`, `banner_image_name`, `created_at`, `updated_at`) VALUES
(1, 'about', '25124.jpg', 'xckldf jgsdfg', NULL, 1, '#00ffee', 1, '74715.jpg', '2021-02-14 15:39:54', '2021-02-14 15:46:56');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner_image_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recent_project` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `banner_text_color` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#FFFFFF',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `title`, `type`, `image_name`, `banner_image_name`, `recent_project`, `status`, `banner_text_color`, `created_at`, `updated_at`) VALUES
(1, 'We Just Completes $17.6 Million Medical Clinic In Mid-Missouri', 'We Just', '89951.jpg', '13682.jpg', 1, 1, '#00ffe1', '2021-02-13 06:46:44', '2021-02-13 15:14:18');

-- --------------------------------------------------------

--
-- Table structure for table `project_details`
--

CREATE TABLE `project_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `architect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `size_detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `year_completed` date NOT NULL,
  `categories` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `countries_experience_icon` text COLLATE utf8mb4_unicode_ci,
  `countries_experience` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `project_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_details`
--

INSERT INTO `project_details` (`id`, `title`, `client_detail`, `architect`, `location_detail`, `size_detail`, `year_completed`, `categories`, `countries_experience_icon`, `countries_experience`, `status`, `project_id`, `created_at`, `updated_at`) VALUES
(1, 'We Just Completes $17.6 Million Medical Clinic In Mid-Missouri', 'We Just Completes $17.6 Million Medical Clinic In Mid-Missouri', 'We Just Completes $17.6 Million Medical Clinic In Mid-Missouri', 'We Just Completes $17.6 Million Medical Clinic In Mid-Missouri', 'We Just Completes $17.6 Million Medical Clinic In Mid-Missouri', '2021-02-14', 'We Just Completes $17.6 Million Medical Clinic In Mid-Missouri', 'We Just Completes $17.6 Million Medical Clinic In Mid-Missouri', 'We Just Completes $17.6 Million Medical Clinic In Mid-Missouri', 1, 1, '2021-02-13 15:25:12', '2021-02-13 15:25:12');

-- --------------------------------------------------------

--
-- Table structure for table `project_detail_image_sliders`
--

CREATE TABLE `project_detail_image_sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `project_detail_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_detail_image_sliders`
--

INSERT INTO `project_detail_image_sliders` (`id`, `name`, `image_name`, `status`, `project_detail_id`, `created_at`, `updated_at`) VALUES
(1, 'test', '40195.jpg', 1, 1, '2021-02-13 15:25:37', '2021-02-13 15:25:37');

-- --------------------------------------------------------

--
-- Table structure for table `seo_data`
--

CREATE TABLE `seo_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `meta_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_canonical` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner_image_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `heading` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `banner_text_color` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#FFFFFF',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `image_name`, `banner_image_name`, `heading`, `description`, `icon`, `status`, `banner_text_color`, `created_at`, `updated_at`) VALUES
(1, '46825.png', '81682.jpg', 'THANDLER AIRPORT WATER RECLAMATION FACILITY EXPANSION PROJECT NAMED', 'THANDLER AIRPORT WATER RECLAMATION FACILITY EXPANSION PROJECT NAMED', '33587.png', 1, '#ff0000', '2021-02-08 05:46:09', '2021-02-13 13:12:44'),
(2, '46825.png', '81682.jpg', 'THANDLER AIRPORT WATER RECLAMATION FACILITY EXPANSION PROJECT NAMED', 'THANDLER AIRPORT WATER RECLAMATION FACILITY EXPANSION PROJECT NAMED', '33587.png', 1, '#ff0000', '2021-02-08 05:46:09', '2021-02-13 13:12:44'),
(3, '46825.png', '81682.jpg', 'THANDLER AIRPORT WATER RECLAMATION FACILITY EXPANSION PROJECT NAMED', 'THANDLER AIRPORT WATER RECLAMATION FACILITY EXPANSION PROJECT NAMED', '33587.png', 1, '#ff0000', '2021-02-08 05:46:09', '2021-02-13 13:12:44'),
(4, '46825.png', '81682.jpg', 'THANDLER AIRPORT WATER RECLAMATION FACILITY EXPANSION PROJECT NAMED', 'THANDLER AIRPORT WATER RECLAMATION FACILITY EXPANSION PROJECT NAMED', '33587.png', 1, '#ff0000', '2021-02-08 05:46:09', '2021-02-13 13:12:44');

-- --------------------------------------------------------

--
-- Table structure for table `service_details`
--

CREATE TABLE `service_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `heading` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `service_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_details`
--

INSERT INTO `service_details` (`id`, `heading`, `description_1`, `description_2`, `banner_image`, `status`, `service_id`, `created_at`, `updated_at`) VALUES
(1, 'SILICON BENCH AND CORNIKE BEGIN CONSTRUCTION SOLAR FACILITIES', 'SILICON BENCH AND CORNIKE BEGIN CONSTRUCTION SOLAR FACILITIES', 'SILICON BENCH AND CORNIKE BEGIN CONSTRUCTION SOLAR FACILITIES', '59368.jpg', 1, 2, '2021-02-08 09:55:54', '2021-02-13 13:20:48'),
(2, 'SILICON BENCH AND CORNIKE BEGIN CONSTRUCTION SOLAR FACILITIES', 'SILICON BENCH AND CORNIKE BEGIN CONSTRUCTION SOLAR FACILITIES', 'SILICON BENCH AND CORNIKE BEGIN CONSTRUCTION SOLAR FACILITIES', '20707.jpg', 1, 1, '2021-02-13 13:21:02', '2021-02-13 13:21:02'),
(3, 'SILICON BENCH AND CORNIKE BEGIN CONSTRUCTION SOLAR FACILITIES', 'SILICON BENCH AND CORNIKE BEGIN CONSTRUCTION SOLAR FACILITIES', 'SILICON BENCH AND CORNIKE BEGIN CONSTRUCTION SOLAR FACILITIES', '83849.jpg', 1, 2, '2021-02-13 13:21:16', '2021-02-13 13:21:16'),
(4, 'SILICON BENCH AND CORNIKE BEGIN CONSTRUCTION SOLAR FACILITIES', 'SILICON BENCH AND CORNIKE BEGIN CONSTRUCTION SOLAR FACILITIES', 'SILICON BENCH AND CORNIKE BEGIN CONSTRUCTION SOLAR FACILITIES', '47313.jpg', 1, 3, '2021-02-13 13:21:28', '2021-02-13 13:21:28');

-- --------------------------------------------------------

--
-- Table structure for table `service_detail_image_sliders`
--

CREATE TABLE `service_detail_image_sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `service_detail_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_detail_image_sliders`
--

INSERT INTO `service_detail_image_sliders` (`id`, `name`, `image_name`, `status`, `service_detail_id`, `created_at`, `updated_at`) VALUES
(1, 'test', '32480.jpg', 1, 1, '2021-02-08 09:56:08', '2021-02-13 13:19:35'),
(2, 'test', '45519.jpg', 1, 1, '2021-02-13 09:10:14', '2021-02-13 09:10:14');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_phone_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_phone_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_1` longtext COLLATE utf8mb4_unicode_ci,
  `address_2` longtext COLLATE utf8mb4_unicode_ci,
  `address_3` longtext COLLATE utf8mb4_unicode_ci,
  `xml_script` longtext COLLATE utf8mb4_unicode_ci,
  `footer_aboutus` longtext COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `logo`, `mobile_phone_1`, `mobile_phone_2`, `address_1`, `address_2`, `address_3`, `xml_script`, `footer_aboutus`, `status`, `created_at`, `updated_at`) VALUES
(1, '15433.png', '55312', '809809', 'kjkh', 'kjh', 'kjhkj', 'sdg', 'hkjh', 1, '2021-03-05 06:56:43', '2021-03-05 07:11:46');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', NULL, '$2y$10$vHjLDMGsUonD8OUnC2l4B.aNrxznmMcnHAcy.1./HBbbXb6.ORM5i', NULL, '2021-01-30 15:37:38', '2021-01-30 15:37:38');

-- --------------------------------------------------------

--
-- Table structure for table `what_make_us_differents`
--

CREATE TABLE `what_make_us_differents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description_1` text COLLATE utf8mb4_unicode_ci,
  `description_2` text COLLATE utf8mb4_unicode_ci,
  `text_point_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_point_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_point_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_point_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_point_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_point_6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_point_7` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `service_detail_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `what_make_us_differents`
--

INSERT INTO `what_make_us_differents` (`id`, `description_1`, `description_2`, `text_point_1`, `text_point_2`, `text_point_3`, `text_point_4`, `text_point_5`, `text_point_6`, `text_point_7`, `status`, `service_detail_id`, `created_at`, `updated_at`) VALUES
(1, 'jkhkjh', 'hkjh', 'jkh', 'kj', 'hkj', 'h', 'kjh', 'jkh', 'jk', 1, 1, '2021-02-08 09:56:23', '2021-02-08 09:56:23');

-- --------------------------------------------------------

--
-- Table structure for table `what_we_dos`
--

CREATE TABLE `what_we_dos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `heading` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `what_we_do_details`
--

CREATE TABLE `what_we_do_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `what_we_do_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `you_should_knows`
--

CREATE TABLE `you_should_knows` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_1` text COLLATE utf8mb4_unicode_ci,
  `title_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_2` text COLLATE utf8mb4_unicode_ci,
  `title_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_3` text COLLATE utf8mb4_unicode_ci,
  `title_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_4` text COLLATE utf8mb4_unicode_ci,
  `title_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_5` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `service_detail_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `you_should_knows`
--

INSERT INTO `you_should_knows` (`id`, `title_1`, `description_1`, `title_2`, `description_2`, `title_3`, `description_3`, `title_4`, `description_4`, `title_5`, `description_5`, `status`, `service_detail_id`, `created_at`, `updated_at`) VALUES
(1, 'kjkl', 'hlk', 'hlk', 'hj', 'k,j', 'lkj', 'lkj', 'lkj', 'kmbn', 'jkh', 1, 1, '2021-02-08 09:56:40', '2021-02-08 09:56:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `about_us_image_sliders`
--
ALTER TABLE `about_us_image_sliders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `about_us_image_sliders_about_us_id_foreign` (`about_us_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq_details`
--
ALTER TABLE `faq_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `faq_details_faq_id_foreign` (`faq_id`);

--
-- Indexes for table `general_infos`
--
ALTER TABLE `general_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_image_sliders`
--
ALTER TABLE `home_image_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `our_teams`
--
ALTER TABLE `our_teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_details`
--
ALTER TABLE `project_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_details_project_id_foreign` (`project_id`);

--
-- Indexes for table `project_detail_image_sliders`
--
ALTER TABLE `project_detail_image_sliders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_detail_image_sliders_project_detail_id_foreign` (`project_detail_id`);

--
-- Indexes for table `seo_data`
--
ALTER TABLE `seo_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_details`
--
ALTER TABLE `service_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_details_service_id_foreign` (`service_id`);

--
-- Indexes for table `service_detail_image_sliders`
--
ALTER TABLE `service_detail_image_sliders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_detail_image_sliders_service_detail_id_foreign` (`service_detail_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `what_make_us_differents`
--
ALTER TABLE `what_make_us_differents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `what_make_us_differents_service_detail_id_foreign` (`service_detail_id`);

--
-- Indexes for table `what_we_dos`
--
ALTER TABLE `what_we_dos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `what_we_do_details`
--
ALTER TABLE `what_we_do_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `what_we_do_details_what_we_do_id_foreign` (`what_we_do_id`);

--
-- Indexes for table `you_should_knows`
--
ALTER TABLE `you_should_knows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `you_should_knows_service_detail_id_foreign` (`service_detail_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `about_us_image_sliders`
--
ALTER TABLE `about_us_image_sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `faq_details`
--
ALTER TABLE `faq_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `general_infos`
--
ALTER TABLE `general_infos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `home_image_sliders`
--
ALTER TABLE `home_image_sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `our_teams`
--
ALTER TABLE `our_teams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `project_details`
--
ALTER TABLE `project_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `project_detail_image_sliders`
--
ALTER TABLE `project_detail_image_sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `seo_data`
--
ALTER TABLE `seo_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `service_details`
--
ALTER TABLE `service_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `service_detail_image_sliders`
--
ALTER TABLE `service_detail_image_sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `what_make_us_differents`
--
ALTER TABLE `what_make_us_differents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `what_we_dos`
--
ALTER TABLE `what_we_dos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `what_we_do_details`
--
ALTER TABLE `what_we_do_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `you_should_knows`
--
ALTER TABLE `you_should_knows`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `about_us_image_sliders`
--
ALTER TABLE `about_us_image_sliders`
  ADD CONSTRAINT `about_us_image_sliders_about_us_id_foreign` FOREIGN KEY (`about_us_id`) REFERENCES `about_us` (`id`);

--
-- Constraints for table `faq_details`
--
ALTER TABLE `faq_details`
  ADD CONSTRAINT `faq_details_faq_id_foreign` FOREIGN KEY (`faq_id`) REFERENCES `faqs` (`id`);

--
-- Constraints for table `project_details`
--
ALTER TABLE `project_details`
  ADD CONSTRAINT `project_details_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`);

--
-- Constraints for table `project_detail_image_sliders`
--
ALTER TABLE `project_detail_image_sliders`
  ADD CONSTRAINT `project_detail_image_sliders_project_detail_id_foreign` FOREIGN KEY (`project_detail_id`) REFERENCES `project_details` (`id`);

--
-- Constraints for table `service_details`
--
ALTER TABLE `service_details`
  ADD CONSTRAINT `service_details_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`);

--
-- Constraints for table `service_detail_image_sliders`
--
ALTER TABLE `service_detail_image_sliders`
  ADD CONSTRAINT `service_detail_image_sliders_service_detail_id_foreign` FOREIGN KEY (`service_detail_id`) REFERENCES `service_details` (`id`);

--
-- Constraints for table `what_make_us_differents`
--
ALTER TABLE `what_make_us_differents`
  ADD CONSTRAINT `what_make_us_differents_service_detail_id_foreign` FOREIGN KEY (`service_detail_id`) REFERENCES `service_details` (`id`);

--
-- Constraints for table `what_we_do_details`
--
ALTER TABLE `what_we_do_details`
  ADD CONSTRAINT `what_we_do_details_what_we_do_id_foreign` FOREIGN KEY (`what_we_do_id`) REFERENCES `what_we_dos` (`id`);

--
-- Constraints for table `you_should_knows`
--
ALTER TABLE `you_should_knows`
  ADD CONSTRAINT `you_should_knows_service_detail_id_foreign` FOREIGN KEY (`service_detail_id`) REFERENCES `service_details` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
