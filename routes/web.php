<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/','Website\HomeController@index')->name('home.index');
Route::get('/about-us','Website\HomeController@about')->name('about.us');
Route::get('/our-people','Website\HomeController@ourpeople')->name('our.people');
Route::get('/testimonial','Website\HomeController@testimonial')->name('testimonial');
Route::get('/faq','Website\HomeController@faq')->name('faq');
Route::get('/contact-us','Website\HomeController@contactus')->name('contact.us');

Route::get('/projects','Website\HomeController@projects')->name('projects');
Route::get('/project-detail/{name}/{id}','Website\HomeController@project_detail')->name('project.detail');
Route::get('/services','Website\HomeController@services')->name('services');
Route::get('/service-detail/{name}/{id}','Website\HomeController@service_detail')->name('service.detail');
Route::get('/news','Website\HomeController@news')->name('news');
Route::get('/news-detail','Website\HomeController@news_detail')->name('news.detail');

// products
Route::get('products','Website\HomeController@product_list')->name('products');
Route::get('products/{name}/{id}','Website\HomeController@product_bycat')->name('products.category');

Route::group(['middleware' => 'auth'], function(){
	// admin routes 
	Route::get('dashboard','Admin\DashboardController@index')->name('admin.dashboard');
	Route::resource('admin/home-image-slider','Admin\HomeImageSliderController');
	Route::resource('admin/offerforstudent','Admin\OfferForStudentController');
	Route::resource('admin/contactus','Admin\ContactUsController');
	Route::resource('admin/ourpresence','Admin\OurPresenceController');
	Route::resource('admin/awardsforyou','Admin\AwardsForYouController');
	Route::resource('admin/ourmindpower','Admin\OurMindPowerController');
	Route::resource('admin/ourpresence','Admin\OurPresenceController');
	Route::resource('admin/school','Admin\SchoolController');
	Route::resource('admin/testimonial','Admin\TestimonialController');
	Route::resource('admin/settings','Admin\WebsiteSettingController');
	Route::resource('admin/services','Admin\ServiceController');
	Route::resource('admin/service-detail','Admin\ServiceDetailController');
	Route::resource('admin/service-detail-image-slider','Admin\ServiceDetailImageSliderController');
	Route::resource('admin/what-we-make-us-different','Admin\WhatMakeUsDifferentController');
	Route::resource('admin/you-should-know','Admin\YouShouldKnowController');
	Route::resource('admin/projects','Admin\ProjectController');
	Route::resource('admin/project-detail','Admin\ProjectDetailController');
	Route::resource('admin/project-detail-image-slider','Admin\ProjectDetailImageSliderController');
	Route::resource('admin/faq','Admin\FAQController');
	Route::resource('admin/faq-detail','Admin\FAQDetailController');
	Route::resource('admin/aboutus','Admin\AboutusController');
	Route::resource('admin/aboutus-detail','Admin\AboutusDetailController');
	Route::resource('admin/what-we-do','Admin\WhatWeDoController');
	Route::resource('admin/what-we-do-detail','Admin\WhatWeDoDetailController');
	Route::resource('admin/our-team','Admin\OurTeamController');
	Route::resource('admin/general-info','Admin\GeneralInfoController');

	Route::resource('admin/seo-data','Admin\SeoDataController');

	Route::resource('admin/categories','Admin\CategoriesController');
	Route::resource('admin/products','Admin\ProductsController');

	Route::resource('admin/settings','Admin\SettingController');

});