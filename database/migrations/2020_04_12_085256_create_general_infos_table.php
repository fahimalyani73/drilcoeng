<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_infos', function (Blueprint $table) {
            $table->id();
            $table->string('total_project_icon')->nullable();
            $table->string('total_projects')->nullable();
            $table->string('staff_member_icon')->nullable();
            $table->string('staff_member')->nullable();
            $table->string('hours_work_icon')->nullable();
            $table->string('hours_work')->nullable();
            $table->string('countries_experience_icon')->nullable();
            $table->string('countries_experience')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_infos');
    }
}
