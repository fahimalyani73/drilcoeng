<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWhatMakeUsDifferentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('what_make_us_differents', function (Blueprint $table) {
            $table->id();
            $table->text('description_1')->nullable();
            $table->text('description_2')->nullable();
            $table->string('text_point_1')->nullable();
            $table->string('text_point_2')->nullable();
            $table->string('text_point_3')->nullable();
            $table->string('text_point_4')->nullable();
            $table->string('text_point_5')->nullable();
            $table->string('text_point_6')->nullable();
            $table->string('text_point_7')->nullable();
            $table->boolean('status')->default(1);
            $table->bigInteger('service_detail_id')->unsigned();
            $table->foreign('service_detail_id')->references('id')->on('service_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('what_make_us_differents');
    }
}
