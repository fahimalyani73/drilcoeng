<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceDetailImageSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_detail_image_sliders', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('image_name');
            $table->boolean('status')->default(1);
            $table->bigInteger('service_detail_id')->unsigned();
            $table->foreign('service_detail_id')->references('id')->on('service_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_detail_image_sliders');
    }
}
