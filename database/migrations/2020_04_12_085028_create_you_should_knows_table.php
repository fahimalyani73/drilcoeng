<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateYouShouldKnowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('you_should_knows', function (Blueprint $table) {
            $table->id();
            $table->string('title_1')->nullable();
            $table->text('description_1')->nullable();
            $table->string('title_2')->nullable();
            $table->text('description_2')->nullable();
            $table->string('title_3')->nullable();
            $table->text('description_3')->nullable();
            $table->string('title_4')->nullable();
            $table->text('description_4')->nullable();
            $table->string('title_5')->nullable();
            $table->text('description_5')->nullable();
            $table->boolean('status')->default(1);
            $table->bigInteger('service_detail_id')->unsigned();
            $table->foreign('service_detail_id')->references('id')->on('service_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('you_should_knows');
    }
}
