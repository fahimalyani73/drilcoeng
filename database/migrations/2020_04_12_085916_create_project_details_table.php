<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_details', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('client_detail');
            $table->string('architect');
            $table->string('location_detail');
            $table->string('size_detail');
            $table->date('year_completed');
            $table->string('categories');
            $table->string('countries_experience_icon')->nullable();
            $table->string('countries_experience')->nullable();
            $table->boolean('status')->default(1);
            $table->bigInteger('project_id')->unsigned();
            $table->foreign('project_id')->references('id')->on('projects');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_details');
    }
}
