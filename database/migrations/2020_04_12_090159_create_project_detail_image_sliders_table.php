<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectDetailImageSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_detail_image_sliders', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('image_name');
            $table->boolean('status')->default(1);
            $table->bigInteger('project_detail_id')->unsigned();
            $table->foreign('project_detail_id')->references('id')->on('project_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_detail_image_sliders');
    }
}
