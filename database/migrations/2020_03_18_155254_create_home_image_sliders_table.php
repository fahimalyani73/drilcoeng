<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeImageSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_image_sliders', function (Blueprint $table) {
            $table->id();
            $table->string('image_name');
            $table->string('heading_1');
            $table->string('heading_2');
            $table->text('heading_3');
            $table->string('button_text');
            $table->text('button_link');
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_image_sliders');
    }
}
